// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  url: 'http://18.222.131.191:8081/api',
  // urlV2: 'http://localhost:8082/api',
  urlV2: 'http://172.105.63.203:8082/api',
  signallingServer: 'wss://ap4ny7bhye.execute-api.us-west-2.amazonaws.com/websocket',
  stunServer: 'stun:3.17.26.243:3478',
  turnServer: 'turn:3.17.26.243:3478',
  turnServerUsername: 'turnZiksukaUsername',
  turnServerPassword: 'turnjasg27181ji921k108822Ziksuka'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
