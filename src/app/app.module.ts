import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared/_guards/auth-guard';
import { HttpConfigInterceptor } from './shared/_helpers/interceptor';
import { LoginGuard } from './shared/_guards/login-guard';
import { SharedModule } from './shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { LocationStrategy, HashLocationStrategy, DatePipe } from '@angular/common';
import { GestureConfig } from '@angular/material';
import { AdminGuard } from './shared/_guards/admin-guard';
import { TeacherStudentGuard } from './shared/_guards/teacher-student-guard';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { CookieService } from 'ngx-cookie-service';
import { PrivacyComponent } from './privacy/privacy.component';


@NgModule({
  declarations: [
    AppComponent,
    ContactUsComponent,
    PrivacyComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    HttpClientModule,
    ToastrModule.forRoot({
      timeOut: 2000,
      positionClass: 'toast-top-right',
      preventDuplicates: true
    })
  ],
  providers: [
    AuthGuard,
    LoginGuard,
    AdminGuard,
    TeacherStudentGuard,
    DatePipe,
    CookieService,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true },
    {provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig}
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
