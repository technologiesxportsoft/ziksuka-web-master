import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPagesRoutingModule } from './admin-pages-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { QuestionsComponent } from './questions/questions.component';
import { ResourcesComponent } from './resources/resources.component';
import { SearchBooksComponent } from './search-books/search-books.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { QuestionsService } from '../shared/_services/questions.service';
import { EditQuestionComponent } from './edit-question/edit-question.component';
import { MatChipsModule } from '@angular/material/chips';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    DashboardComponent,
    QuestionsComponent,
    ResourcesComponent,
    SearchBooksComponent,
    BookDetailsComponent,
    EditQuestionComponent
  ],
  imports: [
    SharedModule,
    CommonModule,
    AdminPagesRoutingModule,
    MatChipsModule,
    MatFormFieldModule,
    MatIconModule,
    MatTooltipModule,
    MatExpansionModule,
    MatButtonModule
  ],
  providers : [
    QuestionsService
  ]
})
export class AdminPagesModule { }
