import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.css']
})
export class ResourcesComponent implements OnInit {

  resourceType: string;
  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.resourceType = params.resourceType;
    });
  }

  ngOnInit() {
  }

}
