import { Component, OnInit } from '@angular/core';
import { isDevMode } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-book-details',
  templateUrl: './book-details.component.html',
  styleUrls: ['./book-details.component.css']
})
export class BookDetailsComponent implements OnInit {
  topics;
  closeResult = '';
  newTopicName = '';
  newSubTopicName = '';
  bookDetails = {id: 123};

  constructor(private modalService: NgbModal) {
    if (isDevMode()) {
      this.topics = [
        {
          id: 1,
          title: 'Topic 1',
          subtopics: [
            { id: 1, title: 'Subtopic-11' },
            { id: 2, title: 'Subtopic-12' },
            { id: 3, title: 'Subtopic-13' },
          ]
        },
        {
          id: 2,
          title: 'Topic 2',
          subtopics: [
            { id: 4, title: 'Subtopic-21' },
            { id: 5, title: 'Subtopic-22' },
            { id: 6, title: 'Subtopic-23' },
          ]
        },
      ];
    }
  }

  ngOnInit() {
  }

  openModal(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${reason}`;
    });
  }

  addSubTopic(i, modal) {
    this.openModal(modal);
  }

}
