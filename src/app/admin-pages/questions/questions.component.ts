import { Component, OnInit } from '@angular/core';
import { QuestionsService } from 'src/app/shared/_services/questions.service';
import { BehaviorSubject } from 'rxjs';
import { MatDialog } from '@angular/material';
import { QuestionPreviewComponent } from 'src/app/shared/_components/question-preview/question-preview.component';
import { CommonService } from 'src/app/shared/_services/common.service';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.css']
})
export class QuestionsComponent implements OnInit {
  // tslint:disable-next-line:variable-name
  _page = 1;
  // tslint:disable-next-line:variable-name
  _pageSize = 10;
  collectionSize = 0;
  // tslint:disable-next-line:variable-name
  _allQuestions = [];
  questions$: BehaviorSubject<any>;

  classOptions: BehaviorSubject<any>;
  subjectOptions: BehaviorSubject<any>;
  bookOptions: BehaviorSubject<any>;
  topicOptions: BehaviorSubject<any>;
  subtopicOptions: BehaviorSubject<any>;

  classField = null;
  subjectField = null;
  bookField = null;
  topicField = null;
  subtopicField = null;

  className = '';
  subjectName = '';
  bookName = '';
  topicName = '';
  subTopicName = '';

  loadingTasks = 0;

  mathJaxObject;

  constructor(public qServ: QuestionsService, public dialog: MatDialog, private commonService: CommonService) {
    this.questions$ = new BehaviorSubject<any>(null);
    this.classOptions = new BehaviorSubject<any>([]);
    this.subjectOptions = new BehaviorSubject<any>([]);
    this.bookOptions = new BehaviorSubject<any>([]);
    this.topicOptions = new BehaviorSubject<any>([]);
    this.subtopicOptions = new BehaviorSubject<any>([]);
    this.qServ.getGrades()
      .toPromise()
      .then(ret => {
        console.log('Received grades', ret);
        this.classOptions.next(ret.data);
      });
    console.log('inside costurctor');
  }

  ngOnInit() { }

  private updatePagination() {
    this.renderMath();
    let ret = this.allQuestions;
    this.collectionSize = this.allQuestions.length;
    ret = ret.slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
    this.questions$.next(ret);
  }

  set page(p) { this._page = p; this.updatePagination(); }
  get page() { return this._page; }

  set pageSize(p) { this._pageSize = p; this.updatePagination(); }
  get pageSize() { return this._pageSize; }

  set allQuestions(p) { this._allQuestions = p; this.updatePagination(); }
  get allQuestions() { return this._allQuestions; }

  public searchForQuestions() {
    this.loadingTasks++;
    this.qServ.getQuestions(+this.subtopicField)
      .toPromise()
      .then(ret => {
        console.log('received this', ret);
        if (ret.success) {
          this.allQuestions = ret.data;
        } else {
          alert('Failed to fetch questions');
        }
      })
      .catch(e => console.error('Caught error', e))
      .finally(() => this.loadingTasks--);
  }

  classFieldChanged() {
    if (!this.classField) {
      this.subjectOptions.next([]);
      this.bookOptions.next([]);
      this.topicOptions.next([]);
      this.subjectField = null;
      this.subjectFieldChanged();
    } else {
      const a = (this.classOptions.value).find(e => {
        console.log(this.classField, e.gradeId);
        return e.gradeId === +this.classField;
      });
      this.className = a ? a.gradeName : '';
      this.loadingTasks++;
      this.qServ.fetchSubjects(this.classField)
        .toPromise()
        .then(ret => {
          if (ret.success) {
            this.subjectOptions.next(ret.data);
            this.subjectField = null;
            this.subjectFieldChanged();
          } else {
            alert('Failed to fetch subjects');
          }
        })
        .finally(() => this.loadingTasks--);
    }
  }

  subjectFieldChanged() {
    if (!this.subjectField) {
      this.bookOptions.next([]);
      this.topicOptions.next([]);
      this.bookField = null;
      this.bookFieldChanged();
    } else {
      const a = (this.subjectOptions.value).find(e => e.subjectId === +this.subjectField);
      this.subjectName = a ? a.subjectName : '';
      this.loadingTasks++;
      this.qServ.fetchBooks(this.subjectField, this.classField)
        .toPromise()
        .then(ret => {
          console.log('Received books', ret);
          if (ret.success) {
            this.bookOptions.next(ret.data);
            this.bookField = null;
            this.bookFieldChanged();
          } else {
            alert('Failed to fetch books');
          }
        })
        .finally(() => this.loadingTasks--);
    }
  }

  bookFieldChanged() {
    if (!this.bookField) {
      console.log('bookfieldchanged');
      this.topicOptions.next([]);
      this.topicField = null;
      this.topicFieldChanged();
    } else {
      const a = (this.bookOptions.value).find(e => e.bookId === +this.bookField);
      this.bookName = a ? a.title : '';
      this.loadingTasks++;
      this.qServ.fetchTopics(this.bookField)
        .toPromise()
        .then(ret => {
          console.log('Received topics', ret);
          if (ret.success) {
            this.topicOptions.next(ret.data);
            this.topicField = null;
            this.topicFieldChanged();
          } else {
            alert('Failed to fetch topics');
          }
        })
        .finally(() => this.loadingTasks--);
    }
  }

  topicFieldChanged() {
    if (!this.topicField) {
      console.log('topicfieldchanged');
    } else {
      const a = (this.topicOptions.value).find(e => e.topicId === +this.topicField);
      this.topicName = a ? a.topicName : '';
      this.loadingTasks++;
      this.qServ.fetchSubtopics(this.bookField, this.topicField)
        .toPromise()
        .then(ret => {
          console.log('Received subtopics', ret);
          if (ret.success) {
            this.subtopicOptions.next(ret.data);
            this.subtopicField = null;
            this.subtopicFieldChanged();
          } else {
            alert('Failed to fetch subtopics');
          }
        })
        .finally(() => this.loadingTasks--);
    }
  }

  subtopicFieldChanged() {
    if (!this.subtopicField) {

    } else {
      const a = (this.subtopicOptions.value).find(e => e.subTopicId === +this.subtopicField);
      if (a) {
        this.subTopicName = a.subTopicName;
      } else {
        this.subTopicName = '';
      }
    }
  }

  previewQuestion(question) {
    console.log('Sending question to preview', question);
    this.renderMath();
    this.dialog.open(QuestionPreviewComponent, {
      width: '80vw',
      // height: '80vh',
      panelClass: 'preview-question-panel',
      data: {
        question,
        meta: {
          schoolSubjectName: this.subjectName,
          topicName: this.topicName,
          subtopicName: this.subTopicName
        }
      }
    });
  }

  updateMathObt() {
    this.mathJaxObject = this.commonService.nativeGlobal()['MathJax'];
  }

  renderMath() {
    this.updateMathObt();
    let angObj = this;
    setTimeout(() => {
      angObj.mathJaxObject['Hub'].Queue(["Typeset", angObj.mathJaxObject.Hub], 'mathContent');
    }, 1000)
  }

  loadMathConfig() {
    this.updateMathObt();
    this.mathJaxObject.Hub.Config({
      showMathMenu: false,
      // tex2jax: { inlineMath: [["$", "$"]], displayMath: [["$$", "$$"]] },
      tex2jax: { inlineMath: [["$", "$"], ["\\(", "\\)"]], displayMath: [["$$", "$$"], ["$", "$"]] },
      menuSettings: { zoom: "Double-Click", zscale: "150%" },
      CommonHTML: { linebreaks: { automatic: true } },
      "HTML-CSS": { linebreaks: { automatic: true } },
      SVG: { linebreaks: { automatic: true } }
    });
  }

}
