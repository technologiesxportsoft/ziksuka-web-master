import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { QuestionsService } from 'src/app/shared/_services/questions.service';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips/typings/chip-input';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { QuestionPreviewComponent } from 'src/app/shared/_components/question-preview/question-preview.component';
import { number } from '@amcharts/amcharts4/core';

@Component({
  selector: 'app-edit-question',
  templateUrl: './edit-question.component.html',
  styleUrls: ['./edit-question.component.css']
})
export class EditQuestionComponent implements OnInit, OnDestroy {
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];

  reloadSubscr: any;

  createNewQuestion = null;
  questionType = '';
  loading = new BehaviorSubject<boolean>(true);
  subTopicId: number = null;
  language = 'ENGLISH';
  questionId = null;
  uploading = new BehaviorSubject<boolean>(false);

  classField = null;
  subjectField = null;
  bookField = null;
  topicField = null;

  className = null;
  subjectName = null;
  bookName = null;
  topicName = null;
  subTopicName = null;

  hashStr = '';

  loadingTasks = 0;

  sco = {
    question: '',
    question_html: '',
    numOptions: 2,
    options: ['', '', '', '', '', ''],
    correct_answer: null,
    explanation: '',
    score: 1,
    tags: [],
    scene: '',
    scenario: ''
  };
  mco = {
    question: '',
    question_html: '',
    numOptions: 3,
    options: ['', '', '', '', '', ''],
    correct_answer: [],
    explanation: '',
    score: 1,
    tags: [],
    scene: '',
    scenario: ''
  };

  // tslint:disable-next-line:variable-name
  true_false = {
    question: '',
    question_html: '',
    correct_answer: '',
    explanation: '',
    score: 1,
    tags: [],
    scene: '',
    scenario: ''
  };

  // tslint:disable-next-line:variable-name
  fill_in_the_blank = {
    question: '',
    question_html: '',
    correct_answer: '',
    explanation: '',
    score: 1,
    tags: [],
    scene: '',
    scenario: ''
  };

  classOptions: BehaviorSubject<any>;
  subjectOptions: BehaviorSubject<any>;
  bookOptions: BehaviorSubject<any>;
  topicOptions: BehaviorSubject<any>;
  subtopicOptions: BehaviorSubject<any>;

  public sceneOptions = [
    { label: 'Visual', value: 'visual' },
    { label: 'Listen', value: 'listen' },
    { label: 'Feel', value: 'feel' },
    { label: 'Taste', value: 'taste' },
    { label: 'Smell', value: 'smell' },
  ];
  public scenarioOptions = [
    { label: 'NCERT', value: 'ncert' },
    { label: 'School', value: 'school' },
    { label: 'Home', value: 'home' },
    { label: 'Others', value: 'others' },
  ];
  constructor(private route: ActivatedRoute, public qServ: QuestionsService, private router: Router, private dialog: MatDialog) {
    this.classOptions = new BehaviorSubject<any>([]);
    this.subjectOptions = new BehaviorSubject<any>([]);
    this.bookOptions = new BehaviorSubject<any>([]);
    this.topicOptions = new BehaviorSubject<any>([]);
    this.subtopicOptions = new BehaviorSubject<any>([]);

    this.route.queryParams.subscribe(param => {
      if (param.createNew === 'true') {
        if (!param.subtopic) {
          alert('Subtopic not present');
          return;
        }
        try {
          // tslint:disable-next-line:radix
          this.subTopicId = parseInt(param.subtopic);
        } catch (e) {
          alert('Invalid subtopic');
          return;
        }
        this.createNewQuestion = true;
        if (param.className) {
          this.className = param.className;
        }
        if (param.subjectName) {
          this.subjectName = param.subjectName;
        }
        if (param.bookName) {
          this.bookName = param.bookName;
        }
        if (param.topicName) {
          this.topicName = param.topicName;
        }
        if (param.subTopicName) {
          this.subTopicName = param.subTopicName;
        }
        if (!param.className && param.subjectName && !param.bookName && !param.topicName && !param.subTopicName) {
          this.loadingTasks++;
          this.qServ.getGrades()
            .toPromise()
            .then(ret => {
              console.log('Received grades', ret);
              this.classOptions.next(ret.data);
            })
            .catch(e => console.error(e))
            .finally(() => this.loadingTasks--);
        }
        this.loading.next(false);
      } else if (param.questionId) {
        this.createNewQuestion = false;
        this.loading.next(false);
        this.loadingTasks++;
        this.qServ.getQuestionById(+param.questionId)
          .toPromise()
          .then(ret => {
            console.log('received ques', ret);
            this.receiveQuestion(ret.data);
          })
          .catch(e => console.error(e))
          .finally(() => this.loadingTasks--);
      }
    });
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;

    this.reloadSubscr = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    if (this.reloadSubscr) {
      this.reloadSubscr.unsubscribe();
    }
  }

  receiveQuestion(q) {
    this.questionId = q.id;
    console.log('Received question id', this.questionId);
    switch (q.type.toLowerCase()) {
      case 'sco':
        this.questionType = 'sco';
        this.sco.question = this.emptyIfNull(q.question);
        this.sco.question_html = this.emptyIfNull(q.questionHtml);
        this.sco.numOptions = q.options.length;
        this.sco.options = q.options;
        this.sco.correct_answer = q.correctAnswer;
        this.sco.explanation = this.emptyIfNull(q.explanation);
        this.sco.tags = q.tags;
        this.sco.scene = this.emptyIfNull(q.scene);
        this.sco.scenario = this.emptyIfNull(q.scenario);
        this.sco.score = q.score;
        break;

      case 'mco':
        this.questionType = 'mco';
        this.mco.question = this.emptyIfNull(q.question);
        this.mco.question_html = this.emptyIfNull(q.questionHtml);
        this.mco.numOptions = q.options.length;
        this.mco.options = q.options;
        q.correctAnswer.forEach(e => {
          this.mco.correct_answer[e - 1] = true;
        });
        this.mco.explanation = this.emptyIfNull(q.explanation);
        this.mco.tags = q.tags;
        this.mco.scene = this.emptyIfNull(q.scene);
        this.mco.scenario = this.emptyIfNull(q.scenario);
        this.mco.score = q.score;
        break;

      case 'true_false':
        this.questionType = 'true_false';
        this.true_false.question = this.emptyIfNull(q.question);
        this.true_false.question_html = this.emptyIfNull(q.questionHtml);
        this.true_false.correct_answer = q.correctAnswer;
        console.log('ca', this.true_false.correct_answer);
        this.true_false.explanation = this.emptyIfNull(q.explanation);
        this.true_false.tags = q.tags;
        this.true_false.scene = this.emptyIfNull(q.scene);
        this.true_false.scenario = this.emptyIfNull(q.scenario);
        this.true_false.score = q.score;
        break;

      case 'fill_in_the_blank':
        this.questionType = 'fill_in_the_blank';
        this.fill_in_the_blank.question = this.emptyIfNull(q.question);
        this.fill_in_the_blank.question_html = this.emptyIfNull(q.questionHtml);
        this.fill_in_the_blank.correct_answer = q.correctAnswer;
        this.fill_in_the_blank.explanation = this.emptyIfNull(q.explanation);
        this.fill_in_the_blank.tags = q.tags;
        this.fill_in_the_blank.scene = this.emptyIfNull(q.scene);
        this.fill_in_the_blank.scenario = this.emptyIfNull(q.scenario);
        this.fill_in_the_blank.score = q.score;
        break;

      default:
        alert(`Editing for ${q.type} questions is not implemented yet!`);
    }
  }

  prepareRequestBody() {
    const req = {} as any;
    if (this.createNewQuestion) {
      req.subtopicId = this.subTopicId;
    } else {
      req.questionId = this.questionId;
    }
    req.type = this.questionType.toUpperCase();
    req.language = this.language;
    switch (this.questionType.toLowerCase()) {
      case 'sco':
        req.question = this.nullIfEmpty(this.sco.question);
        req.questionHtml = this.nullIfEmpty(this.sco.question_html);
        req.options = this.sco.options.slice(0, this.sco.numOptions).map(e => this.nullIfEmpty(e));
        req.correctAnswer = this.nullIfEmpty(parseInt(this.sco.correct_answer));
        req.score = this.sco.score;
        req.explanation = this.nullIfEmpty(this.sco.explanation);
        req.scene = this.nullIfEmpty(this.sco.scene);
        req.scenario = this.nullIfEmpty(this.sco.scenario);
        req.tags = this.nullIfEmpty(this.sco.tags);
        break;

      case 'mco':
        req.question = this.nullIfEmpty(this.mco.question);
        req.questionHtml = this.nullIfEmpty(this.mco.question_html);
        req.options = this.mco.options.slice(0, this.mco.numOptions).map(e => this.nullIfEmpty(e));
        const arr = [];
        for (let i = 0; i < 6; i++) {
          if (this.mco.correct_answer[i]) {
            arr.push(i + 1);
          }
        }
        req.correctAnswer = this.nullIfEmpty(arr);
        req.score = this.mco.score;
        req.explanation = this.nullIfEmpty(this.mco.explanation);
        req.scene = this.nullIfEmpty(this.mco.scene);
        req.scenario = this.nullIfEmpty(this.mco.scenario);
        req.tags = this.nullIfEmpty(this.mco.tags);
        break;

      case 'true_false':
        req.question = this.nullIfEmpty(this.true_false.question);
        req.questionHtml = this.nullIfEmpty(this.true_false.question_html);
        req.options = null;
        req.correctAnswer = this.nullIfEmpty(this.true_false.correct_answer);
        console.log(this.true_false.correct_answer, req.correctAnswer);
        req.score = this.true_false.score;
        req.explanation = this.nullIfEmpty(this.true_false.explanation);
        req.scene = this.nullIfEmpty(this.true_false.scene);
        req.scenario = this.nullIfEmpty(this.true_false.scenario);
        req.tags = this.nullIfEmpty(this.true_false.tags);
        break;

      case 'fill_in_the_blank':
        req.question = this.nullIfEmpty(this.fill_in_the_blank.question);
        req.questionHtml = this.nullIfEmpty(this.fill_in_the_blank.question_html);
        req.options = null;
        req.correctAnswer = this.nullIfEmpty(this.fill_in_the_blank.correct_answer);
        req.score = this.fill_in_the_blank.score;
        req.explanation = this.nullIfEmpty(this.fill_in_the_blank.explanation);
        req.scene = this.nullIfEmpty(this.fill_in_the_blank.scene);
        req.scenario = this.nullIfEmpty(this.fill_in_the_blank.scenario);
        req.tags = this.nullIfEmpty(this.fill_in_the_blank.tags);
        break;
    }

    req.quesImg = null;
    req.quesVid = null;
    req.quesAud = null;
    req.optImg = null;
    req.optVid = null;
    req.optAud = null;
    req.expImg = null;
    req.expVid = null;
    req.expAud = null;

    console.log('Final request body', req);
    return req;
  }

  updateOrCreateQuestion() {
    const req = this.prepareRequestBody();
    if (this.createNewQuestion) {
      this.uploading.next(true);
      this.loadingTasks++;
      this.qServ.createNewQuestion(req)
        .toPromise()
        .then(res => {
          this.uploading.next(false);
          if (res.success) {
            alert('Question created successfully');
            this.router.navigate(['/admin', 'edit-question'], {
              queryParams: {
                createNew: this.createNewQuestion + '',
                subtopic: this.subTopicId,
                className: this.className,
                subjectName: this.subjectName,
                bookName: this.bookName,
                topicName: this.topicName,
                subTopicName: this.subTopicName
              }
            });
            // location.reload();
          } else {
            alert('Question creation failed');
            console.log(res);
          }
        })
        .catch(e => console.error('Caught error', e))
        .finally(() => this.loadingTasks--);
    } else {
      this.uploading.next(true);
      this.loadingTasks++;
      this.qServ.updateQuestion(req)
        .toPromise()
        .then(res => {
          this.uploading.next(false);
          if (res.success) {
            alert('Question updated successfully');
            this.router.navigate(['/admin', 'edit-question'], {
              queryParams: {
                questionId: this.questionId
              }
            });
            // location.reload();
          } else {
            alert('Question updation failed. Reason: ' + res.msg);
            console.log(res);
          }
        })
        .catch(e => console.error('Caught error', e))
        .finally(() => this.loadingTasks--);
    }
  }

  addTag(event: MatChipInputEvent, where): void {
    const input = event.input;
    const value = event.value;
    let arr;

    switch (where) {
      case 'sco': arr = this.sco.tags; break;
      case 'mco': arr = this.mco.tags; break;
      case 'true_false': arr = this.true_false.tags; break;
      case 'fill_in_the_blank': arr = this.fill_in_the_blank.tags; break;
    }
    // Add our tag
    if ((value || '').trim()) {
      arr.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  removeTag(tag, where): void {
    let arr;

    switch (where) {
      case 'sco': arr = this.sco.tags; break;
      case 'mco': arr = this.mco.tags; break;
      case 'true_false': arr = this.true_false.tags; break;
      case 'fill_in_the_blank': arr = this.fill_in_the_blank.tags; break;
    }
    const index = arr.indexOf(tag);

    if (index >= 0) {
      arr.splice(index, 1);
    }
  }

  classFieldChanged() {
    if (!this.classField) {
      this.subjectOptions.next([]);
      this.bookOptions.next([]);
      this.topicOptions.next([]);
      this.subjectField = null;
      this.subjectFieldChanged();
    } else {
      this.loadingTasks++;
      this.qServ.fetchSubjects(this.classField)
        .toPromise()
        .then(ret => {
          console.log('Received subjects', ret);
          if (ret.success) {
            this.subjectOptions.next(ret.data);
            this.subjectField = null;
            this.subjectFieldChanged();
          } else {
            alert('Failed to fetch subjects');
          }
        })
        .finally(() => this.loadingTasks--);
    }
  }

  subjectFieldChanged() {
    if (!this.subjectField) {
      console.log('subjectfieldchanged');
      this.bookOptions.next([]);
      this.topicOptions.next([]);
      this.bookField = null;
      this.bookFieldChanged();
    } else {
      this.loadingTasks++;
      this.qServ.fetchBooks(this.subjectField, this.classField)
        .toPromise()
        .then(ret => {
          console.log('Received books', ret);
          if (ret.success) {
            this.bookOptions.next(ret.data);
            this.bookField = null;
            this.bookFieldChanged();
          } else {
            alert('Failed to fetch books');
          }
        })
        .finally(() => this.loadingTasks--);
    }
  }

  bookFieldChanged() {
    if (!this.bookField) {
      console.log('bookfieldchanged');
      this.topicOptions.next([]);
      this.topicField = null;
      this.topicFieldChanged();
    } else {
      this.loadingTasks++;
      this.qServ.fetchTopics(this.bookField)
        .toPromise()
        .then(ret => {
          console.log('Received topics', ret);
          if (ret.success) {
            this.topicOptions.next(ret.data);
            this.topicField = null;
            this.topicFieldChanged();
          } else {
            alert('Failed to fetch topics');
          }
        })
        .finally(() => this.loadingTasks--);
    }
  }

  topicFieldChanged() {
    if (!this.topicField) {
      console.log('topicfieldchanged');
    } else {
      this.loadingTasks++;
      this.qServ.fetchSubtopics(this.bookField, this.topicField)
        .toPromise()
        .then(ret => {
          console.log('Received subtopics', ret);
          if (ret.success) {
            this.subtopicOptions.next(ret.data);
            this.subTopicId = null;
            this.subtopicFieldChanged();
          }
        })
        .finally(() => this.loadingTasks--);
    }
  }

  subtopicFieldChanged() {
    if (!this.subTopicId) {

    } else {

    }
  }

  nullIfEmpty(str: any) {
    if (['number', 'boolean'].includes(typeof str)) {
      return str;
    }
    return !str || !str.length || (str.length === 0) ? null : str;
  }

  emptyIfNull(str: string) {
    if (str === null || str === undefined) {
      return '';
    } else {
      return str;
    }
  }

  applyHashStr() {
    const opts = this.hashStr.split('#');
    switch (this.questionType.toLowerCase()) {
      case 'sco':
        this.sco.numOptions = opts.length;
        for (let i = 0; i < opts.length; i++) {
          this.sco.options[i] = opts[i];
        }
        for (let i = opts.length; i < 6; i++) {
          this.sco.options[i] = '';
        }
        break;
      case 'mco':
        this.mco.numOptions = opts.length;
        for (let i = 0; i < opts.length; i++) {
          this.mco.options[i] = opts[i];
        }
        for (let i = opts.length; i < 6; i++) {
          this.mco.options[i] = '';
        }
        break;
    }
  }

  previewQuestion() {
    const req = this.prepareRequestBody();
    this.dialog.open(QuestionPreviewComponent, {
      width: '80vw',
      // height: '80vh',
      panelClass: 'preview-question-panel',
      data: {
        question: req,
        meta: {
          schoolSubjectName: this.subjectName,
          topicName: this.topicName,
          subtopicName: this.subTopicName
        }
      }
    });
  }
}
