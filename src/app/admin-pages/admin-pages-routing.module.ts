import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeLayoutComponent } from '../shared/_components/layouts/home-layout/home-layout.component';
import { ResourcesComponent } from './resources/resources.component';
import { QuestionsComponent } from './questions/questions.component';
import { BookDetailsComponent } from './book-details/book-details.component';
import { EditQuestionComponent } from './edit-question/edit-question.component';
import { QuestionPreviewComponent } from '../shared/_components/question-preview/question-preview.component';


const routes: Routes = [{
  path: '',
  component: HomeLayoutComponent,
  children: [
    { path: 'resources', component: ResourcesComponent },
    { path: 'resources/:resourceType', component: ResourcesComponent },
    { path: 'book-details', component: BookDetailsComponent },
    { path: 'questions', component: QuestionsComponent },
    { path: 'edit-question', component: EditQuestionComponent },
    { path: 'preview-question', component: QuestionPreviewComponent },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminPagesRoutingModule { }
