import { NgModule } from '@angular/core';

import { AttendanceRoutingModule } from './attendance-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AttendanceComponent } from './attendance.component';


@NgModule({
  declarations: [
    AttendanceComponent
  ],
  imports: [
    SharedModule,
    AttendanceRoutingModule
  ]
})
export class AttendanceModule { }
