import { Component, OnInit } from '@angular/core';
import { AttendanceService } from 'src/app/shared/_services/attendance.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from 'src/app/shared/_services/auth.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
declare var $;

export class AttendanceReqObj {
  user_id: any;
  month: any;
  year: any;
}
@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {

  attendanceObj: AttendanceReqObj = new AttendanceReqObj();
  date = new Date();
  startDay: any;
  lastDate: any;
  todayDay: any;
  currentUserData;
  userId = '';
  dayList = [{ 'id': 1, 'name': 'MON' }, { 'id': 2, 'name': 'TUE' }, { 'id': 3, 'name': 'WED' },
             { 'id': 4, 'name': 'THU' }, { 'id': 5, 'name': 'FRI' }, { 'id': 6, 'name': 'SAT' }, 
             { 'id': 7, 'name': 'SUN' }];
  monthList = [
    { 'id': 1, 'name': 'January' }, { 'id': 2, 'name': 'February' }, { 'id': 3, 'name': 'March' },
    { 'id': 4, 'name': 'April' }, { 'id': 5, 'name': 'May' }, { 'id': 6, 'name': 'June' },
    { 'id': 7, 'name': 'July' }, { 'id': 8, 'name': 'August' }, { 'id': 9, 'name': 'September' },
    { 'id': 10, 'name': 'October' }, { 'id': 11, 'name': 'November' }, { 'id': 12, 'name': 'December' }
  ];
  dateList = [];
  rowDateList = [];
  lastDay: number;
  selected: any = 'Acamedic year';
  selectedMonth = '';
  modalRef: any;
  currentYear: any;
  currentMonth: any;
  yearList = [
    {id: 1, year: 2020}, {id: 2, year: 2021}
  ];

  constructor(private authenticationService: AuthenticationService, private attendanceService: AttendanceService,
    private toaster: ToastrService, private modalService: NgbModal) {
      this.currentYear = this.date.getFullYear();
      this.currentMonth = this.date.getMonth() + 1;
    }

  ngOnInit() {
    this.currentUserData = this.authenticationService.currentUserValue;
    this.userId = this.currentUserData.user_id;
    this.attendanceObj.month = this.date.getMonth() + 1;
    this.attendanceObj.year = this.date.getFullYear();
    let index = this.monthList.findIndex(v => v.id == this.attendanceObj.month);
    this.selectedMonth = this.monthList[index].name;
    this.todayDay = this.date.getDay();
    this.onFilterApply();
  }

  setCalendar() {
    this.rowDateList = [];
    this.dateList = [];
    let setFirstDay = false;
    if (this.lastDate) {
      let i = 1;
      let j = 0;
      let list = [];
      for (j = 0; j < this.dayList.length;) {
        if (i != 1 && i <= this.lastDate && setFirstDay) {
          this.dateList.push({ 'day': this.dayList[j].id, 'date': i });
          list.push({ 'day': this.dayList[j].id, 'date': i });
          i++;
          j++;
        } else if (i == 1 && !setFirstDay && this.startDay == this.dayList[j].id) {
          setFirstDay = true;
          this.dateList.push({ 'day': this.dayList[j].id, 'date': i });
          list.push({ 'day': this.dayList[j].id, 'date': i });
          i++;
          j++;
        } else if ((i == 1 && !setFirstDay && this.startDay !== this.dayList[j].id) || (i > this.lastDate)) {
          this.dateList.push({ 'day': this.dayList[j].id, 'date': '' });
          list.push({ 'day': this.dayList[j].id, 'date': '' });
          j++;
        }
        if (j == 7 && i <= this.lastDate) {
          this.rowDateList.push(list);
          list = [];
          j = 0;
        }
        if (j == 7 && i > this.lastDate) {
          this.rowDateList.push(list);
          list = [];
        }
      }
    }
  }
  onFilterApply() {
    let month = this.attendanceObj.month;
    this.startDay = (new Date(this.attendanceObj.year, month - 1, 1)).getDay();
    this.lastDate = (new Date(this.attendanceObj.year, this.attendanceObj.month, 0)).getDate();
    this.lastDay = (new Date(this.attendanceObj.year, month - 1, 0)).getDay();
    if (!this.startDay) {
      this.startDay = 7;
    }
    if (!this.lastDay) {
      this.lastDay = 7;
    }
    this.setCalendar();
    this.getAttendance();
  }
  getAttendance() {
    this.attendanceService.getAttendance(this.userId, this.attendanceObj.month, this.attendanceObj.year).subscribe((res: any) => {
      if (res && res.success) {
        let data = res.data;
        if (data && data.length) {
          if (this.dateList && this.dateList.length) {
            data.forEach((ele, index) => {
              let i = this.dateList.findIndex(v => v.date == ele[0]);
              if (i > -1) {
                this.dateList[i]["type"] = ele[3];
              }
              if (index === data.length - 1) {
                this.generateRowList();
              }
            });
          }
        }
      }
    })
  }
  generateRowList() {
    this.rowDateList = [];
    if (this.dateList && this.dateList.length) {
      let list = [];
      let i = 0;
      for (let j = 0; j < this.dayList.length;) {
        if (j < 7 && i < this.dateList.length) {
          list.push(this.dateList[i]);
          i++;
          j++;
        }
        if (j == 7 && i <= this.lastDate) {
          this.rowDateList.push(list);
          list = [];
          j = 0;
        }
        if (j == 7 && i > this.lastDate) {
          this.rowDateList.push(list);
          list = [];
        }
      }
    }
  }
  onYearChange(value) {
    this.attendanceObj.year = value;
    this.onFilterApply();
  }
  onMonthChange(value) {
    this.attendanceObj.month = value.id;
    this.selectedMonth = value.name;
    this.onFilterApply();
  }
}
