import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_material from "@amcharts/amcharts4/themes/material";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";import { TeacherService } from 'src/app/shared/_services/teacher.service';
am4core.useTheme(am4themes_material);
am4core.useTheme(am4themes_animated);
am4core.options.autoSetClassName = true;
@Component({
  selector: 'app-teacher-performance-charts',
  templateUrl: './teacher-performance-charts.component.html',
  styleUrls: ['./teacher-performance-charts.component.css']
})
export class TeacherPerformanceChartsComponent implements OnInit {
@Input() data :any;
@Input() id :any;
  constructor() { 
  }
  ngOnInit() {
  }
  ngAfterViewInit(){
        this.getChart()
  }
  getChart() {
    let chart = am4core.create(this.id, am4charts.RadarChart);
    chart.data = this.data;
    // Make chart not full circle
    chart.startAngle = -90;
    chart.endAngle = 270;
    chart.innerRadius = am4core.percent(20);
    // chart.height=327;
    // chart.width=480;
    chart.paddingRight = 0;
    chart.paddingLeft = 0;
    chart.paddingTop = 0;
    chart.paddingBottom = 0;
    // Set number format
    chart.numberFormatter.numberFormat = "#.#'%'";

    // Create axes
    var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis() as any);
    categoryAxis.dataFields.category = "category";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.grid.template.strokeOpacity = 0;
    categoryAxis.renderer.labels.template.horizontalCenter = "right";
    categoryAxis.renderer.labels.template.fontWeight = 500;
    categoryAxis.renderer.labels.template.adapter.add("fill", function(
      fill,
      target
    ) {
      return target.dataItem.index >= 0
        ? chart.colors.getIndex(target.dataItem.index)
        : fill;
    });
    categoryAxis.renderer.minGridDistance = 10;

    var valueAxis = chart.xAxes.push(new am4charts.ValueAxis() as any);
    valueAxis.renderer.grid.template.strokeOpacity = 0;
    valueAxis.min = 0;
    valueAxis.max = 100;
    valueAxis.strictMinMax = true;

    // Create series
    var series1 = chart.series.push(new am4charts.RadarColumnSeries());
    series1.dataFields.valueX = "full";
    series1.dataFields.categoryY = "category";
    series1.clustered = false;
    series1.columns.template.fill = new am4core.InterfaceColorSet().getFor(
      "alternativeBackground"
    );
    series1.columns.template.fillOpacity = 0.08;
    // series1.columns.template.cornerRadiusTopLeft = 20;
    series1.columns.template.strokeWidth = 0;
    series1.columns.template.radarColumn.cornerRadius = 20;

    var series2 = chart.series.push(new am4charts.RadarColumnSeries());
    series2.dataFields.valueX = "value";
    series2.dataFields.categoryY = "category";
    series2.clustered = false;
    series2.columns.template.strokeWidth = 0;
    series2.columns.template.tooltipText = "{category}: [bold]{value}[/]";
    series2.columns.template.radarColumn.cornerRadius = 20;

    series2.columns.template.adapter.add("fill", function(fill, target) {
      return chart.colors.getIndex(target.dataItem.index);
    });

    chart.legend = new am4charts.Legend();
    chart.legend.reverseOrder = true;
    series2.events.on("dataitemsvalidated", function() {
      var data = [];
      series2.dataItems.each(function(dataItem) {
        data.push({
          name: dataItem.categoryY,
          fill: dataItem.column.fill,
          seriesDataItem: dataItem
        });
      });
      chart.legend.data = data;
      chart.legend.maxWidth = undefined;
      chart.legend.position = "right";
      chart.legend.itemContainers.template.events.on("toggled", function(
        event
      ) {
        var seriesDataItem = (event.target.dataItem.dataContext as any)
          .seriesDataItem;
        if (event.target.isActive) {
          seriesDataItem.hide(series2.interpolationDuration, 0, 0, ["valueX"]);
        } else {
          seriesDataItem.show(series2.interpolationDuration, 0, ["valueX"]);
        }
      });
    });
  }
}
