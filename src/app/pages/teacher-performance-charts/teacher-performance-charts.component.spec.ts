import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherPerformanceChartsComponent } from './teacher-performance-charts.component';

describe('TeacherPerformanceChartsComponent', () => {
  let component: TeacherPerformanceChartsComponent;
  let fixture: ComponentFixture<TeacherPerformanceChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherPerformanceChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherPerformanceChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
