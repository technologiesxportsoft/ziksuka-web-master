import { NgModule } from '@angular/core';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashboardComponent } from './dashboard.component';
import { RecommendationsDashboardComponent } from './recommendations-dashboard/recommendations.dashboard.component';
import { BooksDashboardComponent } from './books-dashboard/books-dashboard.component';
import { PerformanceComponent } from '../performance/performance.component';
import { FeedComponent } from '../feed/feed.component';
import { TeacherSlotsComponent } from '../teacher-slots/teacher-slots.component';
import { StudentsSyllabusPerformanceComponent } from '../students-syllabus-performance/students-syllabus-performance.component';
import { StudentAttendenceComponent } from '../student-attendence/student-attendence.component';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { GradeTestConfigurationComponent } from 'src/app/shared/_components/grade-test-configuration/grade-test-configuration.component';
import { TeacherSyllabusPerformanceComponent } from '../teacher-syllabus-performance/teacher-syllabus-performance.component';
import { PrincipalDashboardInfoComponent } from '../principal-dashboard-info/principal-dashboard-info.component';
import { TeacherPerformanceChartsComponent } from '../teacher-performance-charts/teacher-performance-charts.component';

@NgModule({
  declarations: [
    DashboardComponent,
    RecommendationsDashboardComponent,
    BooksDashboardComponent,
    PerformanceComponent,
    FeedComponent,
    TeacherSlotsComponent,
    StudentsSyllabusPerformanceComponent,
    StudentAttendenceComponent,
    TeacherSyllabusPerformanceComponent,
    PrincipalDashboardInfoComponent,
    TeacherPerformanceChartsComponent,
  ],
  imports: [
    SharedModule,
    DashboardRoutingModule
  ],
  entryComponents: [GradeTestConfigurationComponent],
})
export class DashboardModule {}
