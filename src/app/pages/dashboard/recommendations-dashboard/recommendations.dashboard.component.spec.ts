import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecommendationsDashboardComponent } from './recommendations.dashboard.component';

describe('RecommendationsComponent', () => {
  let component: RecommendationsDashboardComponent;
  let fixture: ComponentFixture<RecommendationsDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RecommendationsDashboardComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecommendationsDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
