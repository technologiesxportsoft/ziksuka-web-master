import { Component, OnInit, ViewChild } from "@angular/core";
import { SlickCarouselComponent } from "ngx-slick-carousel";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DomSanitizer } from "@angular/platform-browser";
import { UserInfo } from "src/app/shared/_model/user-info";
import { DashboardService } from "src/app/shared/_services/dashboard.service";
@Component({
  selector: "app-recommendation-dashboard",
  templateUrl: "./recommendations.dashboard.component.html",
  styleUrls: ["./recommendations.dashboard.component.css"],
})
export class RecommendationsDashboardComponent implements OnInit {
  user: UserInfo;
  recommendations: any = [];
  modalContent: any;
  selectedRecommendation: any;
  zIndex: any;
  selectedRecommendationType = '';
  selectedRecommendationDesc = '';
  selectedTitle = '';

  @ViewChild("slickModal", { static: true }) slickModal: SlickCarouselComponent;

  slideConfig = {
    slidesToShow: 5,
    slidesToScroll: 3,
    infinite: false,
    arrows: false,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 2,
          infinite: false,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
    ],
  };

  constructor(
    private sanitizer: DomSanitizer,
    private dashboardService: DashboardService,
    private bmodalService: NgbModal
  ) {}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("userInfo"));
    this.getRecommendations();
  }

  getRecommendations(subjectId: number = null, topicId: number = null, subtopicId: number = null, origin: String = "DASHBOARD") {
    this.dashboardService.GetRecommendations(this.user.user_id, subjectId, topicId, subtopicId, origin).subscribe(
      (res) => {
        console.log(res);
        var result = res as any;
        this.recommendations = result.data;
      },
      (err) => {
        console.log(err);
      }
    );
  }
  open(recommendation) {
    if (recommendation.type !== "VIDEO") {
      this.modalContent = recommendation.content;
    } else {
      this.modalContent = this.sanitizer.bypassSecurityTrustResourceUrl(
        recommendation.content
      );
    }
  }

  next() {
    this.slickModal.slickNext();
  }
  back() {
    this.slickModal.slickPrev();
  }
  // Let it be here for future Concern START
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
  // Let it be here for future Concern END
}
