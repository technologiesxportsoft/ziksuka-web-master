import { Component, OnInit, ViewChild } from "@angular/core";
import { SlickCarouselComponent } from "ngx-slick-carousel";
import { NgbModal, ModalDismissReasons } from "@ng-bootstrap/ng-bootstrap";
import { DomSanitizer } from "@angular/platform-browser";
import { UserInfo } from "src/app/shared/_model/user-info";
import { DashboardService } from "src/app/shared/_services/dashboard.service";
import { UserRole } from "../../../shared/_enumerations/UserRole";
import { BooksService } from "src/app/shared/_services/books.services";
import {
  GLOBAL_CONSTANT,
  PAGE_NAME,
} from "../../../shared/lib/common/utils/Constants";
@Component({
  selector: "app-books-dashboard",
  templateUrl: "./books-dashboard.component.html",
  styleUrls: ["./books-dashboard.component.css"],
})
export class BooksDashboardComponent implements OnInit {
  public UserRole = UserRole;
  user: UserInfo;
  books: any = [];
  allbooks: any;
  closeResult: any;
  modalContent: any;
  selectedBook: any;
  isSubjectBooks = true;
  selectedBookTitle = "";
  zIndex: any;
  pdfSrc = "../../assets/pdfFiles/Ch_1.pdf";
  @ViewChild("slickModal", { static: true }) slickModal: SlickCarouselComponent;

  slideConfig = {
    slidesToShow: 7,
    slidesToScroll: 3,
    infinite: false,
    arrows: false,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 2,
          infinite: false,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
    ],
  };
  constructor(
    private sanitizer: DomSanitizer,
    private bmodalService: NgbModal,
    private booksService: BooksService
  ) {}

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem("userInfo"));
    this.getBooks();
  }
  getBooks() {
    // TODO : match params with actual request
    // var bookModel: Object = {
    //   user_id: this.user.user_id,
    //   school_grade_id: this.user.school_id,
    //   grade_id: this.user.grade_id,
    // };
    var obj = {
      user_id: this.user.user_id,
      origin: PAGE_NAME.DASHBOARD,
      subject_id: GLOBAL_CONSTANT.ALL_SUBJECTS_ID,
    };
    this.booksService.GetBooks(obj).subscribe(
      (res) => {
        //TODO : currently getting empty array of data
        var result = res as any;
        this.allbooks = result.data;
        if (this.isSubjectBooks) {
          this.books = result.data.subjectBooks;
        } else {
          this.books = result.data.libraryBooks;
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }
  selectBookType() {
    //TODO : add loading on change of it
    this.isSubjectBooks = !this.isSubjectBooks;
    this.books = [];
    if (this.isSubjectBooks) {
      this.books = this.allbooks.subjectBooks;
    } else {
      this.books = this.allbooks.libraryBooks;
    }
    console.log(this.books);
  }
  open(content, book) {
    this.selectedBook = book;
    this.modalContent = book.source;
    this.bmodalService
      .open(content, {
        ariaLabelledBy: "modal-basic-title",
        size: "lg",
        centered: true,
        scrollable: true,
      })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }
  next() {
    this.slickModal.slickNext();
  }
  back() {
    this.slickModal.slickPrev();
  }
  // Let it be here for future Concern START
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return "by pressing ESC";
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return "by clicking on a backdrop";
    } else {
      return `with: ${reason}`;
    }
  }
  // Let it be here for future Concern END
}
