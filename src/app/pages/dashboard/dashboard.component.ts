import { Component, OnInit, ViewChild } from '@angular/core';
import { UserInfo } from 'src/app/shared/_model/user-info';
import { UserRole } from 'src/app/shared/_enumerations/UserRole';
import { AuthenticationService } from 'src/app/shared/_services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public UserRole = UserRole;
  user: UserInfo;
  constructor(private auth: AuthenticationService, private router: Router) {
    if ([UserRole.ContentAdmin, UserRole.ContentDeveloper].includes(this.auth.currentUserValue.role)) {
      this.router.navigate(['/admin', 'questions']);
    }
  }
  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('userInfo'));
    if (this.user.role === this.UserRole.Principal && this.user.teacher === true) {
      if (localStorage.getItem("principalViewAsTeacher") === null) {
        localStorage.setItem("principalViewAsTeacher", 'false')
      } else if (localStorage.getItem("principalViewAsTeacher") === 'true') {
        this.user.role = UserRole.Teacher;
      }
    }
  }
}
