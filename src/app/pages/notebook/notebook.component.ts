import { Component, OnInit, ViewChild } from '@angular/core';
import { CanvasWhiteboardUpdate, CanvasWhiteboardComponent, CanvasWhiteboardService } from 'ng2-canvas-whiteboard';
import { UserRole } from '../../shared/_enumerations/UserRole';
import { UserInfo } from 'src/app/shared/_model/user-info';

@Component({
  selector: 'app-notebook',
  templateUrl: './notebook.component.html',
  styleUrls: ['./notebook.component.css']
})
export class NotebookComponent implements OnInit {
  @ViewChild('canvasWhiteboard', { static: true }) canvasWhiteboard: CanvasWhiteboardComponent;
  public UserRole = UserRole;
  user: UserInfo;

  constructor(private canvasService: CanvasWhiteboardService) {
    // (document.querySelector("canvas") as HTMLElement).style.height="100%";
  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('userInfo'));
  }
  // ngAfterViewInit(){

  // }
  onCanvasClear(update: CanvasWhiteboardUpdate): void {
    // console.log(update);
    this.canvasService.clearCanvas();
  }
  onCanvasUndo() {

  }
  onCanvasRedo() {

  }
}
