import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef, HostListener } from '@angular/core';
import { UserRole } from 'src/app/shared/_enumerations/UserRole';
import { AuthenticationService } from 'src/app/shared/_services/auth.service';
import { ClassroomService } from 'src/app/shared/_services/classroom.service';
import { UserInfo } from 'src/app/shared/_model/user-info';
import { Client, LocalStream } from 'ion-sdk-js'
import { RemoteStream } from 'ion-sdk-js/lib/stream'
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NotepadComponent } from 'src/app/shared/_components/notepad/notepad.component';
import { KeyValue } from '@angular/common';
import { number } from '@amcharts/amcharts4/core';

interface CanvasElement extends HTMLCanvasElement {
	captureStream(): void;
}
type PeerInfo = { name: string, userId: number, role: number, uid: string }
type StudentDetail = { peerInfo: PeerInfo, stream: RemoteStream, audioMuted: boolean, handRaised: boolean };
@Component({
	selector: 'app-classroom',
	templateUrl: './classroom.component.html',
	styleUrls: ['./classroom.component.css']
})
export class ClassroomComponent implements OnInit {
	@ViewChild('classNotebook', { static: false }) notepad: NotepadComponent;
	public UserRole = UserRole;
	teacherRole = false;
	studentRole = false;
	currentUserData: UserInfo;

	requiredPermissionsAvailable = true;
	videoModeNotebook = true;

	classStarted = false;
	classConnected = false;
	classDisconnected = false;
	classEnded = false;
	startClassButtonDisabled = true;
	endClassButtonDisabled = true;
	joinClassButtonDisabled = true;
	leaveClassButtonDisabled = true;
	audioMuted = true;
	videoMuted = true;
	handRaised = false;

	showLoader = true;

	websocketConnection = false;

	cameraModeFront = "user";
	cameraModeBack = "environment";
	cameraMode;

	localStream: LocalStream; remoteStream; canvasStream;
	audioTrack;

	clientId; existingTracks = [];
	hello = false;
	ionClient: Client;

	roomId: string;
	role: UserRole;
	classroomDetails = {
		schoolGradeName: "",
		sectionName: "",
		schoolSubjectName: ""
	};

	students = new Map<string, StudentDetail>();
	teacher;

	canvas; canvasHeight; canvasWidth;

	options = {
		codec: 'VP8',
		resolution: 'hd',
		audio: true,
		video: true,
	}

	heartBeatCount: number = 0;
	heartBeatInterval;

	constructor(private authenticationService: AuthenticationService, private router: Router,
		private classroomService: ClassroomService, private changeDetectorRef: ChangeDetectorRef,
		private toasterService: ToastrService, private route: ActivatedRoute) { }

	ngOnInit() {
		this.currentUserData = this.authenticationService.currentUserValue;

		this.roomId = this.route.snapshot.paramMap.get("classroomId");
		// Reroute on classroom change.
		this.router.routeReuseStrategy.shouldReuseRoute = () => {
			return false;
		}

		this.classroomService.joinLiveClass(this.roomId, this.currentUserData.user_id).subscribe((result) => {
			if (result.success) {
				this.classroomDetails = result.data;
				this.showLoader = false;
			} else {
				this.toasterService.error(result.msg);
				this.router.navigate(['/dashboard']);
				return;
			}
		}, err => {
			this.toasterService.error('Could not join classroom!');
			this.router.navigate(['/dashboard']);
			return;
		});

		if (this.currentUserData.role == UserRole.Teacher) {
			this.teacherRole = true;
			this.cameraMode = this.cameraModeFront;
			this.role = UserRole.Teacher;
			this.audioMuted = false;
		} else if (this.currentUserData.role == UserRole.Student) {
			this.studentRole = true;
			this.role = UserRole.Student;
		} else {
			// Should be unreachable, redirect/logout.
		}

		this.clientId = this.role + "-" + this.currentUserData.user_id;
		this.requiredPermissionsAvailable = true;
		this.checkForPermissions();
		this.changeDetectorRef.detectChanges();

		if (this.teacherRole) {
			this.canvas = this.notepad.canvas;
		}

		// For handling chrome bug.
		window.onbeforeunload = this.onUnload;

	}

	getValues(map){
		return Array.from(map.values());
	}

	@HostListener('window:beforeunload')
	@HostListener('window:unload')
	// @HostListener('window:unload')
	onUnload() {
		console.log("destroying");
		this.disconnectFromClass();
	}

	ngOnDestroy() {
		this.disconnectFromClass();
	}

	encodeIonInfo() {
		return {
			name: JSON.stringify({
				'name': this.currentUserData.first_name + " " + this.currentUserData.last_name,
				'userId': this.currentUserData.user_id,
				'role': this.role,
				'uid': this.clientId
			})
		};
	}

	decodeIonInfo(info) {
		let ionInfo = JSON.parse(info.name);
		let peerInfo = {
			name: ionInfo.name,
			userId: ionInfo.userId,
			role: ionInfo.role,
			uid: ionInfo.uid
		}
		return peerInfo;
	}

	checkForPermissions() {
		var constraints: any;
		if (this.teacherRole) {
			constraints = {
				audio: true,
				// video: {
				// 	facingMode: this.cameraMode
				// }
			}
		} else {
			constraints = {
				audio: true
			}
		}

		var nav = window.navigator as any
		nav.getWebcam = (nav.getUserMedia || nav.webKitGetUserMedia || nav.moxGetUserMedia || nav.mozGetUserMedia || nav.msGetUserMedia);
		if (nav.mediaDevices.getUserMedia) {
			nav.mediaDevices.getUserMedia(constraints)
				.then((stream) => {
					this.requiredPermissionsAvailable = true;
					this.enableStart();
					if (this.teacherRole) {
						this.canvasStream = this.canvas.nativeElement.captureStream();
						for (const track of stream.getTracks()) {
							console.log("Sending Stream.");
							if (track.kind == 'audio') {
								this.audioTrack = track;
							}
						}
						this.canvasStream.addTrack(this.audioTrack);
						this.localStream = new LocalStream(this.canvasStream, this.options);
					} else {
						console.log("Student sending stream");
						this.localStream = new LocalStream(stream, this.options);
					}
				})
				.catch((e) => {
					this.requiredPermissionsAvailable = false;
					console.log(e.name + ": " + e.message);
				});
		} else {
			nav.getWebcam({ audio: true, video: true },
				(stream) => {
					this.requiredPermissionsAvailable = true;
					this.enableStart();
					this.localStream = stream;
				},
				() => {
					this.requiredPermissionsAvailable = false;
					console.log("Web cam is not accessible.");
				}
			);
		}
	}


	async startClass() {
		await this.initiateConnection();
		this.connectToClass();
		this.heartBeatInterval = setInterval(() => {
			this.startHeartBeat();
		}, 1000);
	}

	endClass() {
		// Drop off
		this.disconnectFromClass();
	}

	async joinClass() {
		await this.initiateConnection();
		this.connectToClass();
	}

	leaveClass() {
		this.disconnectFromClass();
	}

	startHeartBeat() {
		if (this.studentRole) {
			console.log("Illegal State!");
			return;
		}
		this.heartBeatCount++;
		switch (this.heartBeatCount) {
			case 1:
				this.removeHeartBeat();
				this.smallCircle();
				break;
			case 2:
				this.mediumCircle();
				this.heartBeatCount = 0;
				break;
			case 3:
				this.bigCircle();
				break;

		}
	}

	removeHeartBeat() {
		let context = this.canvas.nativeElement.getContext('2d');
		context.clearRect(1258, 698, 22, 22);
		context.fillStyle = "#fff";
		context.fillRect(1258, 698, 22, 22);
	}

	smallCircle() {
		let context = this.canvas.nativeElement.getContext('2d');
		context.strokeStyle = "#b30000";
		context.beginPath();
		context.arc(1271, 711, 4, 0, 2 * Math.PI);
		context.fillStyle = "#b30000";
		context.fill();
		context.stroke();
	}

	mediumCircle() {
		let context = this.canvas.nativeElement.getContext('2d');
		context.strokeStyle = "#b30000";
		context.beginPath();
		context.arc(1271, 711, 8, 0, 2 * Math.PI);
		context.stroke();
	}

	bigCircle() {
		let context = this.canvas.nativeElement.getContext('2d');
		context.strokeStyle = "#b30000";
		context.beginPath();
		context.arc(10, 10, 15, 0, 2 * Math.PI);
		context.stroke();
	}

	async sleepDelay(seconds: number) {
		console.log('Sleep for ' + seconds + ' seconds');
		return new Promise(resolve => setTimeout(() => resolve(), seconds * 1000)).then();
	}

	async initiateConnection() {
		if (this.ionClient) {
			console.log("Client connection exists.");
			return;
		}

		this.disableStart();

		this.ionClient = new Client({
			url: this.classroomService.getIonServer(),
			uid: this.clientId, rtc: this.classroomService.getRtcConfiguration()
		});

		// Define ion handlers.
		this.ionClient.on('peer-join', (uid: string, info: any) => {
			this.handlePeerJoin(uid, info)
		});
		this.ionClient.on('stream-add', (mid: string, info: any, tracks: any) => {
			this.handleStreamAdd(mid, info, tracks);
		});
		this.ionClient.on('peer-leave', (uid: string) => {
			this.handlePeerLeave(uid);
		});
		this.ionClient.on('transport-open', () => {
			this.handleTransportOpen();
		});
		this.ionClient.on('transport-closed', () => {
			this.handleTransportClose();
		});
		this.ionClient.on('stream-remove', (stream: RemoteStream) => {
			this.handleStreamRemove(stream);
		});
		this.ionClient.on('broadcast', (uid: string, info: any) => {
			this.handleBroadcast(uid, info);
		});

		// Wait for connection for 5s.
		let retries = 1;
		while (!this.websocketConnection) {
			await this.sleepDelay(1);
			if (retries++ == 5) {
				this.toasterService.error("Could not connect to class. Please check your connection or refresh the page.");
				this.enableStart();
				return;
			}
		}

		console.log("Connected to socket.");

	}

	connectToClass() {
		if (!this.websocketConnection) {
			console.log("Could not connect to socket, check internet");
			return;
		}
		if (this.classConnected) {
			console.log("Class already connected.");
			return;
		}
		this.classConnected = true;

		// Start with audio muted for students.
		if (this.audioMuted) {
			this.muteAudio();
		}

		this.ionClient.join(this.roomId, this.encodeIonInfo()).then(() => {

			if (this.teacherRole) {
				this.ionClient.publish(this.localStream);
				console.log("Publishing teacher's stream - " + this.canvasStream);
			} else {
				this.ionClient.publish(this.localStream);
				console.log("Publishing students's stream - " + this.localStream);
			}

			this.toasterService.success("Connected to the classroom");

		});

	}

	muteAllStudents() {
		if (!this.teacherRole) {
			console.log("Only teacher allowed to mute class.");
			return;
		}
		let muteCmd = {
			'action': 'mute',
			'uid': 'all'
		}
		this.broadcast(muteCmd);
		this.students.forEach((student, uid) => {
			student.audioMuted = true;
		});
		if (this.classConnected) {
			this.toasterService.info("Muted all students");
		}
	}

	unmuteAllStudents() {
		if (!this.teacherRole) {
			console.log("Only teacher allowed to unmute class.");
			return;
		}
		let unmuteCmd = {
			'action': 'unmute',
			'uid': 'all'
		}
		this.broadcast(unmuteCmd);
		this.students.forEach((student, uid) => {
			student.audioMuted = false;
		});
		this.toasterService.info("Unmuted all students");
	}

	muteStudent(uid) {
		let muteCmd = {
			'action': 'mute',
			'uid': uid
		}
		this.broadcast(muteCmd);
		let student = this.students.get(uid);
		student.audioMuted = true;
		this.toasterService.info("Muted " + student.peerInfo.name);
	}

	unmuteStudent(uid) {
		let unmuteCmd = {
			'action': 'unmute',
			'uid': uid
		}
		this.broadcast(unmuteCmd);
		let student = this.students.get(uid);
		student.audioMuted = false;
		this.toasterService.info("Unmuted " + student.peerInfo.name);
	}

	/**
	 * When student wants to raise hand.
	 */
	raiseMyHand() {
		if (!this.studentRole) {
			console.log("only students can raise hand!");
			return;
		}
		let raiseHandCmd = {
			action: 'raiseHand',
			uid: this.clientId
		}
		this.broadcast(raiseHandCmd);
		this.handRaised = true;
	}

	/**
	 * When teacher wants to lower student's hand.
	 */
	lowerMyHand(cmdByTeacher: boolean) {
		if (!this.studentRole) {
			console.log("Illegal State!");
			return;
		}
		this.handRaised = false;

		if (!cmdByTeacher) {
			let lowerHandCmd = {
				'action': 'selfLowerHand',
				'uid': this.clientId
			}
			this.broadcast(lowerHandCmd);
		}
	}

	muteMyAudio() {
		if (!this.studentRole) {
			console.log("Illegal State!");
			return;
		}
		let muteCmd = {
			'action': 'selfMute',
			'uid': this.clientId
		}
		this.broadcast(muteCmd);
		this.muteAudio();
		// this.toasterService.info("Muted all students");
	}	

	/**
	 * Notify teacher of hand raised by student uid.
	 * @param uid 
	 */
	handleRaisedHand(uid: string) {
		if (!this.teacherRole) {
			console.log("Illegal state");
			return;
		}
		let student = this.students.get(uid);
		student.handRaised = true;
		this.toasterService.warning(student.peerInfo.name + " has raised their hand");
	}

	/**
	 * Notify teacher of hand lowered by student uid.
	 * @param uid 
	 */
	handleLowerHand(uid: string) {
		if (!this.teacherRole) {
			console.log("Illegal state");
			return;
		}
		let student = this.students.get(uid);
		student.handRaised = false;
	}

	/**
	 * Notify teacher of audio muted by student uid.
	 * @param uid 
	 */
	handleStudentMute(uid: string) {
		if (!this.teacherRole) {
			console.log("Illegal state");
			return;
		}
		let student = this.students.get(uid);
		student.audioMuted = true;
	}

	/**
	 * Lower hand of the students
	 * @param uid
	 */
	lowerHand(uid: string) {
		if (!this.teacherRole) {
			console.log("Illegal state");
			return;
		}
		let lowerHandCmd = {
			action: 'lowerHand',
			uid: uid
		}
		this.broadcast(lowerHandCmd);
		let student = this.students.get(uid);
		student.handRaised = false;
	}

	/**
	 *  Lower hand of all the students
	 */
	lowerAllHands() {
		if (this.studentRole) {
			console.log("Illegal state");
			return;
		}
		let raiseHandCmd = {
			action: 'lowerHand',
			uid: 'all'
		}
		this.broadcast(raiseHandCmd);
		this.students.forEach((student, uid) => {
			student.handRaised = false;
		});
		this.toasterService.info("Lowered all hands");
	}

	/**
	 * A new peer joing the room.
	 * @param uid 
	 * @param info 
	 */
	handlePeerJoin(uid: string, info: any) {
		console.log("A new peer has joined - " + uid + info);
		let peerInfo = this.decodeIonInfo(info);
		if (peerInfo.role == UserRole.Student) {
			console.log("A student has joined");
			this.addStudent(peerInfo);
		} else if (peerInfo.role == UserRole.Teacher) {
			console.log("The teacher has joined");
			this.setTeacher(peerInfo, null);
			this.toasterService.info("The teacher has joined the class");
		} else {
			// Should not happen ever.
		}
	}

	handlePeerLeave(uid: string) {
		console.log('Peer left - ' + JSON.stringify(uid));

		if (this.studentRole && uid == this.teacher.uid) {
			console.log("The teacher left");
			this.unsetTeacher();
		} else {
			this.removeStudent(uid);
			if (this.teacherRole) {
				let student = this.students.get(uid);
				this.toasterService.warning(student.peerInfo.name + " has left the class");
			}
		}
	}

	handleStreamAdd(mid: string, info: any, tracks: any) {
		console.log("A new stream has joined - " + mid + JSON.stringify(info) + "-tr -" + JSON.stringify(tracks))
		let peerInfo = this.decodeIonInfo(info);
		this.ionClient.subscribe(mid).then((stream) => {
			if (this.teacherRole) {
				if (peerInfo.role == UserRole.Teacher) {
					console.log("Unexpected teacher's stream");
				} else if (peerInfo.role == UserRole.Student) {
					this.addStudent(peerInfo);
					this.subscribeStream(peerInfo, stream);
				} else {
					// Should be unreachable.
					console.log("Unreachable statement.");
				}
			} else if (this.studentRole && peerInfo.uid != this.clientId) {
				if (peerInfo.role == UserRole.Teacher) {
					console.log("Teacher started streaming.");
					this.setTeacher(peerInfo, stream);
				} else if (peerInfo.role == UserRole.Student) {
					this.addStudent(peerInfo);
					this.subscribeStream(peerInfo, stream);
				} else {
					// Should be unreachable.
					console.log("Unreachable statement.");
				}
			} else {
				// Should be unreachable.
				console.log("Unreachable statement.");
			}
		});
	}

	handleStreamRemove(stream: RemoteStream) {
		console.log('Stream removed - ' + JSON.stringify(stream));
		this.unsubscribeStream(stream);
	}

	handleTransportOpen() {
		console.log('Transport open - ');
		this.websocketConnection = true;
	}

	handleTransportClose() {
		console.log('Transport closed - ');
	}

	handleBroadcast(uid: string, info: any) {
		console.log('Broadcast - ' + uid + " " + JSON.stringify(info))
		if (this.teacherRole) {
			if (info.action == 'raiseHand') {
				this.handleRaisedHand(uid);
			} else if (info.action == 'selfMute') {
				this.handleStudentMute(uid);
			} else if (info.action == 'selfLowerHand') {
				this.handleLowerHand(uid);
			}
			return;
		}
		// For students.
		if (this.teacher.uid == uid) {
			if (info.action == 'mute' && (info.uid == this.clientId || info.uid == 'all')) {
				this.muteAudio();
				this.toasterService.info("The teacher has muted your audio");
			} else if (info.action == 'unmute' && (info.uid == this.clientId || info.uid == 'all')) {
				this.unmuteAudio();
				this.toasterService.warning("The teacher has unmuted your audio");
			} else if (info.action == 'lowerHand' && (info.uid == this.clientId || info.uid == 'all')) {
				this.lowerMyHand(true);
				this.toasterService.info("The teacher has lowered your hand");
			} else if (info.action == 'endClass') {
				console.log("Ending class")
				this.classEnded = true;
				this.unsetTeacher();
				this.toasterService.info("The teacher has ended the class");
			}
		} else {
			console.log("User other than teacher trying to broadcast - " + uid);
			return;
		}
	}

	muteAudio() {
		console.log("muting my stream");
		this.localStream.mute("audio");
		this.audioMuted = true;
	}

	unmuteAudio() {
		console.log("unmuting my stream");
		this.localStream.unmute("audio");
		this.audioMuted = false;
	}

	/**
	 * 
	 * @param info Broadcast info to room
	 */
	broadcast(info: any) {
		this.ionClient.broadcast(info);
	}

	/**
	 * Will handle addition of streams from remote. Update the subscribedStreams map.
	 * @param peerInfo 
	 * @param stream 
	 */
	subscribeStream(peerInfo: PeerInfo, stream: RemoteStream) {
		let student = this.students.get(peerInfo.uid);
		student.stream = stream;
	}

	/**
	 * Will handle removal of streams from remote. Update the subscribedStreams map.
	 * @param stream 
	 */
	unsubscribeStream(stream: RemoteStream) {
		this.students.forEach((studentDetail, uid) => {
			if (studentDetail.stream.mid === stream.mid) {
				try {
					stream.unsubscribe();
				} catch (error) {
					console.error(error);
				}
				this.students.delete(uid);
				return;
			}
		});
	}

	/**
	 * Will unsubscribe to all streams, should be called when leaving the room.
	 */
	unsubscribeAllStreams() {
		this.students.forEach((student, uid) => {
			try {
				student.stream.unsubscribe();
			} catch (error) {
				console.error(error);
			}
			this.students.delete(uid);
		});
	}

	/**
	 * Will handle addition of student to class. Update the students map.
	 * @param peerInfo 
	 */
	addStudent(peerInfo: PeerInfo) {
		let studentDetail = { peerInfo: peerInfo, stream: null, audioMuted: true, handRaised: false };
		this.students.set(peerInfo.uid, studentDetail);
	}

	/**
	 * Will handle leaving of student from class. Update the students map.
	 * @param peerInfo 
	 */
	removeStudent(uid: string) {
		let rs = this.students.get(uid).stream;
		try {
			rs.unsubscribe();
		} catch (error) {
			console.error(error);
		}
		this.students.delete(uid);
	}

	/**
	 * Will handle addition of teacher to class. Update the teacher object.
	 * @param peerInfo 
	 */
	setTeacher(peerInfo: PeerInfo, stream: RemoteStream) {
		this.teacher = peerInfo;
		this.classStarted = true;
		if (!!stream) {
			console.log("Setting teacher stream.........")
			this.remoteStream = stream;
		}
	}

	/**
	 * Will handle leaving of teacher to class. Update the teacher object.
	 */
	unsetTeacher() {
		this.teacher = null;
		this.remoteStream = undefined;
	}


	disconnectFromClass() {

		if (!this.classConnected) {
			console.log("Class already disconnected.");
			return;
		}
		
		if (!this.canDeactivate()) {
			event.preventDefault();
			return false;
		}

		if (!!!this.ionClient) {
			return;
		}

		this.classConnected = false;
		if (this.teacherRole) {
			let endClassCmd = {
				action: 'endClass',
				uid: this.clientId
			}
			this.muteAllStudents();
			this.broadcast(endClassCmd);
			this.removeHeartBeat();
		}

		this.ionClient.removeAllListeners();
		this.unsubscribeAllStreams();
		this.ionClient.leave().then(() => {
			this.enableStart();
			this.router.navigate(['/dashboard']);
			this.ionClient.close();
			if (this.studentRole) {
				window.close();
			}
		});

	}

	canDeactivate(): boolean {
		console.log("can deactivate");
		if (!this.classConnected) {
			return true;
		}

		const confirmResult = confirm('Are you sure you want to leave this page?');
		if (confirmResult === true) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Enabling start/join buttons.
	 */
	enableStart() {
		this.startClassButtonDisabled = false;
		this.endClassButtonDisabled = true;
		this.joinClassButtonDisabled = false;
		this.leaveClassButtonDisabled = true;
	}

	/**
	 * Disabling start/join buttons.
	 */
	disableStart() {
		this.startClassButtonDisabled = true;
		this.endClassButtonDisabled = false;
		this.joinClassButtonDisabled = true;
		this.leaveClassButtonDisabled = false;
	}

}
