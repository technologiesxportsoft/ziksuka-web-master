import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClassroomComponent } from './classroom.component';
import { CanDeactivateGuard } from 'src/app/shared/_guards/can-deactivate-classroom-guard';


const routes: Routes = [
  {
    path: '',
    component: ClassroomComponent,
    canDeactivate:[CanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClassroomRoutingModule { }
