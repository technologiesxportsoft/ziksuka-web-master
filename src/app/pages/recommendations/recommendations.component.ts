import { Component, OnInit } from "@angular/core";
import { UserInfo } from "src/app/shared/_model/user-info";
import { RecommendationService } from "src/app/shared/_services/recommendation.service";
import { DomSanitizer } from "@angular/platform-browser";
import { GLOBAL_CONSTANT } from "../../shared/lib/common/utils/Constants";
import { ToastrService } from 'ngx-toastr';
declare var $: any;

@Component({
  selector: "app-recommendation",
  templateUrl: "./recommendations.component.html",
  styleUrls: ["./recommendations.component.css"],
})
export class RecommendationsComponent implements OnInit {
  user: UserInfo;
  Subjects = [];
  Topics = [];
  SubTopics = [];
  showLoader = false;
  RecommedationList = [];
  IsTopicDisable = true;
  IsSubTopicDisable = true;
  PerPageItem = 20;
  SelectedSubjectId = 0;
  TopicId = 0;
  Pagination = [];
  CurrentPage = 1;
  MaxPaginationSize = 0;
  selectedRecommendationType = "";
  selectedRecommendationDesc = "";
  modalContent: any;
  zIndex: any;
  ContentSrc = "";
  selectedTitle = "";
  selectedSchoolSubjectId = null;
  changedSubTopicId: number;
  previousSubjectId: number;
  previousTopicId: number;
  previousSubtopicId: number;
  constructor(
    private recommendationService: RecommendationService,
    private sanitizer: DomSanitizer,
    private toaster: ToastrService
  ) {
    this.user = JSON.parse(localStorage.getItem("userInfo"));
  }

  ngOnInit() {
    this.GetSubjects();
    this.GetRecommendation(); 
    $(document).on("click", "#RecommedationTab > li > a", function (e) {
      $.each($("#RecommedationTab > li > a"), function (x, y) {
        var el = y.attributes["aria-controls"].value;
        $("#" + el)[0].classList.remove("show", "active");
        y.classList.remove(
          "show",
          "active",
          "tab-border-bottom",
          "tab-link-active"
        );
        y.classList.add("tab-link");
      });
      var ele = this.attributes["aria-controls"].value;
      $("#" + ele)[0].classList.add("show", "active");
      this.attributes["aria-selected"].value = "true";
      this.classList.remove("tab-link");
      this.classList.add(
        "show",
        "active",
        "tab-border-bottom",
        "tab-link-active"
      );
      $(this).tab("show");
    });
    $(document).on("click", ".dropdownTopicItem", function () {
      $.each($(".dropdownTopicItem"), function (x, y) {
        y.classList.remove("active-bg-color");
      });
      $(this).addClass("active-bg-color");
      $("#dropdownTopicButton").text(
        $(this).text().length <= 18
          ? $(this).text()
          : $(this).text().substring(0, 18) + "..."
      );
    });
    $(document).on("click", ".dropdownSubTopicItem", function () {
      $.each($(".dropdownSubTopicItem"), function (x, y) {
        y.classList.remove("active-bg-color");
      });
      $(this).addClass("active-bg-color");
      $("#dropdownSubTopicButton").text(
        $(this).text().length <= 18
          ? $(this).text()
          : $(this).text().substring(0, 18) + "..."
      );
    });
    setTimeout(() => {
      this.showLoader = false;
    }, 2000);
  }
  GetSubjects() {
    this.showLoader = true;
    this.recommendationService
      .GetSubjects(this.user.school_grade_section_id, this.user.user_id)
      .subscribe((res) => {
        let ApiResult = res as any;
        this.Subjects.push({
          id: GLOBAL_CONSTANT.ALL_SUBJECTS_ID,
          SubjectName: GLOBAL_CONSTANT.ALL_SUBJECTS,
        });
        for (let subject of ApiResult.data) {
          this.Subjects.push({
            SubjectName: subject.schoolSubjectName,
            id: Number(subject.subjectId),
            schoolGradeSubjectId: subject.schoolGradeSubjectId,
            schoolSubjectId: subject.schoolSubjectId,
          });
        }
      });
  }
  GetTopic(schoolSubjectId: number) {
    this.showLoader = true;
    this.recommendationService
      .GetTopics(this.user.school_grade_section_id, schoolSubjectId, this.user.user_id)
      .subscribe((res) => {
        let ApiResult = res as any;
        this.Topics = [];
        for (let topic of ApiResult.data) {
          this.Topics.push({
            TopicName: topic.topicName,
            id: Number(topic.topicId),
          });
        }
        this.showLoader = false;
      });
  }
  GetSubTopic(schoolSubjectId: number, topicId: number) {
    this.showLoader = true;
    this.recommendationService
      .GetSubTopics(this.user.school_grade_section_id, schoolSubjectId, topicId, this.user.user_id)
      .subscribe((res) => {
        let ApiResult = res as any;
        this.SubTopics = [];
        if (ApiResult.data) {
          for (let subTopic of ApiResult.data) {
            this.SubTopics.push({
              SubTopicName: subTopic.subTopicName,
              id: Number(subTopic.subTopicId),
            });
          }
        } 
        this.showLoader = false;
      });
  }

  GetRecommendation(subjectId: number = null, topicId: number = null, subtopicId: number = null, origin: String = "VID_PAGE") {
    this.showLoader = true;
    this.previousSubjectId = subjectId;
    this.previousTopicId = topicId;
    this.previousSubtopicId = subtopicId;
    this.recommendationService
      .GetRecommendation(this.user.user_id, subjectId, topicId, subtopicId, origin)
      .subscribe((res) => {
          let ApiResult = res as any;
          if (ApiResult.success) {
            if (ApiResult.data !== null) {
              this.RecommedationList = ApiResult.data.slice(0, this.PerPageItem);
              localStorage.setItem("RecommedationList", JSON.stringify(ApiResult.data));
              var len = Math.ceil(+(ApiResult.data.length / this.PerPageItem));
              this.MaxPaginationSize = len;
              this.Pagination = [];
              for (let index = 1; index <= len; index++) {
                this.Pagination.push(index);
              }
              this.showLoader = false;
            }
        } else {
            this.RecommedationList = [];
            this.showLoader = false;
        }
      }, (err) => {
        this.toaster.error("Something went wrong, please try again.");
        this.showLoader = false;
      });
  }
  ChangeTab(id: number, schoolSubjectId: number) {
    if (this.previousSubjectId !== id) {
      $("#dropdownTopicButton").text("All Topics");
      $("#dropdownSubTopicButton").text("All Subtopics");
      this.IsSubTopicDisable = true;
      this.Topics = []; 
      if (id == null) {
        this.IsTopicDisable = true;
        this.GetRecommendation();
      } else {
        this.IsTopicDisable = false;
        this.SelectedSubjectId = id;
        this.selectedSchoolSubjectId = schoolSubjectId;
        this.GetTopic(schoolSubjectId);
        this.GetRecommendation(this.SelectedSubjectId);
      }
    }
    this.ChangePagination(1);
  }
  ChangeTopic(id: number) {
    if (this.previousTopicId !== id) {      
      if (id == null) {
        this.IsSubTopicDisable = true;
        $("#dropdownSubTopicButton").text("All Subtopics");
        this.GetRecommendation(this.SelectedSubjectId, id);
      } else {
          this.IsSubTopicDisable = false;
          // this.ChangeSubTopic(-1);
          this.TopicId = id;
          this.changeToAllSubTopic();
          this.GetSubTopic(this.SelectedSubjectId, id);
          this.GetRecommendation(this.SelectedSubjectId,this.TopicId);
        }
      }
    this.ChangePagination(1);
  }
  changeToAllSubTopic(id: number = null) {
    $("#dropdownSubTopicButton").text("All Subtopics");
  }
  ChangeSubTopic(id: number) {
    this.changedSubTopicId = id;
    if(this.previousSubtopicId !== id) {
      if (id == null) {
        this.GetRecommendation(this.SelectedSubjectId, this.TopicId, id);
        $("#dropdownSubTopicButton").text("All Subtopics");
      } else {
        this.GetRecommendation(this.SelectedSubjectId, this.TopicId, this.changedSubTopicId);
      }
    }
    this.ChangePagination(1);
  }
  ChangePagination(i) {
    $.each($(".pagination > a"), function (x, y) {
      y.classList.remove("active");
    });
    this.CurrentPage = i;
    if ($("#Page-" + i)[0] != undefined) {
      $("#Page-" + i)[0].classList.add("active");
    }
    var ListRecommedation = JSON.parse(
      localStorage.getItem("RecommedationList")
    );
    this.RecommedationList = [];
    this.RecommedationList = ListRecommedation.slice(
      this.PerPageItem * (i - 1),
      this.PerPageItem * i
    );
  }
  Next() {
    if (this.MaxPaginationSize >= this.CurrentPage + 1) {
      this.ChangePagination(this.CurrentPage + 1);
    }
  }
  Previous() {
    if (this.CurrentPage - 1 > 0) {
      this.ChangePagination(this.CurrentPage - 1);
    }
  }
  open(recommendation) {
    if (recommendation.type !== "VIDEO") {
      //this.modalContent = this.sanitizer.bypassSecurityTrustResourceUrl(recommendation.thumbnail);
      this.modalContent = recommendation.content;
    } else {
      this.modalContent = this.sanitizer.bypassSecurityTrustResourceUrl(
        recommendation.content
      );
    }
  }
}
