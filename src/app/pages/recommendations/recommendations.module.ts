import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecommendationsRoutingModule } from './recommendations-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { RecommendationsComponent } from './recommendations.component';


@NgModule({
  declarations: [
    RecommendationsComponent
  ],
  imports: [
    SharedModule,
    RecommendationsRoutingModule
  ]
})
export class RecommendationsModule { }
