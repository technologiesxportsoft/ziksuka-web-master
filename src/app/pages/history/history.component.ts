import { Component, OnInit } from '@angular/core';
import { UserInfo } from 'src/app/shared/_model/user-info';
import { HistoryService } from 'src/app/shared/_services/history.service';
import { DomSanitizer } from '@angular/platform-browser';
declare var $: any;

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  user: UserInfo;
  Subjects = [];
  Topics = [];
  SubTopics = [];
  showLoader = false;
  HistoryList = [];
  IsTopicDisable = true;
  IsSubTopicDisable = true;
  PerPageItem = 20;
  SubjectId = 0;
  TopicId = 0;
  Pagination = [];
  CurrentPage = 1;
  MaxPaginationSize = 0;
  selectedHistory: any;
  selectedHistoryType = '';
  selectedHistoryDesc = '';
  modalContent: any;
  zIndex: any;
  ContentSrc = '';
  selectedTitle = '';
  constructor(private historyService: HistoryService, private sanitizer: DomSanitizer) {
    this.user = JSON.parse(localStorage.getItem('userInfo'));
  }

  ngOnInit() {
    this.GetSubjects();
    this.GetHistory();
    $(document).on('click', '#HistoryTab > li > a', function (e) {
      $.each($('#HistoryTab > li > a'), function (x, y) {
        var el = y.attributes['aria-controls'].value;
        $('#' + el)[0].classList.remove('show', 'active');
        y.classList.remove('show', 'active', 'tab-border-bottom', 'tab-link-active');
        y.classList.add('tab-link');
      });
      var ele = this.attributes['aria-controls'].value;
      $('#' + ele)[0].classList.add('show', 'active');
      this.attributes['aria-selected'].value = 'true';
      this.classList.remove('tab-link');
      this.classList.add('show', 'active', 'tab-border-bottom', 'tab-link-active');
      $(this).tab('show');
    });
    $(document).on('click', '.dropdownTopicItem', function () {
      $.each($(".dropdownTopicItem"), function (x, y) {
        y.classList.remove('active-bg-color');
      });
      $(this).addClass('active-bg-color');
      $("#dropdownTopicButton").text($(this).text().length <= 18 ? $(this).text() : $(this).text().substring(0, 18) + '...');
    });
    $(document).on('click', '.dropdownSubTopicItem', function () {
      $.each($(".dropdownSubTopicItem"), function (x, y) {
        y.classList.remove('active-bg-color');
      });
      $(this).addClass('active-bg-color');
      $("#dropdownSubTopicButton").text($(this).text().length <= 18 ? $(this).text() : $(this).text().substring(0, 18) + '...');
    });
    setTimeout(() => {
      this.showLoader = false;
    }, 2000);
  }
  GetSubjects() {
    this.showLoader = true;
    this.historyService.GetSubjects(this.user.user_id).subscribe((res) => {
      let ApiResult = res as any;
      for (let key in ApiResult.data.subjects) {
        this.Subjects.push({ 'SubjectName': key, 'id': Number(ApiResult.data.subjects[key]) });
      }
    });
  }
  GetTopic(subjectId: number) {
    this.showLoader = true;
    this.historyService.GetTopics(this.user.user_id, subjectId).subscribe((res) => {
      let ApiResult = res as any;
      this.Topics = [];
      for (let key in ApiResult.data.topics) {
        this.Topics.push({ 'TopicName': key, 'id': Number(ApiResult.data.topics[key]) });
      }
      this.showLoader = false;
    });
  }
  GetSubTopic(subjectId: number, topicId: number) {
    this.showLoader = true;
    this.historyService.GetSubTopics(this.user.user_id, subjectId, topicId).subscribe((res) => {
      let ApiResult = res as any;
      this.SubTopics = [];
      for (let key in ApiResult.data.subtopics) {
        this.SubTopics.push({ 'SubTopicName': key, 'id': Number(ApiResult.data.subtopics[key]) });
      }
      this.showLoader = false;
    });
  }
  GetHistory(subjectId: number = null, topicId: number = null, subtopicId: number = null) {
    this.showLoader = true;
    this.historyService.GetHistory(this.user.user_id, subjectId, topicId, subtopicId).subscribe((res) => {
      let ApiResult = res as any;
      this.HistoryList = ApiResult.data.slice(0, this.PerPageItem);
      localStorage.setItem('HistoryList', JSON.stringify(ApiResult.data));
      var len = (ApiResult.data.length / this.PerPageItem);
      var sp = len.toString().split('.');
      var To = 0;
      sp.length > 1 ? To = Number(sp[0]) + 1 : To = Number(sp[0]);
      this.MaxPaginationSize = To;
      this.Pagination = [];
      for (let index = 1; index <= To; index++) {
        this.Pagination.push(index);
      }
      this.showLoader = false;
    });
  }
  ChangeTab(id: number) {
    $('#dropdownTopicButton').text('All Topics');
    $('#dropdownSubTopicButton').text('All SubTopics');
    this.IsSubTopicDisable = true;
    if (id == -1) {
      this.IsTopicDisable = true;
      this.GetHistory();
    }
    else {
      this.IsTopicDisable = false;
      this.SubjectId = id;
      this.GetTopic(id);
      this.GetHistory(id);
    }
    this.ChangePagination(1);
  }
  ChangeTopic(id: number) {
    $('#dropdownSubTopicButton').text('All SubTopics');
    if (id == -1) {
      this.GetHistory(this.SubjectId);
      this.IsSubTopicDisable = true;
    }
    else {
      this.TopicId = id;
      this.IsSubTopicDisable = false;
      this.GetSubTopic(this.SubjectId, id);
      this.GetHistory(this.SubjectId, id);
    }
    this.ChangePagination(1);
  }
  ChangeSubTopic(id: number) {
    if (id == -1) {
      this.GetHistory(this.SubjectId, this.TopicId);
      $('#dropdownSubTopicButton').text('All SubTopics');
    }
    else {
      this.GetHistory(this.SubjectId, this.TopicId, id);
    }
    this.ChangePagination(1);
  }
  ChangePagination(i) {
    $.each($('.pagination > a'), function (x, y) {
      y.classList.remove('active');
    });
    this.CurrentPage = i;
    $('#Page-' + i)[0].classList.add('active');
    var ListHistory = JSON.parse(localStorage.getItem('HistoryList'));
    this.HistoryList = [];
    this.HistoryList = ListHistory.slice((this.PerPageItem * (i - 1)), (this.PerPageItem * i));
  }
  Next() {
    if (this.MaxPaginationSize >= this.CurrentPage + 1) {
      this.ChangePagination(this.CurrentPage + 1);
    }
  }
  Previous() {
    if (this.CurrentPage - 1 > 0) {
      this.ChangePagination(this.CurrentPage - 1);
    }
  }
  open(history) {
    if (history.type !== "VIDEO") {
      //this.modalContent = this.sanitizer.bypassSecurityTrustResourceUrl(history.thumbnail);
      this.modalContent = history.content;
    }
    else {
      this.modalContent = this.sanitizer.bypassSecurityTrustResourceUrl(history.content);
    }
  }
}
