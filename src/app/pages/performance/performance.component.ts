import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import * as _ from "lodash";
import { UserInfo } from 'src/app/shared/_model/user-info';
import { DashboardService } from 'src/app/shared/_services/dashboard.service';

@Component({
  selector: 'app-performance',
  templateUrl: './performance.component.html',
  styleUrls: ['./performance.component.css']
})
export class PerformanceComponent implements OnInit {
  performanceParam: string = "Mathematics";
  LineChart: any;
  user: UserInfo;
  performances: any;
  graphLabels = [];
  graphData = [];
  performanceParams = [];
  graphDataSets = [];
  constructor(private dashboardService: DashboardService) { }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('userInfo'));
    this.GetPerformance();
    this.GetChart();
  }
  GetPerformance() {
    this.dashboardService.GetPerformance(this.user.id).subscribe((res) => {
      let result = res as any;
      this.performances = result.data;
      for (let key in this.performances) {
        this.performanceParams.push(key);
      }
      this.ChangeInGraph();
    }, (err) => {
      console.log(err);
    });
  }
  ChangeInGraph() {
    this.graphLabels = [];
    this.graphDataSets = [];
    for (let key in this.performances[this.performanceParam].Topper) {
      this.graphLabels.push(key);
    }
    this.graphLabels = _.orderBy(this.graphLabels);
    for (let key in this.performances[this.performanceParam]) {
      let data = [];
      for (let item in this.performances[this.performanceParam][key]) {
        data.push(this.performances[this.performanceParam][key][item]);
      }
      let dataSet: any = {
        label: key,
        lineTension: 0.3,
        backgroundColor: "rgba(78, 115, 223,0)",
        pointRadius: 0,
        pointBackgroundColor: "rgba(78, 115, 223, 1)",
        pointBorderColor: "rgba(78, 115, 223, 1)",
        pointHoverRadius: 0,
        pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
        pointHoverBorderColor: "rgba(78, 115, 223, 1)",
        pointHitRadius: 10,
        pointBorderWidth: 2,
        data: data,
      }
      if (key === "Topper") {
        dataSet.borderColor = "#135DFF";
      }
      if (key === "Average") {
        dataSet.borderColor = "#31c534";
      }
      if (key === "You") {
        dataSet.borderColor = "#FF004E";
      }
      this.graphDataSets.push(dataSet);
    }
    setTimeout(() => {
      this.LineChart.data.labels = this.graphLabels;
      this.LineChart.data.datasets = this.graphDataSets;
      this.LineChart.update();
    }, 1000);

  }
  GetChart() {
    this.LineChart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: this.graphLabels,
        datasets: this.graphDataSets
      },
      options: {
        maintainAspectRatio: false,
        layout: {
          padding: {
            left: 10,
            right: 25,
            top: 25,
            bottom: 0
          }
        },
        scales: {
          xAxes: [{
            time: {
              unit: 'date'
            },
            gridLines: {
              display: false,
              drawBorder: false
            },
            ticks: {
              maxTicksLimit: 7
            }
          }],
          yAxes: [{
            ticks: {
              maxTicksLimit: 5,
              padding: 10,
              // Include a dollar sign in the ticks
              callback: function (value, index, values) {
                return value;
              }
            },
            gridLines: {
              color: "rgb(234, 236, 244)",
              zeroLineColor: "rgb(234, 236, 244)",
              drawBorder: false,
              borderDash: [2],
              zeroLineBorderDash: [2, 2]
            }
          }],
        },
        legend: {
          display: false
        },
        tooltips: {
          backgroundColor: "rgb(255,255,255)",
          bodyFontColor: "#858796",
          titleMarginBottom: 10,
          titleFontColor: '#6e707e',
          titleFontSize: 14,
          borderColor: '#dddfeb',
          borderWidth: 1,
          xPadding: 15,
          yPadding: 15,
          displayColors: false,
          intersect: false,
          mode: 'index',
          caretPadding: 10,
          // callbacks: {
          //   label: function (tooltipItem, chart) {
          //     var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          //     return datasetLabel + ': ' + tooltipItem.yLabel;
          //   }
          // }
        }
      }
    });
  }
}
