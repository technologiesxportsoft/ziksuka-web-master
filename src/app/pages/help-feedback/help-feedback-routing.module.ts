import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HelpFeedbackComponent } from './help-feedback.component';


const routes: Routes = [
  { path: '', component: HelpFeedbackComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HelpFeedbackRoutingModule { }
