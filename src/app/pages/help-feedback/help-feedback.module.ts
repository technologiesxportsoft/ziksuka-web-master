import { NgModule } from '@angular/core';
import { HelpFeedbackRoutingModule } from './help-feedback-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { HelpFeedbackComponent } from './help-feedback.component';


@NgModule({
  declarations: [HelpFeedbackComponent],
  imports: [
    SharedModule,
    HelpFeedbackRoutingModule
  ]
})
export class HelpFeedbackModule { }
