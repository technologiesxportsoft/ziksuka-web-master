import { Component, OnInit } from '@angular/core';
import { FeedbackObj } from 'src/app/shared/_model/feedback';
import { HelpFeedbackService } from 'src/app/shared/_services/help-feedback.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from 'src/app/shared/_services/auth.service';
import { UserInfo } from 'src/app/shared/_model/user-info';

@Component({
  selector: 'app-help-feedback',
  templateUrl: './help-feedback.component.html',
  styleUrls: ['./help-feedback.component.css']
})
export class HelpFeedbackComponent implements OnInit {

  helpFeedbackObj: FeedbackObj = new FeedbackObj();
  pagecount: any = 0;
  currentUserData: UserInfo;
  constructor(private feedbackService: HelpFeedbackService, private router: Router,
    private toaster: ToastrService, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.currentUserData = this.authenticationService.currentUserValue;
    this.helpFeedbackObj = new FeedbackObj();
    this.helpFeedbackObj.first_name = this.currentUserData.first_name;
    this.helpFeedbackObj.last_name = this.currentUserData.last_name;
    this.helpFeedbackObj.email_id = this.currentUserData.email;
    this.helpFeedbackObj.phone_number = this.currentUserData.phone_number;
  }
  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  onFormSubmit(form) {
    console.log('helpFeedbackObj', this.helpFeedbackObj);
    let feedbackApiObj = {
      firstName: this.helpFeedbackObj.first_name,
      lastName: this.helpFeedbackObj.last_name,
      emailId: this.helpFeedbackObj.email_id,
      phoneNumber: this.helpFeedbackObj.phone_number,
      subject: this.helpFeedbackObj.subject,
      message: this.helpFeedbackObj.message,
      userId: this.currentUserData.user_id,
    }
    console.log('helpFeedbackObj', feedbackApiObj);
    this.feedbackService.submitHelpFeedback(feedbackApiObj).subscribe((res: any) => {
      if (res && res.success) {
        this.toaster.success(res.msg);
        form.reset();
        this.router.navigate(['/dashboard']);
      } else {
        this.toaster.error(res.msg);
      }
    })
  }
}
