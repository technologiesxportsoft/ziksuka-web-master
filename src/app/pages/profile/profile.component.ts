import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User, State, Country, City } from '../../shared/_model/user';
import { AuthenticationService } from '../../shared/_services/auth.service';
import { ProfileService } from '../../shared/_services/profile.service';
import { ToastrService } from 'ngx-toastr';
import { UserRole } from '../../shared/_enumerations/UserRole';
import { CommonService } from '../../shared/_services/common.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  UserRole = UserRole;
  password: any = '';
  initials;
  newPassword: any = '';
  confirmPassword: any = '';
  currentUserData;
  userId = '';
  user_name = '';
  cityList: any = [];
  stateList = [];
  countryList = [];
  currentUserObj: User = new User();
  mapType = 'satellite';
  changePwdFlag = false;
  pagecount: any = 0;
  stateObj = new State();
  countryObj = new Country();
  cityObj = new City();

  showLoader = true;
  constructor(private router: Router, private bmodalService: NgbModal, private toaster: ToastrService,
    private authenticationService: AuthenticationService, private profileService: ProfileService,
    public commonService: CommonService) { }

  ngOnInit() {
    this.currentUserData = this.authenticationService.currentUserValue;
    this.userId = this.currentUserData.user_id;
    this.user_name = this.currentUserData.user_name;
    this.getProfile();
  }
  getProfile() {
    this.profileService.getProfileById(this.userId).subscribe((res: any) => {
      if (res && res.success) {
        this.currentUserObj = res.data;
        this.stateObj = this.currentUserObj.state;
        this.cityObj = this.currentUserObj.city;
        this.countryObj = this.currentUserObj.country;
        this.getCity();
        this.getState();
        this.getCountry();
        this.initials = this.commonService.getShortName(this.currentUserObj.firstName + ' ' + this.currentUserObj.lastName);
      }else {
        this.showLoader = false;
      }
    },err =>{
      this.showLoader = false
    })
  }
  getCountry() {
    this.countryList = [];
    this.profileService.getCountry().subscribe((res: any) => {
      if (res && res.success) {
        this.countryList = res.data;
        this.showLoader = false;
      }
    })
  }
  getState() {
    this.stateList = [];
    this.profileService.getState(this.countryObj.countryId).subscribe((res: any) => {
      if (res && res.success) {
        this.stateList = res.data;
      }
    })
  }
  getCity() {
    this.cityList = [];
    this.profileService.getCity(this.countryObj.countryId, this.stateObj.stateId).subscribe((res: any) => {
      if (res && res.success) {
        this.cityList = res.data;
      }
    })
  }
  open(content) {
    this.bmodalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true }).result.then((result) => {
    }, (reason) => {
    });
  }
  sendEmail() {
    let obj = {
      "userId": this.currentUserObj.userId,
      "oldPassword": this.password,
      "newPassword": this.newPassword,
      "confirmPassword": this.confirmPassword
    }
    this.profileService.changePassword(obj).subscribe((res: any) => {
      if (res && res.success) {
        this.confirmPassword = '';
        this.newPassword = '';
        this.password = '';
        this.bmodalService.dismissAll();
        this.toaster.success(res.msg)
      } else {
        this.toaster.error(res.msg)
      }
    })
  }
  onCountryChange() {
    let i = this.countryList.findIndex(v => this.countryObj.countryId == v.countryId);
    if (i > -1) {
      this.countryObj.countryId = Number(this.countryList[i].countryId);
      this.countryObj.countryName = this.countryList[i].countryName;
    } else {
      this.countryObj.countryName = '';
    }
    this.getState();
    this.cityList = [];
    this.stateObj.stateId = null;
    this.cityObj.cityId = null;
  }
  onStateChange() {
    let i = this.stateList.findIndex(v => this.stateObj.stateId == v.stateId);
    if (i > -1) {
      this.stateObj.stateId = Number(this.stateObj.stateId);
      this.stateObj.stateName = this.stateList[i].stateName;
    } else {
      this.stateObj.stateName = '';
    }
    this.getCity();
    this.cityObj.cityId = null;
    this.cityObj.cityName = null;
  }
  onCityChange() {
    let i = this.cityList.findIndex(v => this.cityObj.cityId == v.cityId);
    if (i > -1) {
      this.cityObj.cityId = Number(this.cityObj.cityId);
      this.cityObj.cityName = this.cityList[i].cityName;
    } else {
      this.cityObj.cityName = '';
    }
  }
  onUpdate() {
    let obj = {
      'userId': this.currentUserObj.userId,
      'userName': this.currentUserObj.userName,
      'firstName': this.currentUserObj.firstName,
      'lastName': this.currentUserObj.lastName,
      'emailId': this.currentUserObj.emailId,
      'phoneNumber': this.currentUserObj.phoneNumber,
      'line1': this.currentUserObj.line1,
      'line2': this.currentUserObj.line2,
      'city': this.cityObj,
      'state': this.stateObj,
      'country': this.countryObj,
      'zipcode': this.currentUserObj.zipcode,
    }
    // this.currentUserObj.user_id = this.currentUserObj.id;
    this.profileService.updateProfile(obj).subscribe((res: any) => {
      if (res && res.success) {
        this.toaster.success(res.msg);
        this.currentUserObj = new User();
        this.ngOnInit();
      } else {
        this.toaster.error(res.msg);
      }
    })
  }
  // checkConfirmPassword() {
  //   if (this.newPassword && this.confirmPassword && this.confirmPassword == this.newPassword) {
  //     this.changePwdFlag = true;
  //   }
  // }
  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
