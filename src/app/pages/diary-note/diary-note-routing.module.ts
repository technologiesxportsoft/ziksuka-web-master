import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DiaryNoteComponent } from './diary-note.component';


const routes: Routes = [{
  path: '',
  component: DiaryNoteComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DiaryNoteRoutingModule { }
