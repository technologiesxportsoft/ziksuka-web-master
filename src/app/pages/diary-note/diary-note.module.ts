import { NgModule } from '@angular/core';

import { DiaryNoteRoutingModule } from './diary-note-routing.module';
import { DiaryNoteComponent } from './diary-note.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [DiaryNoteComponent],
  imports: [
    SharedModule,
    DiaryNoteRoutingModule
  ]
})
export class DiaryNoteModule { }
