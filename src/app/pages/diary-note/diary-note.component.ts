import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { HistoryService } from '../../shared/_services/history.service';
import { DiaryNoteService } from '../../shared/_services/diary-note.service';
import { AuthenticationService } from '../../shared/_services/auth.service';
import { UserRole } from '../../shared/_enumerations/UserRole';
import { NotesNoticeObj } from '../../shared/_model/diary';
import { GradeService } from '../../shared/_services/grade.service';
import { GradeObj } from '../../shared/_model/grade';
import { StudentObj } from '../../shared/_model/student';
import { StudentService } from '../../shared/_services/student.service';

@Component({
  selector: 'app-diary-note',
  templateUrl: './diary-note.component.html',
  styleUrls: ['./diary-note.component.css']
})
export class DiaryNoteComponent implements OnInit {
  public UserRole = UserRole;
  showLoader = true;
  teacherRole = false;
  selectedSubject = 'Select Subject';
  selectedSubjectId = '';
  diaryNote = '';
  subjectList = [];
  currentUserData;
  diaryList: NotesNoticeObj[] = [];
  noticesList: NotesNoticeObj[] = [];
  gradeList: GradeObj[] = [];
  studentList: StudentObj[] = [];
  pagecount: any = 0;
  fileList: any = [];
  filterDate = new Date();
  newdate = new Date();
  fileToUpload: File = null;
  docHash: any = '';
  searchGradeTerm = "";
  searchStudentTerm = "";
  showSubject = false;
  selectedrecipientId = [];
  selectedrecepientName = [];
  recipientSchoolGradeSectionName = [];
  recipientSchoolGradeSectionId = [];
  @ViewChild('myInput', { static: true }) myInput: ElementRef;
  fileName = '';
  isNotice = 0;
  allowed_types = ['image/png', 'image/jpeg', 'image/jpg', 'application/pdf'];
  constructor(private historyService: HistoryService, private notesService: DiaryNoteService,
    private authenticationService: AuthenticationService, private toasterService: ToastrService,
    private router: Router, private gradeService: GradeService, private studentService: StudentService) { }

  ngOnInit() {
    this.currentUserData = this.authenticationService.currentUserValue;
    if (this.currentUserData.role == this.UserRole.Teacher) {
      this.teacherRole = true;
      this.getGradeList();
    }
    this.getSubjectList();
    this.getNotesList();
  }
  getGradeList() {
    this.gradeList = [];
    this.gradeService.getGradeListByUserIdV2(this.currentUserData.user_id).subscribe((res: any) => {
      this.gradeList = res.data;
      if (this.gradeList.length) {
        this.gradeList.forEach((grade, index) => {
          grade.imgTitle = null;
          let first_letter = grade.schoolGradeName as string;
          let last_letter = grade.sectionName as string;
          grade.imgTitle = first_letter + last_letter;
          grade.filterString = first_letter + last_letter;
          grade.isActive = false;
        })
      }
    });
  }
  getStudentList() {
    this.studentList = [];
    this.studentService.getStudentListByGradeSectionV2(this.recipientSchoolGradeSectionId).subscribe((res: any) => {
      this.studentList = res.data;
      if (this.studentList && this.studentList.length) {
        this.studentList.forEach((ele, index) => {
          let first_name = ele.firstName as string;
          let last_name = ele.lastName as string;
          ele.imgTitle = first_name.substring(0, 1).toUpperCase() + last_name.substring(0, 1).toUpperCase();
          ele.filterString = first_name + ' ' + last_name;
          ele.isActive = false;
        })
      }
    });
  }
  getSubjectList() {
    if ((this.recipientSchoolGradeSectionId != null && this.recipientSchoolGradeSectionId.length == 1) || this.currentUserData.role == UserRole.Student) {
      this.subjectList = [];
      let id = this.recipientSchoolGradeSectionId;
      if (this.currentUserData.role == UserRole.Student) {
        id = this.currentUserData.school_grade_section_id;
      }
      this.historyService.GetSubjectsListV2(id, this.currentUserData.user_id).subscribe((res) => {
        let apiResult = res as any;
        if (apiResult && apiResult.data && apiResult.data.length) {
          apiResult.data.forEach(ele => {
            this.subjectList.push({ 'subjectName': ele.schoolSubjectName, 'id': Number(ele.schoolGradeSubjectId) });
          })
        }
      });
    } else {
      this.subjectList = [];
    }
  }
  onDateChange(event) {
    this.filterDate = new Date(event.year, event.month - 1, event.day);
    console.log('event', this.filterDate);
    this.getNotesList();
  }
  getNotesList() {
    if (this.currentUserData.role == UserRole.Student) {
      let list = [];
      list.push(this.currentUserData.user_id);
      let obj = {
        userId: this.currentUserData.user_id,
        requestedTime: this.filterDate.getTime(),
        recipientIdList: list
      }
      this.notesService.getStudentNotes(obj).subscribe((res) => {
        let apiResult = res as any;
        if (apiResult.success) {
          this.noticesList = apiResult.data.notices;
          this.diaryList = apiResult.data.notes;
          if (this.noticesList) {
            this.noticesList.forEach(ele => {
              if (ele.createdAt) {
                ele.createdAt = new Date(ele.createdAt);
              }
            })
          }
          if (this.diaryList) {
            this.diaryList.forEach(ele => {
              if (ele.createdAt) {
                ele.createdAt = new Date(ele.createdAt);
              }
            })
          }

        }
        this.showLoader = false;
      });
    } else {
      let recipientIdList = null;
      let recipientSchoolGradeSectionIdList = null;
      if (this.selectedrecipientId.length) {
        recipientIdList = this.selectedrecipientId;
      } else if (this.recipientSchoolGradeSectionId.length) {
        recipientSchoolGradeSectionIdList = this.recipientSchoolGradeSectionId;
      } else {
        recipientIdList = [this.currentUserData.user_id];
      }
      const obj = {
        userId: this.currentUserData.user_id,
        recipientIdList,
        recipientSchoolGradeSectionIdList,
        requestedTime: this.filterDate.getTime()
      };
      this.notesService.getTeacherNotes(obj).subscribe((res) => {
        let apiResult = res as any;
        if (apiResult.success) {
          this.noticesList = apiResult.data.notices;
          this.diaryList = apiResult.data.notes;
          if (this.noticesList) {
            this.noticesList.forEach(ele => {
              if (ele.createdAt) {
                ele.createdAt = new Date(ele.createdAt);
              }
            });
          }
          if (this.diaryList) {
            this.diaryList.forEach(ele => {
              if (ele.createdAt) {
                ele.createdAt = new Date(ele.createdAt);
              }
            })
          }

        }
        this.showLoader = false;
      });
    }

  }
  onAddNote(form) {
    this.showLoader = true;
    let formdata = new FormData();
    let list = [];
    if (this.selectedrecipientId.length) {
      list = this.selectedrecipientId;
      formdata.append('recipientIdList', JSON.stringify(list));
    } else if (this.recipientSchoolGradeSectionId.length) {
      list = this.recipientSchoolGradeSectionId;
      formdata.append('recipientSchoolGradeSectionIdList', JSON.stringify(list));
    } else {
      list.push(this.currentUserData.user_id);
      formdata.append('recipientIdList', JSON.stringify(list));
    }

    formdata.append('senderId', this.currentUserData.user_id);
    formdata.append('diaryNote', this.diaryNote);
    if (this.selectedSubjectId != '') {
      formdata.append('subjectId', this.selectedSubjectId);
    }
    formdata.append('createdBy', this.currentUserData.user_name);

    if (this.fileToUpload && this.fileToUpload != null) {
      formdata.append('attachment', this.fileToUpload, this.fileToUpload.name);
    }
    if (this.currentUserData.role == this.UserRole.Teacher && this.isNotice) {
      formdata.append('noteType', 'notice');
    }
    this.notesService.addStudentNote(formdata).subscribe((res) => {
      let apiResult = res as any;
      if (apiResult && apiResult.success) {
        this.toasterService.success(apiResult.msg);
        form.reset();
        this.fileName = '';
        this.fileToUpload = null;
        if (this.currentUserData.role === UserRole.Student) {
          this.selectedSubject = 'Select subject';
          this.selectedSubjectId = '';
        }
        this.showLoader = false;
        this.getNotesList();
      } else {
        this.showLoader = false;
        this.toasterService.error(apiResult.msg);
      }
    });
  }
  fileProgress(fileInput: File) {
    this.fileToUpload = fileInput[0];
    this.fileName = this.fileToUpload.name;
  }
  onSubjectChange(subject) {
    this.selectedSubject = subject.subjectName;
    this.selectedSubjectId = subject.id;
  }
  studentSelection(studentObject) {
    this.studentList.forEach((student: any) => {
      if (studentObject.userId == student.userId) {
        student.isActive = !student.isActive;
        let i = this.selectedrecipientId.findIndex(v => v == student.userId);
        if (student.isActive) {
          if (i == -1) {
            this.selectedrecipientId.push(student.userId)
            this.selectedrecepientName.push(student.firstName + ' ' + student.lastName);
          }
        } else {
          if (i > -1) {
            this.selectedrecepientName.splice(i, 1);
            this.selectedrecipientId.splice(i, 1);
          }
        }
      }
    });
    // studentObject.isActive = true;
    this.getNotesList();
  }
  classSelection(gradeObject) {
    this.selectedrecipientId = [];
    this.selectedrecepientName = [];
    // this.recipientSchoolGradeSectionId = gradeObject.school_grade_section_id;
    this.gradeList.forEach((grade, index) => {
      // gradeObject.isActive = false;
      if (grade.schoolGradeSectionId == gradeObject.schoolGradeSectionId) {
        grade.isActive = !grade.isActive;
        let i = this.recipientSchoolGradeSectionId.findIndex(v => v == grade.schoolGradeSectionId);
        if (grade.isActive) {
          if (i == -1) {
            this.recipientSchoolGradeSectionName.push(gradeObject.schoolGradeName + gradeObject.sectionName);
            this.recipientSchoolGradeSectionId.push(gradeObject.schoolGradeSectionId)
          }
        } else {
          if (i > -1) {
            this.recipientSchoolGradeSectionName.splice(i, 1);
            this.recipientSchoolGradeSectionId.splice(i, 1);
          }
        }

      }
    });
    this.getSubjectList();
    if (this.recipientSchoolGradeSectionId != null && this.recipientSchoolGradeSectionId.length == 1) {
      this.getStudentList();
    } else if (this.recipientSchoolGradeSectionId.length > 1 || !this.recipientSchoolGradeSectionId.length) {
      this.studentList = [];
    }
    // gradeObject.isActive = true;
    this.getNotesList();
  }
}
