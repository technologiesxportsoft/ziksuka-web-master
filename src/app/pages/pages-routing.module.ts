import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeLayoutComponent } from '../shared/_components/layouts/home-layout/home-layout.component';
import { FeedComponent } from './feed/feed.component';
import { NotebookComponent } from './notebook/notebook.component';
import { StudentAttendenceComponent } from './student-attendence/student-attendence.component';
import { TeacherStudentGuard } from '../shared/_guards/teacher-student-guard';


const routes: Routes = [
	{
		path: '',
		component: HomeLayoutComponent,
		children: [
			{
				path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule',
        canActivate:[TeacherStudentGuard]
			},
			{
				path: 'profile',
				loadChildren: './profile/profile.module#ProfileModule',
			},
			{
				path: 'recommendations',
        loadChildren: './recommendations/recommendations.module#RecommendationsModule',
        canActivate:[TeacherStudentGuard]
			},
			{
				path: 'attendance',
        loadChildren: './attendance/attendance.module#AttendanceModule',
        canActivate:[TeacherStudentGuard]
			},
			{
				path: 'timetable',
        loadChildren: './time-table/time-table.module#TimeTableModule',
        canActivate:[TeacherStudentGuard]
			},
			{
				path: 'diary',
        loadChildren: './diary-note/diary-note.module#DiaryNoteModule',
        canActivate:[TeacherStudentGuard]
			},
			{
				path: 'help-feedback',
        loadChildren: './help-feedback/help-feedback.module#HelpFeedbackModule',
        canActivate:[TeacherStudentGuard]
			},
			{
				path: 'history',
        loadChildren: './history/history.module#HistoryModule',
        canActivate:[TeacherStudentGuard]
			},
			{
				path: 'books',
        loadChildren: './book/book.module#BookModule',
        canActivate:[TeacherStudentGuard]
			},
			{
				path: 'practice-test',
        loadChildren: './practice-test/practice-test.module#PracticeTestModule',
        canActivate:[TeacherStudentGuard]
			},
			{
				path: 'recall-summary',
        loadChildren: './test-results/test-results.module#TestResultsModule',
        canActivate:[TeacherStudentGuard]
			},
			{
				path: 'classroom/:classroomId',
        loadChildren: './classroom/classroom.module#ClassroomModule',
        canActivate:[TeacherStudentGuard]
			},
			{
				path: 'feed',
        component: FeedComponent,
        canActivate:[TeacherStudentGuard]
			},
			{
				path: 'notebook',
        component: NotebookComponent,
        canActivate:[TeacherStudentGuard]
			},
			{
				path: 'student-attendence/:sectionId',
        component: StudentAttendenceComponent,
        canActivate:[TeacherStudentGuard]
			},
			{
				path: '',
				redirectTo: 'dashboard',
				pathMatch: 'full',
			},
		],
	}
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PagesRoutingModule { }
