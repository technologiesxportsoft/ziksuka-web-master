import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';
import { PracticeTestComponent } from './practice-test.component';
import { TestResultComponent } from './test-result/test-result.component';

export class practiceTestGuard implements CanActivate {
  canActivate() {
    const practiceTestGaurd = localStorage.getItem('practiceTestGaurd')
    if (practiceTestGaurd == 'true') {
      return true;
    } else {
      return false;
    }
  }
}
const routes: Routes = [
  {
    path: '',
    component: PracticeTestComponent,
    canActivate: [practiceTestGuard]
  },
  {
    path: 'result',
    component: TestResultComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    practiceTestGuard
  ]
})

export class PracticeTestRoutingModule { }