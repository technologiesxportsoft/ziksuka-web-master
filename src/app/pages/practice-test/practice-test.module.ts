import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PracticeTestRoutingModule } from "./practice-test-routing.module";
import { PracticeTestComponent } from "./practice-test.component";
import { SharedModule } from "src/app/shared/shared.module";
import { TestResultComponent } from './test-result/test-result.component';

@NgModule({
  declarations: [PracticeTestComponent, TestResultComponent],
  imports: [SharedModule, PracticeTestRoutingModule]
})
export class PracticeTestModule {}
