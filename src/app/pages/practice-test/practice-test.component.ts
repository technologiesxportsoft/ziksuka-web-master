import { Component, OnInit, OnDestroy, ElementRef, Renderer, ChangeDetectorRef, HostListener, ViewChild } from '@angular/core';
import { PracticeTestService } from 'src/app/shared/_services/practice-test.service';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { Subject } from 'rxjs/internal/Subject';
import { PracticeTestObj, QuestionObj, AnswerObj, QuestionAnsObj, TestLocalStorageObj, MetaObj } from 'src/app/shared/_model/practice-test';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/shared/_services/auth.service';
import { CommonService } from 'src/app/shared/_services/common.service';

@Component({
  selector: 'app-practice-test',
  templateUrl: './practice-test.component.html',
  styleUrls: ['./practice-test.component.css']
})

export class PracticeTestComponent implements OnInit, OnDestroy {
  showLoader = false;
  practiceTestData: PracticeTestObj = new PracticeTestObj();
  unsubscribeAll: Subject<boolean> = new Subject<boolean>();
  testLocalStorageObj = new TestLocalStorageObj();
  questionsLength = 0;
  currentQuestionNo: any = 1;
  rows = [];
  currentQuestionObj = new QuestionObj();
  answerObj = new AnswerObj();
  questionTag = '';
  trueFalse = [{ 'key': 'true', 'value': 'True' }, { 'key': 'false', 'value': 'False' }];
  alphabets = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
  countDownDate;
  countDownHours;
  countDownMinutes;
  countDownSeconds;
  minutes;
  seconds;
  fullscreenFlag = false;
  showResumeWindow = false;
  endTestClicked = false;
  fullscreenModeStorage;
  testDetail = {};
  metaObj: any = new MetaObj();
  mathJaxObject;

  @ViewChild('modalRoot', { static: false }) modalRoot: ElementRef;
  user: any;
  timerInterval: any;

  @HostListener('document:fullscreenchange', [])
  @HostListener('document:webkitfullscreenchange', [])
  @HostListener('document:mozfullscreenchange', [])
  @HostListener('document:MSFullscreenChange', [])
  fullscreenmode() {
    if (this.fullscreenFlag) {
      //console.log('this.fullscreenFlag', this.fullscreenFlag);
      this.fullscreenFlag = !this.fullscreenFlag;
    } else {
      //console.log('this.fullscreenFlag', this.fullscreenFlag);
      localStorage.setItem('fullscreenMode', 'false');
      this.modalRoot.nativeElement.style.visibility = 'hidden';
      this.showResumeWindow = true;
    }
    event.preventDefault();
    event.stopPropagation();
  }
  @HostListener('keydown.esc', ['$event'])
  onKeyDown(event): void {
    alert(12)
    //console.log('esc key');
    // event.preventDefault();
    // event.stopPropagation();
    // this.hide();
  }
  constructor(private practiceTestService: PracticeTestService, private elementRef: ElementRef, private ref: ChangeDetectorRef,
    private authenticationService: AuthenticationService, private commonService: CommonService,
    private renderer: Renderer, private toasterService: ToastrService, private router: Router) {
  }

  ngOnInit() {
    localStorage.removeItem('testResultObj');
    localStorage.removeItem('testResultError');
    this.user = this.authenticationService.currentUserValue;
  }
  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }
  ngAfterViewInit() {
    this.setInitialData();
    if (localStorage.getItem('fullscreenMode') != undefined) {
      if (this.testLocalStorageObj.timerObj.countDownMinutes != -1) {
        let countDownDate = new Date(new Date().getTime() + this.testLocalStorageObj.timerObj.countDownMinutes * 60000 + this.testLocalStorageObj.timerObj.countDownSeconds * 1000).getTime();
        this.countDownDate = countDownDate;
        this.countDownMinutes = this.testLocalStorageObj.timerObj.countDownMinutes;
        this.countDownSeconds = this.testLocalStorageObj.timerObj.countDownSeconds;
        this.minutes = this.testLocalStorageObj.timerObj.minutes;
        this.seconds = this.testLocalStorageObj.timerObj.seconds;
        this.setLocalStorage();
      }
      this.showResumeWindow = true;
      this.fullscreenFlag = false;
      this.showLoader = false;
      this.modalRoot.nativeElement.style.visibility = 'hidden';
    } else {
      localStorage.setItem('fullscreenMode', 'true');
      this.fullScreen();
      this.timerInterval = setInterval(() => {
        this.timer();
      }, 1000);
    }

    // if (this.testLocalStorageObj.timerObj.countDownMinutes != -1) {
    //   let countDownDate = new Date(new Date().getTime() + this.testLocalStorageObj.timerObj.countDownMinutes * 60000 + this.testLocalStorageObj.timerObj.countDownSeconds * 1000).getTime();
    //   this.countDownDate = countDownDate;
    //   this.countDownMinutes = this.testLocalStorageObj.timerObj.countDownMinutes;
    //   this.countDownSeconds = this.testLocalStorageObj.timerObj.countDownSeconds;
    //   this.minutes = this.testLocalStorageObj.timerObj.minutes;
    //   this.seconds = this.testLocalStorageObj.timerObj.seconds;
    //   setTimeout(() => {
    //     this.timer();
    //   }, 1000);
    // } else {
    //   this.countDownDate = new Date(new Date().getTime() + this.practiceTestData.meta.duration * 60000).getTime();
    //   this.timer();
    // }
    setTimeout(() => {
      this.showLoader = false;
    }, 1000);
  }
  setInitialData() {
    this.showLoader = true;
    // this.testDetail = JSON.parse(localStorage.getItem('testDetail'));
    this.testLocalStorageObj = JSON.parse(localStorage.getItem('testLocalStorageObj'));
    this.practiceTestData = new PracticeTestObj();
    this.practiceTestData = this.testLocalStorageObj.practiceTestObj;
    this.metaObj = this.practiceTestData.metaData;
    this.questionsLength = this.practiceTestData.questions.length;
    if (this.testLocalStorageObj.answerObj.questions && this.testLocalStorageObj.answerObj.questions.length) {
      this.answerObj = this.testLocalStorageObj.answerObj;
    } else {
      this.answerObj.testId = this.practiceTestData.testId;
      this.mapQuestionAns();
    }
    if (this.questionsLength && this.testLocalStorageObj.currentQuestionNo == -1) {
      this.currentQuestionNo = 1;
      this.currentQuestionObj = this.practiceTestData.questions[0];
    } else {
      this.currentQuestionNo = this.testLocalStorageObj.currentQuestionNo;
      this.currentQuestionObj = this.testLocalStorageObj.currentQuestionObj;
    }
    localStorage.setItem('timerFlag', 'true');
    this.countDownDate = new Date(new Date().getTime() + this.practiceTestData.metaData.duration * 60000).getTime();
    this.generateQuestionTag();
  }
  ngOnDestroy() {
    this.unsubscribeAll.next(true);
    this.unsubscribeAll.complete();
  }
  onCheckboxChange(event, value) {
    if (event.target.checked) {
      this.answerObj.questions[this.currentQuestionNo - 1].answer.push(value);
    } else if (!event.target.checked) {
      let i = this.answerObj.questions[this.currentQuestionNo - 1].answer.findIndex(v => v == value);
      if (i > -1) {
        this.answerObj.questions[this.currentQuestionNo - 1].answer.splice(i, 1);
      }
    }
    this.setLocalStorage();
  }
  onInputChange(value) {
    this.answerObj.questions[this.currentQuestionNo - 1].answer = value;
    this.setLocalStorage();
  }
  mapQuestionAns() {
    this.answerObj.questions = [];
    if (this.practiceTestData.questions && this.practiceTestData.questions.length) {
      this.practiceTestData.questions.forEach((ele, index) => {
        let obj = new QuestionAnsObj();
        obj.questionId = ele.questionId;
        obj.optionsShown = ele.optionsIndices;
        obj.answer = [];
        // if (ele.type == 'mco') {
        //   obj.answer = [];
        // } else {
        //   obj.answer = '';
        // }
        this.answerObj.questions.push(obj);
      })
    }
  }
  generateQuestionTag() {
    var heading = document.getElementById('heading');
    this.questionTag = '';
    let question = this.currentQuestionObj.questionHtml;
    this.rows = [];
    if (this.currentQuestionObj && this.currentQuestionObj.options) {
      for (let i = 0; i < this.currentQuestionObj.options.length; i = i + 2) {
        let arrayObj = [];
        //console.log('this.currentQuestionObj.options', this.currentQuestionObj.options)
        arrayObj.push(this.currentQuestionObj.options[i]);
        if (this.currentQuestionObj.options[i + 1] != undefined) {
          arrayObj.push(this.currentQuestionObj.options[i + 1]);
        }
        this.rows.push(arrayObj);
      }
    }
    if (question && question.indexOf('<blank>') > -1) {
      //console.log('blank');
      let replaceString = question;
      if (this.currentQuestionObj.type == 'FILL_IN_THE_BLANK') {
        let inputString = `<span class="ptag"><input style="border: none;outline:none;border-bottom: black 1px solid;"  type="text" id=${this.currentQuestionNo} value="${this.answerObj.questions[this.currentQuestionNo - 1].answer}" (change)="onInputChange('value')" name="answer${this.currentQuestionNo}" ></span>`
        replaceString = replaceString.replace('<blank>', inputString);
        heading.innerHTML = replaceString;
      } else {
        let inputString = `<p class="ptag"><input style="border: none;outline:none;border-bottom: black 1px solid;"  type="text" disabled name="answer${this.currentQuestionNo}"></p>`;
        replaceString = replaceString.replace('<blank>', inputString);
        heading.innerHTML = replaceString;
      }
    } else {
      heading.innerHTML = question;
    }
    //console.log('question', heading.innerHTML);
    this.renderMath();
    heading.classList.add('ptag');
    let id = document.getElementById(this.currentQuestionNo);
    if (id) {
      this.renderer.listen(id, 'change', (event) => {
        this.onInputChange(event.target.value);
      });
    }
    this.renderMath();
  }

  onPreviousClick() {
    this.currentQuestionNo = this.currentQuestionNo - 1;
    this.currentQuestionObj = this.practiceTestData.questions[this.currentQuestionNo - 1];
    this.setLocalStorage();
    this.generateQuestionTag();
  }
  onNextClick() {
    this.currentQuestionNo = this.currentQuestionNo + 1;
    this.currentQuestionObj = this.practiceTestData.questions[this.currentQuestionNo - 1];
    this.setLocalStorage();
    this.generateQuestionTag();
  }
  onEndTest() {
    this.answerObj.questions.forEach((ele, index) => {
      if (ele.optionsShown && ele.optionsShown.length) {
        if (this.practiceTestData.questions[index].type == 'MCO') {
          if (ele.answer && ele.answer.length) {
            ele.answer.forEach((subele, subIndex) => {

              let i = this.practiceTestData.questions[index].options.findIndex(v => v == subele);
              if (i > -1) {
                ele.answer[subIndex] = this.practiceTestData.questions[index].optionsIndices[i];
              }
            });
            //console.log('mco answer indices', ele.answer);
          }
          let i = this.practiceTestData.questions[index].options.findIndex(v => v == ele.answer);
          if (i > -1) {
            ele.answer = this.practiceTestData.questions[index].optionsIndices[i];
          }
        } else {
          let i = this.practiceTestData.questions[index].options.findIndex(v => v == ele.answer);
          if (i > -1) {
            ele.answer = this.practiceTestData.questions[index].optionsIndices[i];
          }
        }
      }
    });
    this.endTestClicked = true;
    localStorage.setItem('practiceTestGaurd', 'false')
    localStorage.setItem('timerFlag', 'false')
    this.answerObj.userId = this.user.user_id;
    //console.log('obj', this.answerObj);
    // this.answerObj.submitted_at = new Date().getTime();
    if (this.metaObj.type == 'RECALL') {
      this.practiceTestService.endRecallTest(this.answerObj).subscribe((res: any) => {
        if (res && res.success) {
          localStorage.setItem('testResultObj', JSON.stringify(res.data));
          this.toasterService.success(res.msg);
          this.destroyTestData();
          this.router.navigate(['/practice-test/result']);
        } else {
          localStorage.setItem('testResultError', res.msg);
          this.destroyTestData();
          this.router.navigate(['/practice-test/result']);
          this.toasterService.error(res.msg);
        }
      }, err => {
        this.destroyTestData();
        this.toasterService.error('End test failed!')

      })
    } else {
      this.practiceTestService.endPracticeTest(this.answerObj).subscribe((res: any) => {
        if (res && res.success) {
          localStorage.setItem('testResultObj', JSON.stringify(res.data));
          this.toasterService.success(res.msg);
          this.destroyTestData();
          this.router.navigate(['/practice-test/result']);
        } else {
          localStorage.setItem('testResultError', res.msg);
          this.destroyTestData();
          this.router.navigate(['/practice-test/result']);
          this.toasterService.error(res.msg);
        }
      }, err => {
        this.endTestClicked = false;
        this.toasterService.error('End test failed!')
      })
    }

  }
  destroyTestData() {
    //console.log('destrorf')
    clearInterval(this.timerInterval);
    this.endTestClicked = false;
    this.showResumeWindow = false;
    localStorage.removeItem('testLocalStorageObj')
    localStorage.removeItem('fullscreenMode')
  }
  timer() {
    let now = new Date().getTime();
    let distance: any = this.countDownDate - now;
    this.countDownHours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    this.countDownMinutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    this.countDownSeconds = Math.floor((distance % (1000 * 60)) / 1000);
    this.minutes = (this.countDownMinutes * 100) / this.practiceTestData.metaData.duration;
    this.seconds = (this.countDownSeconds * 100) / 60;
    if ((localStorage.getItem('timerFlag') != 'false' && localStorage.getItem('fullscreenMode') != 'false') || this.metaObj.type == 'RECALL') {
      if ((this.countDownMinutes <= 0 && this.countDownSeconds <= 0)) {
        this.onEndTest();
        clearInterval(this.timerInterval)
      }
      else {
        this.setLocalStorage();
      }
    }

  }
  setLocalStorage() {
    this.testLocalStorageObj.practiceTestObj = this.practiceTestData;
    this.testLocalStorageObj.timerObj.countDownMinutes = this.countDownMinutes;
    this.testLocalStorageObj.timerObj.countDownSeconds = this.countDownSeconds;
    this.testLocalStorageObj.timerObj.minutes = this.minutes;
    this.testLocalStorageObj.timerObj.seconds = this.seconds;
    this.testLocalStorageObj.timerObj.countDownDate = this.countDownDate;
    this.testLocalStorageObj.currentQuestionNo = this.currentQuestionNo;
    this.testLocalStorageObj.currentQuestionObj = this.currentQuestionObj;
    this.testLocalStorageObj.answerObj = this.answerObj;
    localStorage.setItem('testLocalStorageObj', JSON.stringify(this.testLocalStorageObj));
  }
  fullScreen() {
    this.fullscreenFlag = true;
    if (this.showResumeWindow) {
      this.modalRoot.nativeElement.style.visibility = 'visible';
      this.showResumeWindow = !this.showResumeWindow;
    }
    let elem: any = document.getElementById('practice-test');
    let methodToBeInvoked = elem.requestFullscreen ||
      elem.webkitRequestFullScreen || elem['mozRequestFullscreen']
      ||
      elem['msRequestFullscreen'];
    if (methodToBeInvoked) methodToBeInvoked.call(elem);
  }
  onResume() {
    if (this.testLocalStorageObj.timerObj.countDownMinutes != -1) {
      let countDownDate = new Date(new Date().getTime() + this.testLocalStorageObj.timerObj.countDownMinutes * 60000 + this.testLocalStorageObj.timerObj.countDownSeconds * 1000).getTime();
      this.countDownDate = countDownDate;
      this.countDownMinutes = this.testLocalStorageObj.timerObj.countDownMinutes;
      this.countDownSeconds = this.testLocalStorageObj.timerObj.countDownSeconds;
      this.minutes = this.testLocalStorageObj.timerObj.minutes;
      this.seconds = this.testLocalStorageObj.timerObj.seconds;
      this.timerInterval = setInterval(() => {
        this.timer();
      }, 1000);
    }
    this.fullScreen();
    localStorage.setItem('fullscreenMode', 'true');
  }
  renderMath() {
    this.updateMathObt();
    let angObj = this;
    // setTimeout(() => {
    angObj.mathJaxObject['Hub'].Queue(["Typeset", angObj.mathJaxObject.Hub], 'mathContent');
    // }, 10)
  }
  updateMathObt() {
    this.mathJaxObject = this.commonService.nativeGlobal()['MathJax'];
  }
  loadMathConfig() {
    this.updateMathObt();
    this.mathJaxObject.Hub.Config({
      showMathMenu: false,
      // tex2jax: { inlineMath: [["$", "$"]], displayMath: [["$$", "$$"]] },
      tex2jax: { inlineMath: [["$", "$"], ["\\(", "\\)"]], displayMath: [["$$", "$$"], ["$", "$"]] },
      menuSettings: { zoom: "Double-Click", zscale: "150%" },
      CommonHTML: { linebreaks: { automatic: true } },
      "HTML-CSS": { linebreaks: { automatic: true } },
      SVG: { linebreaks: { automatic: true } }
    });
  }
}