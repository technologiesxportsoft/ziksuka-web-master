import { Component, OnInit, ElementRef, ChangeDetectorRef, Renderer, AfterViewInit, AfterViewChecked, OnDestroy, ViewEncapsulation } from '@angular/core';
import { TestResultObj, TestResultQuestionObj } from 'src/app/shared/_model/practice-test';
import { PracticeTestService } from 'src/app/shared/_services/practice-test.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/internal/Subject';
import { CommonService } from 'src/app/shared/_services/common.service';

@Component({
  selector: 'app-test-result',
  templateUrl: './test-result.component.html',
  styleUrls: ['./test-result.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class TestResultComponent implements OnInit, AfterViewInit, AfterViewChecked, OnDestroy {

  testResultObj = new TestResultObj();
  showLoader = false;
  unsubscribeAll: Subject<boolean> = new Subject<boolean>();
  questionTagList = [];
  trueFalse = [{ 'key': 'true', 'value': 'True' }, { 'key': 'false', 'value': 'False' }];
  alphabets = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
  imageTag = "<image>";
  videoTag = "<video>";
  audioTag = "<audio>";

  explanation = [];
  mathJaxObject;
  resultError = false;
  error = '';
  constructor(private practiceTestService: PracticeTestService, private elementRef: ElementRef, private ref: ChangeDetectorRef,
    private renderer: Renderer, private toasterService: ToastrService, private router: Router, private commonService: CommonService) { }

  ngOnInit() {
    localStorage.removeItem('testLocalStorageObj')
    localStorage.removeItem('fullscreenMode')
    this.showLoader = true;
    if (localStorage.getItem('testResultObj') != undefined) {
      this.testResultObj = JSON.parse(localStorage.getItem('testResultObj'));
      console.log('test', this.testResultObj);
    } else if (localStorage.getItem('testResultError') != undefined) {
      this.error = localStorage.getItem('testResultError');
      this.resultError = true;
      this.showLoader = false;
    } else {
      this.showLoader = false;
    }
  }
  ngAfterViewChecked(): void {
    this.ref.detectChanges();
  }
  ngAfterViewInit() {
    this.setInitialData();
  }
  ngOnDestroy() {
    this.unsubscribeAll.next(true);
    this.unsubscribeAll.complete();
  }
  setInitialData() {
    if (this.testResultObj && this.testResultObj.questions && this.testResultObj.questions.length) {
      this.questionTagList = [];
      this.testResultObj.questions.forEach((ele, index) => {
        this.showLoader = false;
        this.generateQuestionTag(ele, index + 1);
      })
    } else {
      this.showLoader = false;
    }
  }
  generateQuestionTag(currentQuestionObj: TestResultQuestionObj, currentQuestionNo) {
    var heading = document.getElementById('heading' + currentQuestionNo);
    let question = currentQuestionObj.questionHtml;
    let rows = [];
    if (currentQuestionObj && currentQuestionObj.optionsShown) {
      for (let i = 0; i < currentQuestionObj.optionsShown.length; i = i + 2) {
        let arrayObj = [];
        arrayObj.push(currentQuestionObj.optionsShown[i]);
        if (currentQuestionObj.optionsShown[i + 1] != undefined) {
          arrayObj.push(currentQuestionObj.optionsShown[i + 1]);
        }
        rows.push(arrayObj);
      }
      if (currentQuestionObj.questionType == 'SCO') {
        // let i = currentQuestionObj.optionsIndices.findIndex(v => v == currentQuestionObj.answer);
        let correctIndex = currentQuestionObj.optionsIndices.findIndex(v => v == currentQuestionObj.correctAnswer);
        // currentQuestionObj.answer = i > -1 ? currentQuestionObj.optionsShown[i] : ''
        currentQuestionObj.correctAnswerString = correctIndex > -1 ? currentQuestionObj.optionsShown[correctIndex] : ''
      } else if (currentQuestionObj.questionType == 'MCO') {
        let answer = [];
        if (currentQuestionObj.correctAnswer && currentQuestionObj.correctAnswer.length) {
          currentQuestionObj.correctAnswer.forEach((ans, i) => {
            let correctAnsIndex = currentQuestionObj.optionsIndices.findIndex(v => v == ans);
            answer.push(correctAnsIndex > -1 ? currentQuestionObj.optionsShown[correctAnsIndex] : '');
          })
        }
        currentQuestionObj.correctAnswerString = [...answer];
      }
      currentQuestionObj["rows"] = rows;
    } else {
      currentQuestionObj.correctAnswerString = currentQuestionObj.correctAnswer;
    }
    if (question.indexOf('<blank>') > -1) {
      let replaceString = question;
      if (currentQuestionObj.questionType == 'FILL_IN_THE_BLANK') {
        let inputString = '';
        if (!!currentQuestionObj.answer) {
          // currentQuestionObj.correctAnswer == currentQuestionObj.answer
          if (currentQuestionObj.isCorrect) {
            inputString = `<span class="ptag"><input style="border: none;outline:none;border-bottom: black 1px solid;color:#3da93d""  disabled type="text" id=${currentQuestionNo} value="${currentQuestionObj.answer}"  name="answer${currentQuestionNo}" ></span>`
          } else {
            inputString = `<span class="ptag"><input style="border: none;outline:none;border-bottom: black 1px solid;color:#ed2121bf"  disabled type="text" id=${currentQuestionNo} value="${currentQuestionObj.answer}"  name="answer${currentQuestionNo}" ></span>`
          }
        } else {
          inputString = `<span class="ptag"><input style="border: none;outline:none;border-bottom: black 1px solid;color:#ed2121bf"  disabled type="text" id=${currentQuestionNo} value=""  name="answer${currentQuestionNo}" ></span>`
        }
        replaceString = replaceString.replace('<blank>', inputString);
        heading.innerHTML = replaceString;
      } else {
        let inputString = `<p class="ptag"><input style="border: none;outline:none;border-bottom: black 1px solid;"  type="text" disabled name="answer${currentQuestionNo}"></p>`;
        replaceString = replaceString.replace('<blank>', inputString);
        heading.innerHTML = replaceString;
      }
    } else {
      heading.innerHTML = question;
    }
    heading.classList.add('ptag');
    if (currentQuestionObj.explanation !== this.imageTag && currentQuestionObj.explanation !== this.videoTag && currentQuestionObj.explanation !== this.audioTag) {
      this.explanation.push(currentQuestionObj.explanation);
    } else {
      this.explanation.push(null);
    }
    this.renderMath();
  }
  onDone() {
    this.router.navigate(['./dashboard']);
    localStorage.removeItem('testResultObj');
    localStorage.removeItem('testResultError');
  }
  updateMathObt() {
    this.mathJaxObject = this.commonService.nativeGlobal()['MathJax'];
  }

  renderMath() {
    this.updateMathObt();
    let angObj = this;
    // setTimeout(() => {
    angObj.mathJaxObject['Hub'].Queue(["Typeset", angObj.mathJaxObject.Hub], 'mathContent');
    // }, 1000)
  }
  loadMathConfig() {
    this.updateMathObt();
    this.mathJaxObject.Hub.Config({
      showMathMenu: false,
      // tex2jax: { inlineMath: [["$", "$"]], displayMath: [["$$", "$$"]] },
      tex2jax: { inlineMath: [["$", "$"], ["\\(", "\\)"]], displayMath: [["$$", "$$"], ["$", "$"]] },
      menuSettings: { zoom: "Double-Click", zscale: "150%" },
      CommonHTML: { linebreaks: { automatic: true } },
      "HTML-CSS": { linebreaks: { automatic: true } },
      SVG: { linebreaks: { automatic: true } }
    });
  }
}
