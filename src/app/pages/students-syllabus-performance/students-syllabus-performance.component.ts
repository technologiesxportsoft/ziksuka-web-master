import { Component, OnInit } from "@angular/core";
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_material from "@amcharts/amcharts4/themes/material";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import { GradeService } from "src/app/shared/_services/grade.service";
import { StudentService } from "src/app/shared/_services/student.service";
am4core.useTheme(am4themes_material);
am4core.useTheme(am4themes_animated);
am4core.options.autoSetClassName = true;
declare var jQuery;
declare var seriesDataItem;
import { UserRole } from '../../shared/_enumerations/UserRole';
import { AuthenticationService } from 'src/app/shared/_services/auth.service';
@Component({
  selector: "app-students-syllabus-performance",
  templateUrl: "./students-syllabus-performance.component.html",
  styleUrls: ["./students-syllabus-performance.component.css"]
})
export class StudentsSyllabusPerformanceComponent implements OnInit {
  public UserRole = UserRole;
  currentUserData: any;
  syllabusChart: any;
  performanceChart: any;
  syllabusData = [];
  performanceData = [];
  gradeList: any = [];
  studentList:any=[];
  searchGradeTerm = "";
  searchStudentTerm = "";
  constructor(
    private authenticationService: AuthenticationService,
    private gradeService: GradeService,
    private studentService: StudentService
  ) {
    this.currentUserData = this.authenticationService.currentUserValue;
    this.getGradeList();
  }
  ngOnInit() {}
  ngOnDestroy() {
    if (this.syllabusChart) {
      this.syllabusChart.dispose();
    }
    if (this.performanceChart) {
      this.performanceChart.dispose();
    }
  }
  getGradeList() {
    this.gradeList = [];
    this.gradeService.getGradeListByUserIdV2(this.currentUserData.user_id).subscribe((res: any) => {
      this.gradeList = res.data;
      if (this.gradeList.length) {
        this.gradeList.forEach((grade, index) => {
          grade.imgTitle = null;
          let first_letter = grade.schoolGradeName as string;
          let last_letter = grade.sectionName as string;
          grade.imgTitle = first_letter + last_letter;
          grade.filterString = first_letter + last_letter;
          grade.isActive = false;
        })
      }
      this.gradeList[0].isActive = true;
      //Get Syllabus
      this.getGradeSyllabus(this.gradeList[0].schoolGradeSectionId);
      //Get Performance
      this.getGradePerformance(this.gradeList[0].schoolGradeSectionId);
      //GetStudentList
      this.getStudentList(this.gradeList[0].schoolGradeSectionId);
    });
  }
  getStudentList(sectionId) {
    this.studentService.getStudentListByGradeSectionV2(sectionId).subscribe(res => {
      this.studentList = res["data"];
      this.studentList.forEach((student: any) => {

        let first_name = student.firstName as string;
        let last_name = student.lastName as string;
        student.filterString = first_name + ' ' + last_name;
        student.imgTitle = first_name.substring(0, 1).toUpperCase() + last_name.substring(0, 1).toUpperCase();
        student.isActive = false;
      })
    })
  }
  getStudentPerformance(studentId){
    this.studentService.getPerformance(studentId).subscribe(res=>{
      this.performanceData = [];
      (res["data"] as []).forEach((subject: any) => {
        var dataset = {
          category: subject.subject_name,
          value: subject.percentage_scored,
          full: 100
        };
        this.performanceData.push(dataset);
      });
      this.getPerformanceChart();
    })
  }
  studentSelection(studentObject) {
    this.getStudentPerformance(studentObject.userId);
    (this.studentList as []).forEach((student: any) => {
      student.isActive = false;
    });
    studentObject.isActive = true;
  }
  gradeSelection(gradeObject) {
    this.getStudentList(gradeObject.schoolGradeSectionId)
    this.getGradePerformance(gradeObject.schoolGradeSectionId);
    this.getGradeSyllabus(gradeObject.schoolGradeSectionId);
    (this.gradeList as []).forEach((grade: any) => {
      grade.isActive = false;
    });
    gradeObject.isActive = true;
  }
  getGradePerformance(sectionId) {
    this.gradeService.getPerformance(sectionId).subscribe(res => {
      this.performanceData = [];
      (res["data"] as []).forEach((subject: any) => {
        var dataset = {
          category: subject.subject_name,
          value: subject.percentage_scored,
          full: 100
        };
        this.performanceData.push(dataset);
      });
      this.getPerformanceChart();
    });
  }
  getGradeSyllabus(gradeId) {
    this.gradeService.getSyllabusIndicator(gradeId).subscribe(res => {
      this.syllabusData = [];
      (res["data"] as []).forEach((subject: any) => {
        var dataset = {
          category: subject.subject_name,
          value: subject.percentage_completion,
          full: 100
        };
        this.syllabusData.push(dataset);
      });
      this.getSyllybusChart();
    });
  }
  getSyllybusChart() {
    if (this.syllabusChart) {
      this.syllabusChart.dispose();
    }
    let chart = am4core.create("syllabus_chartdiv", am4charts.RadarChart);
    this.syllabusChart = chart;
    chart.data = this.syllabusData;

    // Make chart not full circle
    chart.startAngle = -90;
    chart.endAngle = 270;
    chart.innerRadius = am4core.percent(20);
    chart.height=327;
    chart.width=480;
    chart.paddingRight = 0;
    chart.paddingLeft = 0;
    chart.paddingTop = 0;
    chart.paddingBottom = 0;
    // Set number format
    chart.numberFormatter.numberFormat = "#.#'%'";

    // Create axes
    var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis() as any);
    categoryAxis.dataFields.category = "category";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.grid.template.strokeOpacity = 0;
    categoryAxis.renderer.labels.template.horizontalCenter = "right";
    categoryAxis.renderer.labels.template.fontWeight = 500;
    categoryAxis.renderer.labels.template.adapter.add("fill", function(
      fill,
      target
    ) {
      return target.dataItem.index >= 0
        ? chart.colors.getIndex(target.dataItem.index)
        : fill;
    });
    categoryAxis.renderer.minGridDistance = 10;

    var valueAxis = chart.xAxes.push(new am4charts.ValueAxis() as any);
    valueAxis.renderer.grid.template.strokeOpacity = 0;
    valueAxis.min = 0;
    valueAxis.max = 100;
    valueAxis.strictMinMax = true;

    // Create series
    var series1 = chart.series.push(new am4charts.RadarColumnSeries());
    series1.dataFields.valueX = "full";
    series1.dataFields.categoryY = "category";
    series1.clustered = false;
    series1.columns.template.fill = new am4core.InterfaceColorSet().getFor(
      "alternativeBackground"
    );
    series1.columns.template.fillOpacity = 0.08;
    // series1.columns.template.cornerRadiusTopLeft = 20;
    series1.columns.template.strokeWidth = 0;
    series1.columns.template.radarColumn.cornerRadius = 20;

    var series2 = chart.series.push(new am4charts.RadarColumnSeries());
    series2.dataFields.valueX = "value";
    series2.dataFields.categoryY = "category";
    series2.clustered = false;
    series2.columns.template.strokeWidth = 0;
    series2.columns.template.tooltipText = "{category}: [bold]{value}[/]";
    series2.columns.template.radarColumn.cornerRadius = 20;

    series2.columns.template.adapter.add("fill", function(fill, target) {
      return chart.colors.getIndex(target.dataItem.index);
    });

    chart.legend = new am4charts.Legend();
    chart.legend.reverseOrder = true;
    series2.events.on("dataitemsvalidated", function() {
      var data = [];
      series2.dataItems.each(function(dataItem) {
        data.push({
          name: dataItem.categoryY,
          fill: dataItem.column.fill,
          seriesDataItem: dataItem
        });
      });
      chart.legend.data = data;
      chart.legend.maxWidth = undefined;
      chart.legend.position = "right";
      chart.legend.itemContainers.template.events.on("toggled", function(
        event
      ) {
        var seriesDataItem = (event.target.dataItem.dataContext as any)
          .seriesDataItem;
        if (event.target.isActive) {
          seriesDataItem.hide(series2.interpolationDuration, 0, 0, ["valueX"]);
        } else {
          seriesDataItem.show(series2.interpolationDuration, 0, ["valueX"]);
        }
      });
    });
  }
  getPerformanceChart() {
    if (this.performanceChart) {
      this.performanceChart.dispose();
    }
    let chart = am4core.create("performance_chartdiv", am4charts.RadarChart);
    this.performanceChart = chart;
    chart.data = this.performanceData;

    // Make chart not full circle
    chart.startAngle = -90;
    chart.endAngle = 270;
    chart.innerRadius = am4core.percent(20);
    chart.height=327;
    chart.width=480;
    chart.paddingRight = 0;
    chart.paddingLeft = 0;
    chart.paddingTop = 0;
    chart.paddingBottom = 0;
    // Set number format
    chart.numberFormatter.numberFormat = "#.#'%'";

    // Create axes
    var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis() as any);
    categoryAxis.dataFields.category = "category";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.grid.template.strokeOpacity = 0;
    categoryAxis.renderer.labels.template.horizontalCenter = "right";
    categoryAxis.renderer.labels.template.fontWeight = 500;
    categoryAxis.renderer.labels.template.adapter.add("fill", function(
      fill,
      target
    ) {
      return target.dataItem.index >= 0
        ? chart.colors.getIndex(target.dataItem.index)
        : fill;
    });
    categoryAxis.renderer.minGridDistance = 10;

    var valueAxis = chart.xAxes.push(new am4charts.ValueAxis() as any);
    valueAxis.renderer.grid.template.strokeOpacity = 0;
    valueAxis.min = 0;
    valueAxis.max = 100;
    valueAxis.strictMinMax = true;

    // Create series
    var series1 = chart.series.push(new am4charts.RadarColumnSeries());
    series1.dataFields.valueX = "full";
    series1.dataFields.categoryY = "category";
    series1.clustered = false;
    series1.columns.template.fill = new am4core.InterfaceColorSet().getFor(
      "alternativeBackground"
    );
    series1.columns.template.fillOpacity = 0.08;
    // series1.columns.template.cornerRadiusTopLeft = 20;
    series1.columns.template.strokeWidth = 0;
    series1.columns.template.radarColumn.cornerRadius = 20;

    var series2 = chart.series.push(new am4charts.RadarColumnSeries());
    series2.dataFields.valueX = "value";
    series2.dataFields.categoryY = "category";
    series2.clustered = false;
    series2.columns.template.strokeWidth = 0;
    series2.columns.template.tooltipText = "{category}: [bold]{value}[/]";
    series2.columns.template.radarColumn.cornerRadius = 20;

    series2.columns.template.adapter.add("fill", function(fill, target) {
      return chart.colors.getIndex(target.dataItem.index);
    });

    chart.legend = new am4charts.Legend();
    chart.legend.reverseOrder = true;
    series2.events.on("dataitemsvalidated", function() {
      var data = [];
      series2.dataItems.each(function(dataItem) {
        data.push({
          name: dataItem.categoryY,
          fill: dataItem.column.fill,
          seriesDataItem: dataItem
        });
      });
      chart.legend.data = data;
      chart.legend.maxWidth = undefined;
      chart.legend.position = "right";
      chart.legend.itemContainers.template.events.on("toggled", function(
        event
      ) {
        var seriesDataItem = (event.target.dataItem.dataContext as any)
          .seriesDataItem;
        if (event.target.isActive) {
          seriesDataItem.hide(series2.interpolationDuration, 0, 0, ["valueX"]);
        } else {
          seriesDataItem.show(series2.interpolationDuration, 0, ["valueX"]);
        }
      });
    });
  }
}
