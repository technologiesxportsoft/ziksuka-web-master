import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsSyllabusPerformanceComponent } from './students-syllabus-performance.component';

describe('StudentsSyllabusPerformanceComponent', () => {
  let component: StudentsSyllabusPerformanceComponent;
  let fixture: ComponentFixture<StudentsSyllabusPerformanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsSyllabusPerformanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsSyllabusPerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
