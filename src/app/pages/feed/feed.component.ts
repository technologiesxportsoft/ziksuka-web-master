import { Component, OnInit } from '@angular/core';
import { UserInfo } from '../../shared/_model/user-info';
import { DashboardService } from '../../shared/_services/dashboard.service';
import { AuthenticationService } from '../../shared/_services/auth.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {
  user: UserInfo;
  books: any;
  feeds = [];
  showNotification: boolean = true;
  constructor(private dashboardService: DashboardService, private authenticationService: AuthenticationService) {

  }

  ngOnInit() {
    this.user = this.authenticationService.currentUserValue;
    if(this.user.role === 5){
      this.GetFeed();
    }
  }
  GetFeed() {
    var feedModel: Object = {
      userId: this.user.user_id
    }
    this.dashboardService.GetFeed(feedModel).subscribe((res) => {
      var result = res as any;
      var todayDate = new Date();
      this.feeds = [];
      result.data.forEach(item => {
        if(todayDate < item.expiry ){
            this.feeds.push(item.newsFeed + "    ");
        }
      });
      if (this.feeds.length == 0) {
        this.feeds.push("No new notices");
      }
    }, (err) => {
      console.log(err);
    })
  }
}
