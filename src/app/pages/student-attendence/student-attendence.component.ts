import { Component, OnInit } from '@angular/core';
import { StudentService } from 'src/app/shared/_services/student.service';
import { StudentAttendence } from 'src/app/shared/_model/StudentAttendence';
import { Router, ActivatedRoute } from '@angular/router';
import { UserInfo } from 'src/app/shared/_model/user-info';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { AuthenticationService } from 'src/app/shared/_services/auth.service';

@Component({
	selector: 'app-student-attendence',
	templateUrl: './student-attendence.component.html',
	styleUrls: ['./student-attendence.component.css']
})
export class StudentAttendenceComponent implements OnInit {
	studentsList: []
	gradeId: any;
	user: UserInfo;
	studentAttendence: StudentAttendence;
	sectionId;
	showLoader = true;
	datePipe = new DatePipe("en-IN");

	constructor(private studentService: StudentService, private router: Router, private toastr: ToastrService,
		private route: ActivatedRoute, private authenticationService: AuthenticationService) {
		this.user = this.authenticationService.currentUserValue;
		
		this.newStudentAttendenceModel();
	}

	ngOnInit() {
		this.sectionId = this.route.snapshot.paramMap.get("sectionId");
		this.getStudentList();
	}

	getStudentList() {
		this.studentService.getStudentAttendence(this.sectionId).subscribe((res: any) => {
			this.studentsList = res["data"].students as [];
			this.studentsList.forEach((student: any) => {
				let first_name = student.firstName as string;
				let last_name = student.lastName as string;
				student.imgTitle = first_name.substring(0, 1).toUpperCase() + last_name.substring(0, 1).toUpperCase();
			})
			this.showLoader = false;
		})
	}

	saveAttendence() {
		let attendanceObj = {}
		let students = []
		this.studentsList.forEach((student: any) => {
			let attendance = {
				studentId: student.userId,
				isPresent: student.attendance
			}
			students.push(attendance);
			console.log(student);
		});

		attendanceObj['teacherId'] = this.user.user_id;
		attendanceObj['schoolGradeSectionId'] = this.sectionId;
		attendanceObj['date'] = this.datePipe.transform(new Date(), "yyyy-MM-dd");
		attendanceObj['students'] = students;

		this.studentService.SaveStudentAttendence(attendanceObj).subscribe(
			(res) => {
				if (res['success']) {
					this.toastr.success(res["msg"].toString())
				} else {
					this.toastr.error(res["msg"].toString())
				}
			}, (err) => {
				this.toastr.error('Something went wrong. Please try again!');
			}
		);
	}

	newStudentAttendenceModel() {
		this.studentAttendence = new StudentAttendence(
			this.gradeId,
			[],
			[],
		);
	}
}
