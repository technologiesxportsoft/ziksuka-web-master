import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrincipalDashboardInfoComponent } from './principal-dashboard-info.component';

describe('PrincipalDashboardInfoComponent', () => {
  let component: PrincipalDashboardInfoComponent;
  let fixture: ComponentFixture<PrincipalDashboardInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrincipalDashboardInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrincipalDashboardInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
