import { Component, OnInit } from '@angular/core';
import { PrincipalService } from 'src/app/shared/_services/principal.service';
import { NgbModal, NgbTabsetConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-principal-dashboard-info',
  templateUrl: './principal-dashboard-info.component.html',
  styleUrls: ['./principal-dashboard-info.component.css']
})
export class PrincipalDashboardInfoComponent implements OnInit {
  user:any;
  dashboardData:any=[];
  popopUpTeachersList: any=[];
  popopUpStudentsList: any=[];
    constructor(private principalService:PrincipalService,private modalService: NgbModal,config: NgbTabsetConfig) {
    this.user = JSON.parse(localStorage.getItem('userInfo'));
      config.justify = 'center';
    this.getDashboard();
  }

  ngOnInit() {
  }
  getDashboard(){
    this.principalService.getDashboard(this.user.schoolId,this.user.user_id).subscribe(res=>{
      this.dashboardData = res["data"];
    })
  }
  openPopUpTeachersList(list,popUp){
    this.popopUpTeachersList=list;
    this.popopUpTeachersList.forEach((person: any) => {
      let first_name = person.first_name as string;
      let last_name = person.last_name as string;
      person.imgTitle = first_name.substring(0, 1).toUpperCase() + last_name.substring(0, 1).toUpperCase();
    })
    this.modalService.open(popUp, { size: 'lg',centered: true})
  }
  openPopUpStudentsList(list : [],popUp){
    let grades= Object.keys(list);
    let attendence = Object.values(list);
    grades.forEach((item,index)=>{
      this.popopUpStudentsList[index]=[]
      this.popopUpStudentsList[index].gradeName=item;
      this.popopUpStudentsList[index].atendenceList=attendence[index];
      this.popopUpStudentsList[index].atendenceList.forEach((person: any) => {
        let first_name = person.first_name as string;
        let last_name = person.last_name as string;
        person.imgTitle = first_name.substring(0, 1).toUpperCase() + last_name.substring(0, 1).toUpperCase();
      })
    })
    this.modalService.open(popUp, { size: 'lg',centered: true})
  }
}
