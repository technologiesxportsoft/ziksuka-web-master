import { Component, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { UserInfo } from "src/app/shared/_model/user-info";
import { AuthenticationService } from "src/app/shared/_services/auth.service";
import { BooksService } from "../../shared/_services/books.services";
import { UserRole } from "../../shared/_enumerations/UserRole";
import * as _ from "lodash";
import { GLOBAL_CONSTANT } from "../../shared/lib/common/utils/Constants";

@Component({
  selector: "app-book",
  templateUrl: "./book.component.html",
  styleUrls: ["./book.component.css"],
})
export class BookComponent implements OnInit {
  public UserRole = UserRole;
  user: UserInfo;
  subjectsList = [];
  showLoader = false;
  subjectId: any;
  zIndex: any;
  pdfSrc = "";
  selectedSubject: any;
  bookList = [];
  paginatedBookList = [];
  totalBooks = 0;
  maxBookPerPage = GLOBAL_CONSTANT.MAX_BOOK_PER_PAGE;
  totalPage = 0;
  Pagination: any[];
  CurrentPage = 1;
  selectedBookTitle = "";
  currentUserSection;
  constructor(
    private modal: NgbModal,
    private authenticationService: AuthenticationService,
    private booksService: BooksService
  ) {}

  ngOnInit() {
    this.user = this.authenticationService.currentUserValue;
    this.getSubjects();
    setTimeout(() => {
      this.showLoader = false;
    }, 2000);
  }

  getSubjects() {
    this.showLoader = true;
    if (this.user.role == UserRole.Student) {
      this.currentUserSection = this.authenticationService.currentUserValue.school_grade_section_id;
      this.booksService.GetSubjects(this.currentUserSection).subscribe((res) => {
        let ApiResult = res as any;
        this.subjectsList.push({
          subjectId: GLOBAL_CONSTANT.ALL_SUBJECTS_ID,
          SubjectName: GLOBAL_CONSTANT.ALL_SUBJECTS,
        });
        for (let subject of ApiResult.data) {
          this.subjectsList.push({
            schoolGradeSubjectId: subject.schoolGradeSubjectId,
            SubjectName: subject.schoolSubjectName,
            schoolSubjectId: subject.schoolSubjectId,
            subjectId: Number(subject.subjectId),
          });
        }
        this.selectedSubject = this.subjectsList[0];
        this.subjectId = this.subjectsList[0].subjectId;
        this.getBookList();
      });
    } else {
      this.subjectId = GLOBAL_CONSTANT.ALL_SUBJECTS_ID;
      this.getBookList();
    }
  }

  onChangeTab(subject) {
    this.selectedSubject = subject;
    this.subjectId = subject.subjectId;
    this.getBookList();
    this.changePagination(1);
  }
  getBookList() {
    let obj = {
      user_id: this.user.user_id,
      subject_id: this.subjectId
    };
    this.showLoader = true;
    this.booksService.GetBooks(obj).subscribe((res: any) => {
      if (res.success && res.data.subjectBooks.length > 0) {
        this.bookList = [];
        this.paginatedBookList = [];
        this.bookList = _.cloneDeep(res.data.subjectBooks);
        this.paginatedBookList = this.bookList.splice(0, this.maxBookPerPage);
        var len = Math.ceil(+(this.bookList.length / this.maxBookPerPage));
        this.totalPage = len;

        this.Pagination = [];
        for (let index = 1; index <= len; index++) {
          this.Pagination.push(index);
        }
      }
      this.showLoader = false;
    });
  }
  changePagination(i) {
    this.CurrentPage = i;
    this.paginatedBookList = [];
    this.paginatedBookList = this.bookList.slice(
      this.maxBookPerPage * (i - 1),
      this.maxBookPerPage * i
    );
  }

  onNext() {
    if (this.totalPage >= this.CurrentPage + 1) {
      this.changePagination(this.CurrentPage + 1);
    }
  }
  onPrevious() {
    if (this.CurrentPage - 1 > 0) {
      this.changePagination(this.CurrentPage - 1);
    }
  }
}
