import { NgModule } from '@angular/core';
import { BookRoutingModule } from './book-routing.module';
import { BookComponent } from './book.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';


@NgModule({
  declarations: [BookComponent],
  imports: [
    SharedModule,
    BookRoutingModule,
    NgxExtendedPdfViewerModule
  ],
  entryComponents: []
})
export class BookModule { }
