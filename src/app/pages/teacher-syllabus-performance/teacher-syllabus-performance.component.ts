import { Component, OnInit } from '@angular/core';
import { UserRole } from 'src/app/shared/_enumerations/UserRole';
import { UserInfo } from 'src/app/shared/_model/user-info';
import { GradeService } from 'src/app/shared/_services/grade.service';
import { TeacherService } from 'src/app/shared/_services/teacher.service';
@Component({
  selector: 'app-teacher-syllabus-performance',
  templateUrl: './teacher-syllabus-performance.component.html',
  styleUrls: ['./teacher-syllabus-performance.component.css']
})
export class TeacherSyllabusPerformanceComponent implements OnInit {

  public UserRole = UserRole;
  user: UserInfo;
  gradelist:any=[];
  userId: string = "0";
  syllabusData = [];
  performanceData = [];
  teacherList:any=[];
  searchTeacherTerm = "";
  schoolId=1;
  constructor(
    private gradeService: GradeService,
    private teacherService: TeacherService
  ) {
    this.user = JSON.parse(localStorage.getItem('userInfo'));
    this.userId =
      !localStorage.getItem("userInfo") !== null
        ? (localStorage.getItem("userInfo") as any).user_id
        : 0;
        this.getTeacherList(this.schoolId);
  }
  ngOnInit() {}
  getTeacherList(schoolId){
    this.teacherService.getTeacherListBySchool(schoolId).subscribe(res=>{
      this.teacherList= res["data"];
      this.teacherList.forEach((teacher:any)=>{

        let first_name= teacher.first_name as string;
        let last_name= teacher.last_name as string;
        teacher.imgTitle= first_name.substring(0,1).toUpperCase()+last_name.substring(0,1).toUpperCase();
        teacher.isActive=false;
      })
      this.teacherSelection(this.teacherList[0])
    })
  }
  getTeacherPerformance(teacherId,schoolId){
    teacherId=12;
    schoolId=2;
    this.teacherService.getTeacherPerformance(teacherId,schoolId).subscribe(res=>{
      let array = Object.values((res["data"] as []))
      this.performanceData = [];
      this.syllabusData = [];
      array.forEach((grade,index) => {
        this.performanceData[index] = [];
        this.syllabusData[index] = [];
        this.performanceData[index].data = [];
        this.syllabusData[index].data = [];
        this.performanceData[index].id = "teacher_performance_chartdiv_"+index;
        this.syllabusData[index].id = "teacher_syllabus_chartdiv_"+index;
        (grade as []).forEach((subject:any)=>{
          var dataset = {
            category: subject.subject_name, 
            value: subject.percentage_scored,
            full: 100
          };
          this.performanceData[index].data.push(dataset);
          this.syllabusData[index].data.push(dataset);
        })
      });
      this.gradelist=Object.keys((res["data"] as []));
    })
  }
  teacherSelection(teacherObject){
    this.getTeacherPerformance(teacherObject.user_id,teacherObject.school_id);
    (this.teacherList as []).forEach((teacher: any) => {
      teacher.isActive = false;
    });
    teacherObject.isActive = true;
  }
}
