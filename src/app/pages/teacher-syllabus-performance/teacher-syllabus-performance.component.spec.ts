import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherSyllabusPerformanceComponent } from './teacher-syllabus-performance.component';

describe('TeacherSyllabusPerformanceComponent', () => {
  let component: TeacherSyllabusPerformanceComponent;
  let fixture: ComponentFixture<TeacherSyllabusPerformanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherSyllabusPerformanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherSyllabusPerformanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
