import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { SlickCarouselComponent } from 'ngx-slick-carousel';
import { NgbModalConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { GradeTestConfigurationComponent } from '../../shared/_components/grade-test-configuration/grade-test-configuration.component';
import { DashboardService } from '../../shared/_services/dashboard.service';
import { UserInfo } from '../../shared/_model/user-info';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { TestConfigurationComponent } from '../../shared/_components/test-configuration/test-configuration.component';
import { CommonService } from 'src/app/shared/_services/common.service';

@Component({
	selector: 'app-teacher-slots',
	templateUrl: './teacher-slots.component.html',
	styleUrls: ['./teacher-slots.component.css']
})
export class TeacherSlotsComponent implements OnInit {
	user: UserInfo;
	timetableDailySchedules = [];
	public today = new Date();
	showLoader = true;

	selectedTimeslot: any;
	classMode: number;
	meetUrl: string;
	skypeUrl: string;
	zoomUrl: string;
	notificationPopup;

	CLASS_MODE = {
		MEET: 1,
		ZOOM: 2,
		SKYPE: 3
	}

	MEET_NEW_MEETING_URL = "https://meet.google.com/new";
	ZOOM_NEW_MEETING_URL = "https://zoom.us/start/webmeeting";
	SKYPE_NEW_MEETING_URL = "https://www.skype.com/en/free-conference-call";

	@ViewChild('slickModal', { static: true }) slickModal: SlickCarouselComponent;
	constructor(
		config: NgbModalConfig,
		private modalService: NgbModal,
		private router: Router,
		private dashboardService: DashboardService,
		private toasterService: ToastrService,
		private commonService: CommonService,
		private changeDetectorRef: ChangeDetectorRef
	) {
		config.backdrop = 'static';
	}

	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem('userInfo'));
		this.GetTimeTableDailySchedule();
	}

	ngAfterViewInit() {
		this.showLoader = false;
		this.changeDetectorRef.detectChanges();
	}

	slideConfig = {
		slidesToShow: 4,
		slidesToScroll: 2,
		infinite: false,
		arrows: false,
		responsive: [
			{
				breakpoint: 1181,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: false,
				},
			},
			{
				breakpoint: 926,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				},
			},
			{
				breakpoint: 567,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				},
			},
		],
	};


	next() {
		this.slickModal.slickNext();
	}


	back() {
		this.slickModal.slickPrev();
	}


	openPopUp(school_grade_section_id, school_subject_name, school_subject_id, school_grade_subject_id) {
		let modelRef = this.modalService.open(TestConfigurationComponent, {
			size: "lg",
			centered: true,
		});
		modelRef.componentInstance.school_grade_section_id = school_grade_section_id;
		modelRef.componentInstance.showInformationForRecall = false;
		modelRef.componentInstance.school_subject_name = school_subject_name;
		modelRef.componentInstance.school_subject_id = school_subject_id
		modelRef.componentInstance.school_grade_subject_id = school_grade_subject_id
	}


	GetTimeTableDailySchedule() {
		this.dashboardService
			.GetTimeTableDailySchedule(this.user.user_id)
			.subscribe(
				(res) => {
					var timeTableMap = res["data"].timetable as Map<String, Array<any>>;
					for (let key in timeTableMap) {
						let timeScheduleList = timeTableMap[key];
						if (timeScheduleList == null || timeScheduleList.length == 0) {
							continue;
						}
						let timeSchedule = timeTableMap[key][0];
						if (timeSchedule == null || timeSchedule.break == true) {
							continue;
						}
						timeSchedule.starttime =
							timeSchedule.start_time >= 1000
								? moment(timeSchedule.start_time, "HH:mm").format("LT")
								: moment("0" + timeSchedule.start_time, "HH:mm").format("LT");
						timeSchedule.endtime =
							timeSchedule.end_time >= 1000
								? moment(timeSchedule.end_time, "HH:mm").format("LT")
								: moment("0" + timeSchedule.end_time, "HH:mm").format("LT");

						this.timetableDailySchedules.push(timeSchedule);
					}
				},
				(err) => {
					console.log(err);
				}
			);
	}


	isActiveSlot(slot) {
		// return true;
		let hours = this.today.getHours();
		let minutes = this.today.getMinutes();
		let currentTime = hours + "" + (minutes >= 10 ? minutes : "0" + minutes);
		if (
			parseInt(currentTime) >=
			parseInt(slot.start_time >= 1000 ? slot.start_time : "0" + slot.start_time) &&
			parseInt(currentTime) <=
			parseInt(slot.end_time >= 1000 ? slot.end_time : "0" + slot.end_time)
		) {
			return true;
		} else {
			return false;
		}
	}

	startLiveClass(timetableSlot: any) {
		if (!confirm("Please confirm if you want to start this class?")) {
			return;
		}
		this.showLoader = true;
		this.dashboardService.startLiveClass(timetableSlot, this.user.user_id).subscribe(
			(classroom) => {
				if (classroom.success) {
					let url = window.location.protocol + "//" + window.location.host + "/#/classroom/" + classroom.data.classroomId;
					window.open(url, '_self');
				} else {
					this.toasterService.error(classroom.msg);
					this.showLoader = false;
				}
			},
			(err) => {
				this.toasterService.error('Something went wrong. Please try again!');
				this.showLoader = false;
			});
	}

	startLiveClassV2(timetableSlot: any) {
		if (!confirm('Please confirm if you want to start this class?')) {
			return;
		}
		this.showLoader = true;

		this.dashboardService.startLiveClassV2(timetableSlot, this.user.user_id).subscribe(
			(classroom) => {
				if (classroom.success) {
					// alert("The live class is scheduled. Please check your notifications.");
					let url = classroom.data.launchUrl;
					window.location.href = url;
				} else {
					this.toasterService.error(classroom.msg);
					this.showLoader = false;
				}
			}, (err) => {
				this.toasterService.error('Something went wrong. Please check your internet and try again!');
				this.showLoader = false;
			});
	}

	classModeSelection(timetableSlot: any, classSelectionPopup: any) {
		this.selectedTimeslot = timetableSlot;
		this.modalService.open(classSelectionPopup, { ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true });
	}

	startLiveClassV3(classMode: number, classroomUrlPopup: any, notificationPopup: any) {
		this.classMode = classMode;
		this.notificationPopup = notificationPopup;
		

		switch (classMode) {
			case this.CLASS_MODE.MEET: {
				if (!!this.selectedTimeslot.meet_url) {
					window.open(this.selectedTimeslot.meet_url);
					this.notifyStudents(this.selectedTimeslot.meet_url, false);
				} else {
					window.open(this.MEET_NEW_MEETING_URL);
					this.modalService.open(classroomUrlPopup, { ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true });
				}
				break;
			}
			case this.CLASS_MODE.ZOOM: {
				window.open(this.ZOOM_NEW_MEETING_URL);
				this.modalService.open(classroomUrlPopup, { ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true });
				break;
			}
			case this.CLASS_MODE.SKYPE: {
				if (!!this.selectedTimeslot.skype_url) {
					window.open(this.selectedTimeslot.skype_url);
					this.notifyStudents(this.selectedTimeslot.skype_url, false);
				} else {
					window.open(this.SKYPE_NEW_MEETING_URL);
					this.modalService.open(classroomUrlPopup, { ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true });
				}
				break;
			}
			default: break;
		}
 
	}

	notifyStudents(classroomUrl: string, isNewClass: boolean) {

		this.showLoader = true;
		classroomUrl = classroomUrl.replace('Meeting URL: ', "");

		if (this.commonService.isUrlValid(classroomUrl)) {
			
			let classType = "";
			if (this.classMode == this.CLASS_MODE.MEET) {
				classType = "meetUrl";
				this.selectedTimeslot.meet_url = classroomUrl;
			} else if (this.classMode == this.CLASS_MODE.ZOOM) {
				classType = "zoomUrl";
			} else if (this.classMode == this.CLASS_MODE.SKYPE) {
				classType = "skypeUrl";
				this.selectedTimeslot.skype_url = classroomUrl;
			} else {
				return;
			}

			this.dashboardService.notifyStudentsOfClassroom(this.user.user_id, this.selectedTimeslot, classType, classroomUrl).subscribe(
				(classroom) => {
					if (classroom.success) {
						this.toasterService.success("Notification sent to students!");
						if (isNewClass) {
							this.onCloseModalClassroom();
						} else {
							this.modalService.open(this.notificationPopup, { ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true });
						}
						this.showLoader = false;
					} else {
						this.toasterService.error(classroom.msg);
						this.showLoader = false;
					}
				}, (err) => {
					this.toasterService.error('Something went wrong. Please check your internet and try again!');
					this.showLoader = false;
				}
			);
		} else {
			alert("The url is invalid.");
		}
	}

	onCloseModalClassroom() {
		this.showLoader = false;
		this.modalService.dismissAll();
		this.meetUrl = "";
	}

}
