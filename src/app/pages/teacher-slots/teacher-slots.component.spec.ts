import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherSlotsComponent } from './teacher-slots.component';

describe('TeacherSlotsComponent', () => {
  let component: TeacherSlotsComponent;
  let fixture: ComponentFixture<TeacherSlotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherSlotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherSlotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
