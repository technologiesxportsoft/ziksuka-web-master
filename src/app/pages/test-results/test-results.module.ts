import { NgModule } from '@angular/core';

import { TestResultsRoutingModule } from './test-results-routing.module';
import { TestResultsComponent } from './test-results.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [TestResultsComponent],
  imports: [
    SharedModule,
    TestResultsRoutingModule
  ]
})
export class TestResultsModule { }
