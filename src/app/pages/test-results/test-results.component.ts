import { Component, OnInit } from '@angular/core';
import { HistoryService } from 'src/app/shared/_services/history.service';
import { AuthenticationService } from 'src/app/shared/_services/auth.service';
import { TestResultListObj } from 'src/app/shared/_model/test-result.class';
import { TestResultsService } from 'src/app/shared/_services/test-result.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { UserRole } from 'src/app/shared/_enumerations/UserRole';
import { GradeObj } from 'src/app/shared/_model/grade';
import { StudentObj } from 'src/app/shared/_model/student';
import { StudentService } from 'src/app/shared/_services/student.service';
import { GradeService } from 'src/app/shared/_services/grade.service';
import { NotesNoticeObj } from 'src/app/shared/_model/diary';
import { DatePipe } from '@angular/common';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
import am4themes_material from "@amcharts/amcharts4/themes/material";
// am4core.useTheme(am4themes_material);
am4core.useTheme(am4themes_animated);
am4core.options.autoSetClassName = true;

@Component({
	selector: 'app-test-results',
	templateUrl: './test-results.component.html',
	styleUrls: ['./test-results.component.css']
})
export class TestResultsComponent implements OnInit {

	public UserRole = UserRole;
	currentUserData;
	searchStudentTerm: '';
	teacherRole = false;
	panelOpenState = false;
	testList = ['All', 'Recall', 'Practice'];
	selectedTest = 'All';
	subjectList = [];
	selectedSubject = 'Select Subject';
	newdate = new Date();
	filterDate = new Date();
	showLoader = false;
	testResultList: TestResultListObj[] = [];
	gradeList: GradeObj[] = [];
	studentList: StudentObj[] = [];
	diaryList: NotesNoticeObj[] = [];
	noticesList: NotesNoticeObj[] = [];
	testType = 'ALL';
	searchGradeTerm = "";
	selectedrecipientId;
	selectedrecepientName;
	recipientSchoolGradeSectionName = [];
	recipientSchoolGradeSectionId = [];

	selectedGradeObject: any;

	showInsights: boolean = false;
	showStudentInsights: boolean = false;

	lastXDaysChart: any;
	distributionChart: any;
	studentScoreChart: any;

	constructor(private historyService: HistoryService,
		private testResultService: TestResultsService,
		private authenticationService: AuthenticationService,
		private toasterService: ToastrService,
		private router: Router,
		private studentService: StudentService,
		private gradeService: GradeService,
		private datePipe: DatePipe
	) { }

	ngOnInit() {
		this.currentUserData = this.authenticationService.currentUserValue;
		if (this.currentUserData.role == this.UserRole.Teacher) {
			this.teacherRole = true;
			this.getGradeList();
		} else {
			this.getTestResults();
		}
		// this.getSubjectList();
	}

	ngOnDestroy() {
		this.destroyCharts();
	}

	destroyCharts() {
		this.showInsights = false;
		if (this.lastXDaysChart) {
			this.lastXDaysChart.dispose();
		}
		if (this.distributionChart) {
			this.distributionChart.dispose();
		}
		if (this.studentScoreChart) {
			this.studentScoreChart.dispose();
		}
	}

	getGradeList() {
		this.gradeList = [];
		this.gradeService.getGradeListByUserIdV2(this.currentUserData.user_id).subscribe((res: any) => {
			this.gradeList = res.data;
			if (this.gradeList.length) {
				this.gradeList.forEach((grade, index) => {
					grade.imgTitle = null;
					let first_letter = grade.schoolGradeName as string;
					let last_letter = grade.sectionName as string;
					grade.imgTitle = first_letter + last_letter;
					grade.filterString = first_letter + last_letter;
					grade.isActive = false;
				})
			}
		});
	}
	getTestResults() {
		this.showLoader = true;
		let obj = {
			user_id: this.currentUserData.user_id,
			student_id: this.currentUserData.user_id
		}
		if (this.teacherRole) {
			obj.student_id = this.selectedrecipientId;
		}
		this.testResultService.getTestResult(obj.user_id, obj.student_id, this.testType).subscribe((res: any) => {
			if (res && res.success) {
				this.testResultList = res.data;
			} else {
				this.toasterService.error(res.msg);
			}
			this.showLoader = false;
		}, err => {
			this.showLoader = false;
			this.toasterService.error('Failed to get Recall Results')
		})
	}

	showTestResult(test: TestResultListObj) {
		this.showLoader = true;
		this.testResultService.getTest(this.currentUserData.user_id, test.studentId, test.testId).subscribe((res: any) => {
			if (res && res.success) {
				localStorage.setItem('testResultObj', JSON.stringify(res.data));
				this.router.navigate(['/practice-test/result']);
			} else {
				localStorage.setItem('testResultError', res.msg);
				this.router.navigate(['/practice-test/result']);
				this.toasterService.error(res.msg);
			}
			this.showLoader = false;
		}, err => {
			this.showLoader = false;
			this.toasterService.error('Failed to fetch result')
		})
	}

	onChangeTab(test) {
		this.selectedTest = test;
	}

	getSubjectList() {
		this.subjectList = [];
		let id = this.currentUserData.school_grade_section_id;
		this.historyService.GetSubjectsListV2(id, this.currentUserData.user_id).subscribe((res) => {
			let apiResult = res as any;
			if (apiResult && apiResult.data && apiResult.data.length) {
				apiResult.data.forEach(ele => {
					this.subjectList.push({ 'subjectName': ele.schoolSubjectName, 'id': Number(ele.schoolGradeSubjectId) });
				})
			}
		});
	}

	onDateChange(event) {
		this.filterDate = new Date(event.year, event.month - 1, event.day);
	}

	getStudentList() {
		this.showLoader = true;
		this.studentList = [];
		this.studentService.getStudentListByGradeSectionV2(this.recipientSchoolGradeSectionId).subscribe((res: any) => {
			this.studentList = res.data;
			if (this.studentList && this.studentList.length) {
				this.studentList.forEach((ele, index) => {
					let first_name = ele.firstName as string;
					let last_name = ele.lastName as string;
					ele.imgTitle = first_name.substring(0, 1).toUpperCase() + last_name.substring(0, 1).toUpperCase();
					ele.filterString = first_name + ' ' + last_name;
					ele.isActive = false;
				})
			}
			this.showLoader = false;
		});
	}


	studentSelection(studentObject) {
		let fetchResults = true;
		this.studentList.forEach((student: any) => {
			if (studentObject.userId == student.userId) {
				this.destroyCharts();
				if (student.isActive) {
					this.selectedrecipientId = null;
					fetchResults = false;
					student.isActive = false;
					this.showClassLevelInsights();
					return;
				}
				student.isActive = true;
				this.selectedrecipientId = student.userId;
				this.selectedrecepientName = student.firstName + ' ' + student.lastName;

				this.showInsights = true;
				this.showStudentInsights = true;
				this.testResultService.getPerformanceData(this.currentUserData.user_id, this.recipientSchoolGradeSectionId[0], this.selectedrecipientId).subscribe((res: any) => {
					if (res.success && !!res.data) {
						this.createlast7DaysChart(res.data, student.firstName);
						this.studentMedianChart(res.data);
					} else {
						this.showNoAnalytics();
					}
				}, (err) => {
					this.toasterService.error("Something went wrong! Could not fetch insights.");
					this.showNoAnalytics();
				});
			} else {
				student.isActive = false;
			}
		});
		if (fetchResults) {
			this.testResultList = [];
			this.getTestResults();
		}
	}

	classSelection(gradeObject) {
		this.selectedGradeObject = gradeObject;
		this.selectedrecipientId = '';
		this.selectedrecepientName = '';
		// this.recipientSchoolGradeSectionId = gradeObject.school_grade_section_id;
		this.gradeList.forEach((grade, index) => {
			// gradeObject.isActive = false;
			if (grade.schoolGradeSectionId == gradeObject.schoolGradeSectionId) {
				grade.isActive = !grade.isActive;
				let i = this.recipientSchoolGradeSectionId.findIndex(v => v == grade.schoolGradeSectionId);
				if (grade.isActive) {
					if (i == -1) {
						this.recipientSchoolGradeSectionName.push(gradeObject.schoolGradeName + gradeObject.sectionName);
						this.recipientSchoolGradeSectionId.push(gradeObject.schoolGradeSectionId)
					}
				} else {
					if (i > -1) {
						this.recipientSchoolGradeSectionName.splice(i, 1);
						this.recipientSchoolGradeSectionId.splice(i, 1);
					}
				}
			}
		});
		// this.getSubjectList();
		if (this.recipientSchoolGradeSectionId != null && this.recipientSchoolGradeSectionId.length == 1) {
			this.getStudentList();
			this.showClassLevelInsights();
		} else if (this.recipientSchoolGradeSectionId.length > 1 || !this.recipientSchoolGradeSectionId.length) {
			this.studentList = [];
			this.testResultList = [];
			this.destroyCharts();
		}
	}

	showClassLevelInsights() {
		this.testResultList = [];

		this.showInsights = true;
		this.showStudentInsights = false;
		this.testResultService.getPerformanceData(this.currentUserData.user_id, this.recipientSchoolGradeSectionId[0], null).subscribe((res: any) => {
			if (res.success && !!res.data) {
				this.createlast7DaysChart(res.data);
				this.createDistributionChart(res.data);
			} else {
				this.showNoAnalytics();
			}
		}, (err) => {
			this.toasterService.error("Something went wrong! Could not fetch insights.");
			this.showNoAnalytics();
		});
	}

	getFormattedTime(ts: number) {
		return this.datePipe.transform(ts, "MMM d, y h:mm a");
	}

	createlast7DaysChart(data: any, student = null) {
		if (this.lastXDaysChart) {
			this.lastXDaysChart.dispose();
		}

		if (!!!data.previousDaysDistribution) {
			return;
		}

		// Create chart instance
		let chart = am4core.create("last_x_days_chart_class", am4charts.XYChart);
		chart.hiddenState.properties.opacity = 0;
		
		this.lastXDaysChart = chart;
		let title = chart.titles.create();
		title.text = "Last 7 days";
		title.fontSize = 15;
		title.marginBottom = 20;
		let dayNum = 7;
		chart.data = []

		data.previousDaysDistribution.forEach(dayData => {
			if (dayData.testStats.length > 0) {
				dayData.testStats.forEach(testData => {
					let yValue = (!!testData.median || testData.median == 0) ? testData.median : "-5";
					chart.data.push({ "date": dayData.day + "\n" + dayData.date, "x": dayNum, "y": yValue, "tooltip": this.getStatsTooltip(testData, student) });
				});
			}
			dayNum--;
		});

		// Create axes
		let xAxis = chart.xAxes.push(new am4charts.ValueAxis());
		// xAxis.dataFields.category = "date";
		xAxis.title.text = "Date";
		xAxis.renderer.grid.template.strokeOpacity = 0;
		xAxis.disabled = false;
		xAxis.min = 0;
		xAxis.max = 8;
		xAxis.strictMinMax = true;
		xAxis.renderer.labels.template.disabled = true;

		// Create value axis
		let yAxis = chart.yAxes.push(new am4charts.ValueAxis() as any);
		yAxis.title.text = "Median Score (in %)";
		yAxis.min = -10;
		yAxis.max = 110;
		yAxis.strictMinMax = true;
		yAxis.renderer.minGridDistance = 30;


		let series1 = chart.series.push(new am4charts.LineSeries());
		series1.dataFields.categoryX = "date";
		series1.dataFields.valueX = "x";
		series1.dataFields.valueY = "y";
		series1.dataFields.value = "y";
		series1.strokeWidth = 0;

		//add bullets
		let circleBullet = series1.bullets.push(new am4charts.CircleBullet());
		circleBullet.circle.fill = am4core.color("#fff");
		circleBullet.circle.strokeWidth = 2;
		circleBullet.circle.radius = 5;
		circleBullet.circle.propertyFields.fill = "color";
		var hoverState = circleBullet.circle.states.create("hover");
		hoverState.properties.fillOpacity = 1;
		hoverState.properties.strokeOpacity = 1;

		circleBullet.circle.fill = am4core.color("#2c3e96");
		circleBullet.circle.stroke = am4core.color("#fff");
		circleBullet.circle.tooltipText = "{tooltip}";

		// series1.heatRules.push({ target: circleBullet.circle, min: 5, max: 10, property: "radius" });

		// circleBullet.circle.adapter.add("tooltipY", function (tooltipY, target) {
		// 	return 10;
		// })

		
		// //scrollbars
		// chart.scrollbarX = new am4core.Scrollbar();
		// chart.scrollbarY = new am4core.Scrollbar();
        /* Create a cursor */
		// chart.cursor = new am4charts.XYCursor();
		// chart.cursor.behavior = "zoomXY";
		// chart.responsive.enabled = true;
		// chart.responsive.useDefault = true;
		// chart.responsive.rules.push({
		// 	relevant: function(target) {
		// 	  return false;
		// 	},
		// 	state: function(target, stateId) {
		// 	  return null;
		// 	}
		//   });
	}

	createDistributionChart(data: any) {
		if (this.distributionChart) {
			this.distributionChart.dispose();
		}

		if (!!!data.lastTestDistribution) {
			return;
		}

		// Create chart instance
		var chart = am4core.create("last_test_distribution", am4charts.PieChart);
		this.distributionChart = chart;

		// Create chart title
		let title = chart.titles.create();
		title.text = "Recall (Latest)";
		title.fontSize = 15;
		title.marginTop = 10;

		// Add data
		chart.data = [
			{ "desc": "Absent", "value": data.lastTestDistribution.absentStudents }
		];

		data.lastTestDistribution.distribution.forEach(element => {
			chart.data.push({ "desc": element.description, "value": element.studentCount });
		});

		// Add label
		chart.innerRadius = 50;
		var label = chart.seriesContainer.createChild(am4core.Label);
		let stats = data.lastTestDistribution.stats;
		label.text = (!!stats.median || stats.median == 0) ? stats.median + "%" : "-";
		label.tooltipText = this.getStatsTooltip(stats);
		label.horizontalCenter = "middle";
		label.verticalCenter = "middle";
		label.fontSize = 20;

		// Add and configure Series
		var pieSeries = chart.series.push(new am4charts.PieSeries());
		pieSeries.dataFields.value = "value";
		pieSeries.dataFields.category = "desc";
		pieSeries.slices.template.stroke = am4core.color("#fff");
		pieSeries.slices.template.strokeWidth = 2;
		pieSeries.slices.template.strokeOpacity = 1;
		pieSeries.labels.template.fontSize = 10;
		var colorSet = new am4core.ColorSet();
		colorSet.list = ["#D9D9D9", "#F44336", "#E91E5A", "#9C27B0", "#673AB7", "#67B7DC", "#8067DC", "#6794DC"].map(function(color) {
			return am4core.color(color);
		});
		pieSeries.colors = colorSet;

	}

	studentMedianChart(data: any) {
		if (this.studentScoreChart) {
			this.studentScoreChart.dispose();
		}
		if (!!!data.lastTestMedian && data.lastTestMedian != 0) {
			return;
		}

		// Create chart instance
		var chart = am4core.create("student_test_distribution", am4charts.RadarChart);
		this.studentScoreChart = chart;
		// Create chart title
		let title = chart.titles.create();
		title.text = "Recall (Latest)";
		title.fontSize = 15;
		title.marginTop = 10;
		
		// Make chart full circle
		chart.startAngle = -90;
		chart.endAngle = 270;
		chart.innerRadius = am4core.percent(50);
		
		chart.data = [{
			"category": "Score",
			"value": data.lastTestMedian,
			"full": 100
		}]

		var label = chart.seriesContainer.createChild(am4core.Label);
		label.text = data.lastTestMedian + "%";
		label.horizontalCenter = "middle";
		label.verticalCenter = "middle";
		label.fontSize = 15;

		// Set number format
		chart.numberFormatter.numberFormat = "#.#'%'";

		// Create axes
		var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis() as any);
		categoryAxis.dataFields.category = "category";
		categoryAxis.renderer.labels.template.disabled = true;
		categoryAxis.renderer.grid.template.location = 0;
		categoryAxis.renderer.grid.template.strokeOpacity = 0;
		categoryAxis.renderer.minGridDistance = 10;
	
		var valueAxis = chart.xAxes.push(new am4charts.ValueAxis() as any);
		valueAxis.renderer.labels.template.disabled = true;
		valueAxis.renderer.grid.template.strokeOpacity = 0;
		valueAxis.min = 0;
		valueAxis.max = 100;
		valueAxis.strictMinMax = true;
	
		// Create series
		var series1 = chart.series.push(new am4charts.RadarColumnSeries());
		series1.dataFields.valueX = "full";
		series1.dataFields.categoryY = "category";
		series1.clustered = false;
		series1.columns.template.fill = new am4core.InterfaceColorSet().getFor(
		  "alternativeBackground"
		);
		series1.columns.template.fillOpacity = 0.08;
		// series1.columns.template.cornerRadiusTopLeft = 20;
		series1.columns.template.strokeWidth = 0;
		series1.columns.template.radarColumn.cornerRadius = 20;

		var series2 = chart.series.push(new am4charts.RadarColumnSeries());
		series2.dataFields.valueX = "value";
		series2.dataFields.categoryY = "category";
		series2.clustered = false;
		series2.columns.template.strokeWidth = 0;
		let color;
		if (data.lastTestMedian < 33) {
			color = "red";
		} else if (data.lastTestMedian < 66) {
			color = "#ff751a";
		} else {
			color = "green";
		}

		series2.columns.template.fill = color;
		series2.columns.template.stroke = color;
		series2.columns.template.radarColumn.cornerRadius = 20;


	}

	getStatsTooltip(stats: any, student = null) {
		let tip = ""
		if (!!stats.median || stats.median == 0) {
			if (!!student) {
				tip += student + "'s Score: " + stats.median + "%\n";
			} else {
				tip += "Class Median: " + stats.median + "%\n";
			}
		} else {
			if (!!student) {
				tip += "Unattempted by " + student + ", no Insights\n";
			} else {
				tip += "Unattempted by students, no Insights\n";
			}
		}
		if (!!stats.createdAt) {
			tip += "Time: " + this.getFormattedTime(stats.createdAt) + "\n";
		}
		if (!!stats.subject) {
			tip += "Subject: " + stats.subject + "\n";
		}
		if (!!stats.topic) {
			tip += "Topic: " + stats.topic + "\n";
		}
		if (!!stats.subTopic) {
			tip += "Subtopic: " + stats.subTopic + "\n";
		}
		return tip;
	}

	showNoAnalytics() {
		this.destroyCharts();
	}


}
