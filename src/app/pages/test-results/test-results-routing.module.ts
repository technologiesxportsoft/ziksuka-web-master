import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TestResultsComponent } from './test-results.component';


const routes: Routes = [
  { path: '', component: TestResultsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestResultsRoutingModule { }
