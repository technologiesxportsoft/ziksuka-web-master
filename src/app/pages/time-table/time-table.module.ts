import { NgModule } from '@angular/core';
import { TimeTableRoutingModule } from './time-table-routing.module';
import { TimeTableComponent } from './time-table.component';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [
    TimeTableComponent
  ],
  imports: [
    SharedModule,
    TimeTableRoutingModule
  ],
  entryComponents: []
})
export class TimeTableModule { }
