import { Component, OnInit } from '@angular/core';
import { StudentService } from '../../shared/_services/student.service';
import { AuthenticationService } from '../../shared/_services/auth.service';
import { UserRole } from '../../shared/_enumerations/UserRole';

@Component({
  selector: 'app-time-table',
  templateUrl: './time-table.component.html',
  styleUrls: ['./time-table.component.css']
})
export class TimeTableComponent implements OnInit {
  public UserRole = UserRole;
  currentUserData: any;
  daysName:any=[];
  timingsList:any=[];
  periodList:any=[]
  showLoader = true;
  constructor(private studentService: StudentService, 
              private authenticationService: AuthenticationService) {
                this.currentUserData = this.authenticationService.currentUserValue;
                this.getTimeTable();
  }

  ngOnInit() {
  
  }
  getTimeTable(){
    this.studentService
    .getWeeklyTimeTable(this.currentUserData.user_id)
    .subscribe((res) => {
      let timetable = res['data'].timetable;
      this.daysName = res['data'].day_list;
      this.timingsList = Object.keys(timetable);
      this.periodList = Object.values(timetable);
      this.showLoader = false;
    });
  }
    isToday(dayName){
      let daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
      let today = new Date();
      let todayDayName = daysOfWeek[today.getDay()];
      if(dayName == todayDayName.toUpperCase()){
        return true;
      }
      else return false;
    }
}
