import { NgModule } from '@angular/core';

import { PagesRoutingModule } from './pages-routing.module';
import { SharedModule } from '../shared/shared.module';
import { DashboardModule } from './dashboard/dashboard.module';

@NgModule({
  imports: [
    SharedModule,
    PagesRoutingModule,
    DashboardModule
  ]
})
export class PagesModule { }
