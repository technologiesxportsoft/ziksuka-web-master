import { Component, OnInit } from '@angular/core';
import { UserInfo } from '../shared/_model/user-info';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../shared/_services/auth.service';
import { ContactusService } from '../shared/_services/contactus.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  //helpFeedbackObj: FeedbackObj = new FeedbackObj();
  pagecount: any = 0;
  currentUserData: UserInfo;
  contactUsModel: any = {};
  showLoader = false;
  constructor(private router: Router,
    private toaster: ToastrService,
    private contactusService: ContactusService,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {

  }

  student = [
    { id: 1, name: '0-100' },
    { id: 2, name: '100-500' },
    { id: 3, name: '500-1000' },
    { id: 4, name: '>1000' }
  ];
  onLogin(){
    this.router.navigate(['login'])
  }
  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  onFormSubmit() {
    this.showLoader = true;
    let item = this.contactUsModel;
    let contactrequest = {
      firstName: item.first_name,
      lastName: item.last_name,
      emailId: item.email_id,
      phoneNumber: parseInt(item.phone_number),
      numStudents: item.no_of_Student,
      message: item.message,
      userId:0
    };
    this.contactusService.submitContactUs(contactrequest).subscribe((res: any) => {
      if (res && res.success) {
        this.toaster.success(res.msg);
        this.router.navigate(['login'])
      } else {
        this.toaster.error(res.msg);
      }
      this.showLoader = false;
    },
      err => {
        console.log(err);
        this.showLoader = false;
      })
  }

}
