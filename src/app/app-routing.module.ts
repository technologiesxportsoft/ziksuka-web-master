import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/_guards/auth-guard';
import { LoginGuard } from './shared/_guards/login-guard';
import { AdminGuard } from './shared/_guards/admin-guard';
import { TeacherStudentGuard } from './shared/_guards/teacher-student-guard';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { PrivacyComponent } from './privacy/privacy.component';

const routes: Routes = [
  { path: 'login', loadChildren: './auth/auth.module#AuthModule', canActivate: [LoginGuard] },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'privacy', component: PrivacyComponent },
  { path: 'admin', loadChildren: './admin-pages/admin-pages.module#AdminPagesModule', canActivate: [AuthGuard, AdminGuard] },
  { path: '', loadChildren: './pages/pages.module#PagesModule', canActivate: [AuthGuard] },
  { path: '', redirectTo: '', pathMatch: 'full' },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
