import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LoginLayoutComponent } from '../shared/_components/layouts/login-layout/login-layout.component';


const routes: Routes = [
  { path: '', component: LoginLayoutComponent,
  children: [
    { path: '', component: LoginComponent },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
