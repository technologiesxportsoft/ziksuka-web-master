import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { LoginModel } from 'src/app/shared/_model/login-model';
import { AuthenticationService } from 'src/app/shared/_services/auth.service';
import { UserRole } from 'src/app/shared/_enumerations/UserRole';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public model: LoginModel;
  public isErrorLogin = false;
  public errorLoginMessage;
  closeResult: any;
  public forgotPasswordFormModel = {
    emailId: ''
  };
  showLoader = false;

  @ViewChild('loginfrm', { static: true }) loginfrm;

  constructor(private router: Router, private bmodalService: NgbModal,
    private AuthenticationService: AuthenticationService, private toaster: ToastrService) { }

  ngOnInit() {
    this.prepareLoginModel();
  }

  prepareLoginModel() {
    this.model = new LoginModel(null, null);
  }
  login() {
    if (this.loginfrm.valid) {
      this.showLoader = true;
      this.isErrorLogin = false;
      this.AuthenticationService.login(this.model).subscribe(res => {
        if (res['success']) {
          if (![UserRole.ContentAdmin, UserRole.ContentDeveloper].includes(res.data.user_details.role)) {
            this.router.navigate(['/dashboard']);
          } else {
            this.router.navigate(['/admin', 'questions']);
          }
        } else {
          this.isErrorLogin = true;
          this.errorLoginMessage = res["msg"];
          this.showLoader = false;
        }
      }, (err) => {
        this.toaster.error("Something went wrong, please try again.");
        this.showLoader = false;
      });
    } else {
    }
  }

  sendEmail() {
    if (!confirm('Are you sure you want to change your password?')) {
      return;
    } else {
      let obj = {
        "emailId": this.forgotPasswordFormModel.emailId
      }
      this.showLoader = true;
      this.AuthenticationService.forgotPassword(obj).subscribe(
        (res: any) => {
          if (res && res.success) {
            this.forgotPasswordFormModel.emailId = '';
            this.bmodalService.dismissAll();
            this.toaster.success(res.msg);
          } else {
            this.toaster.error(res.msg);
          }
          this.showLoader = false;
        }, (err) => {
          this.toaster.error("Something went wrong, please try again.");
          this.showLoader = false;
        }
      )
    }
  }

  open(content) {
    this.bmodalService.open(content, { ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  // Let it be here for future Concern START
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  // Let it be here for future Concern END
}
