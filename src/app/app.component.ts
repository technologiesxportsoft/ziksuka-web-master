import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from './shared/_services/auth.service';
import { PracticeTestService } from './shared/_services/practice-test.service';
import { Router } from '@angular/router';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Basta';

  iconList = [
    { tag: 'custom_brush', url: '../assets/images/Icons/brush.svg' },
    { tag: 'draw_rectangle', url: '../assets/images/Icons/rectangle.svg' },
    { tag: 'draw_ellipse', url: '../assets/images/Icons/smallBrush.svg' },
    { tag: 'custom_clean', url: '../assets/images/Icons/clean.svg' },
    { tag: 'custom_fullscreen', url: '../assets/images/Icons/full-screen.svg' },
    { tag: 'custom_fullscreen_exit', url: '../assets/images/Icons/exit-full-screen.svg' },
    { tag: 'custom_save', url: '../assets/images/Icons/floppy-disk.svg' },
    { tag: 'draw_line', url: '../assets/images/Icons/line.svg' },
  ];
  recentLoginTime: Date;
  localStorageTime: string;
  newLatestLoginTime: any;

  constructor(
    private authenticationService: AuthenticationService,
    private practiceTestService: PracticeTestService,
    private router: Router,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public datepipe: DatePipe) {
      this.iconList.forEach(e => {
        this.matIconRegistry.addSvgIcon(
          e.tag,
          this.domSanitizer.bypassSecurityTrustResourceUrl(e.url)
        );
      });
  }
  ngOnInit() {

    if (localStorage.getItem('userInfo')) {
      const user = JSON.parse(localStorage.getItem('userInfo'));
      this.authenticationService.currentUserSubject.next(user);
    }
    if (localStorage.getItem('testLocalStorageObj')) {
      const test = JSON.parse(localStorage.getItem('testLocalStorageObj'));
      this.practiceTestService.practiceTestData.next(test);
    }
    if (localStorage.getItem('loginDate')) {
      this.recentLoginTime = new Date();  
      this.newLatestLoginTime = this.datepipe.transform(this.recentLoginTime, 'yyyy-MM-dd');  
      this.localStorageTime = localStorage.getItem('loginDate');
      if ( this.localStorageTime !== this.newLatestLoginTime){
        this.authenticationService.logout();
        this.router.navigate(['/login']);
      }else {
        console.log('date matched');
      }
      
    } 
  
    // this.authenticationService.currentUserSubject.subscribe(val => {
    //   if (val && val.user_id && (val.user_type === 'CONTENT_ADMIN' || val.user_type === 'CONTENT_DEVELOPER')) {
    //     this.router.navigate(['/admin']);
    //   }
    // });
  }
}
