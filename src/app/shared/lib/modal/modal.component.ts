import {
  Component, ElementRef, ViewChild, Input, Output, OnInit, AfterViewChecked, HostListener, HostBinding, EventEmitter, ViewEncapsulation
} from '@angular/core';
import { ResizableEvent } from '../resizable/types';
import { maxZIndex, findAncestor } from '../common/utils';
declare var jQuery;
@Component({
  selector: 'app-modal',
  templateUrl: 'modal.component.html',
  styleUrls: ['modal.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ModalComponent implements OnInit, AfterViewChecked {

  @Input() modalTitle: string;
  @Input() zIndex: number;
  @Input() scrollTop: boolean = true;
  @Input() maximizable: boolean;
  @Input() backdrop: boolean = true;
  @Input() modalType: string;

  @Output() closeModal: EventEmitter<boolean> = new EventEmitter();

  @ViewChild('modalRoot', { static: false }) modalRoot: ElementRef;
  @ViewChild('modalBody', { static: false }) modalBody: ElementRef;
  @ViewChild('modalHeader', { static: false }) modalHeader: ElementRef;
  // @ViewChild('modalFooter', { static: false }) modalFooter: ElementRef;

  @HostBinding('class.app-modal') cssClass = true;

  visible: boolean;
  contentzIndex: number;
  executePostDisplayActions: boolean;
  maximized: boolean;
  preMaximizeRootWidth: number;
  preMaximizeRootHeight: number;
  preMaximizeBodyHeight: number;
  preMaximizePageX: number;
  preMaximizePageY: number;
  dragEventTarget: MouseEvent | TouchEvent;

  constructor(private element: ElementRef) { }

  ngOnInit() {
    jQuery(document).ready(function () {
      jQuery('.lid').hide();
    });
    jQuery(document).ready(function () {
      jQuery('.box').hover(function () {
        console.log('document', document)
        jQuery(this).find('.lid').show(500);
      },
        function () {
          jQuery('.lid').hide(500);
        });
    });
    if (!this.zIndex) {
      this.zIndex = this.getMaxModalIndex();
      this.zIndex = (this.zIndex || 1000) + 1;
    }
    this.contentzIndex = this.zIndex + 1;
  }

  ngAfterViewChecked() {
    if (this.executePostDisplayActions) {
      this.center();
      this.executePostDisplayActions = false;
    }
  }

  @HostListener('keydown.esc', ['$event'])
  onKeyDown(event): void {
    if (this.maximized) {
      this.revertMaximize();
    }
    this.maximized = false;
    event.preventDefault();
    event.stopPropagation();
    // this.hide();
  }
  @HostListener('document:fullscreenchange', [])
  @HostListener('document:webkitfullscreenchange', [])
  @HostListener('document:mozfullscreenchange', [])
  @HostListener('document:MSFullscreenChange', [])
  fullscreenmode() {
    let elem: any = document.getElementById('modal');
    if (this.maximized) {
      this.revertMaximize();
      if (elem.children[0].children[0].classList.contains("video-h-w")) {
        elem.children[0].children[0].classList.remove("video-h-w");
        elem.children[0].children[0].classList.add("video-fullscreen");
      }
    } else {
      if (elem.children[0].children[0].classList.contains("video-fullscreen")) {
        elem.children[0].children[0].classList.remove("video-fullscreen");
        elem.children[0].children[0].classList.add("video-h-w");
      }
    }
    event.preventDefault();
    event.stopPropagation();
  }
  @HostListener('window:resize')
  onWindowResize(): void {
    this.executePostDisplayActions = true;
    this.center();
  }

  show(): void {
    this.executePostDisplayActions = true;
    this.visible = true;
    setTimeout(() => {
      this.modalRoot.nativeElement.focus();
      if (this.scrollTop) {
        this.modalBody.nativeElement.scrollTop = 0;
      }
    }, 1);
  }

  hide(): void {
    this.visible = false;
    this.maximized = false;
    this.closeModal.emit(true);
    this.focusLastModal();
  }
  fullScreen() {
    this.preMaximizePageX = parseFloat(this.modalRoot.nativeElement.style.top);
    this.preMaximizePageY = parseFloat(this.modalRoot.nativeElement.style.left);
    this.preMaximizeRootWidth = this.modalRoot.nativeElement.offsetWidth;
    this.preMaximizeRootHeight = this.modalRoot.nativeElement.offsetHeight;
    this.preMaximizeBodyHeight = this.modalBody.nativeElement.offsetHeight;
    let elem: any = document.getElementById('modal');
    let methodToBeInvoked = elem.requestFullscreen ||
      elem.webkitRequestFullScreen || elem['mozRequestFullscreen']
      ||
      elem['msRequestFullscreen'];
    if (methodToBeInvoked) methodToBeInvoked.call(elem);
    this.maximized = true;
  }
  center() {
    let elementWidth = this.modalRoot.nativeElement.offsetWidth;
    let elementHeight = this.modalRoot.nativeElement.offsetHeight;

    if (elementWidth === 0 && elementHeight === 0) {
      this.modalRoot.nativeElement.style.visibility = 'hidden';
      this.modalRoot.nativeElement.style.display = 'block';
      elementWidth = this.modalRoot.nativeElement.offsetWidth;
      elementHeight = this.modalRoot.nativeElement.offsetHeight;
      this.modalRoot.nativeElement.style.display = 'none';
      this.modalRoot.nativeElement.style.visibility = 'visible';
    }

    const x = Math.max((window.innerWidth - elementWidth) / 2, 0);
    const y = Math.max((window.innerHeight - elementHeight) / 2, 0);
    this.modalRoot.nativeElement.style.left = x + 'px';
    this.modalRoot.nativeElement.style.top = y + 'px';
    this.modalRoot.nativeElement.style.height = '';
    this.modalRoot.nativeElement.style.minHeight = '';
    //this.modalRoot.nativeElement.style.maxHeight = '';
    this.modalRoot.nativeElement.style.width = '';
    this.modalRoot.nativeElement.style.minWidth = '';
    this.modalRoot.nativeElement.style.overflowY = 'none';
    this.modalRoot.nativeElement.style.overflowX = 'none';
    this.modalRoot.nativeElement.children[1].children[0].style.height = '';
    this.modalRoot.nativeElement.children[1].style.height = '';
    this.modalRoot.nativeElement.children[1].style.overflow = '';
    //this.modalRoot.nativeElement.children[1].children[0].style.minHeight = '';
    //this.modalRoot.nativeElement.children[1].children[0].style.minWidth = '';
    if (this.modalType == "AUDIO") {
      this.modalRoot.nativeElement.style.height = '100px';
    }
    else if (this.modalType == "VIDEO") {
      //this.modalRoot.nativeElement.style.width = '50%';
      this.modalRoot.nativeElement.style.minWidth = '250px';
      //this.modalRoot.nativeElement.style.height = '45%';
      this.modalRoot.nativeElement.style.minHeight = '360px';
      this.modalRoot.nativeElement.children[1].style.height = '312px';
      this.modalRoot.nativeElement.children[1].children[0].style.height = '100%';
      //this.modalRoot.nativeElement.children[1].children[0].style.minHeight = '450px';
      this.modalRoot.nativeElement.children[1].children[0].style.width = '100%';
      //this.modalRoot.nativeElement.children[1].children[0].style.minWidth = '50%';
      // if (elementHeight <= 200) {
      //   this.modalRoot.nativeElement.style.minHeight = '52%';
      // } else if (elementHeight >= 400) {
      //   this.modalRoot.nativeElement.style.minHeight = '52%';
      //   this.modalRoot.nativeElement.style.overflowX = 'hidden';
      //   this.modalRoot.nativeElement.style.overflowY = 'hidden';
      // }
    }
    else if (this.modalType == "TEXT") {
      this.modalRoot.nativeElement.children[1].style.height = '295px';
      //this.modalRoot.nativeElement.children[1].style.overflow = 'hidden scroll';
      this.modalRoot.nativeElement.style.minHeight = '150px';
      //this.modalRoot.nativeElement.style.top = '28px';
    }
  }

  initDrag(event: MouseEvent | TouchEvent) {
    if (!this.maximized) {
      this.dragEventTarget = event;
    }
  }

  onResize(event: ResizableEvent) {
    if (event.direction === 'vertical') {
      this.calcBodyHeight();
    }
  }

  calcBodyHeight() {
    const diffHeight = this.modalHeader.nativeElement.offsetHeight;
    // const diffHeight = this.modalHeader.nativeElement.offsetHeight + this.modalFooter.nativeElement.offsetHeight;
    const contentHeight = this.modalRoot.nativeElement.offsetHeight - diffHeight;
    this.modalBody.nativeElement.style.height = contentHeight + 'px';
    this.modalBody.nativeElement.style.maxHeight = 'none';
  }

  getMaxModalIndex() {
    return maxZIndex('.ui-modal');
  }

  focusLastModal() {
    const modal = findAncestor(this.element.nativeElement.parentElement, 'app-modal');
    if (modal && modal.children[1]) {
      modal.children[1].focus();
    }
  }

  onCloseIcon(event: Event) {
    this.maximized = false;
    if (jQuery('.e2e-iframe-trusted-src')[0] != undefined) {
      jQuery('.e2e-iframe-trusted-src')[0].src = '';
    }
    if (jQuery('.audio-width')[0] != undefined) {
      jQuery('.audio-width')[0].pause();
      jQuery('.audio-width')[0].currentTime = 0;
    }
    event.stopPropagation();
  }

  toggleMaximize(event) {
    this.maximized = false;
    // if (this.maximized) {
    //   this.revertMaximize();
    // } else {
    this.fullScreen();
    // this.maximize();
    // }
    event.preventDefault();
  }

  maximize() {
    this.preMaximizePageX = parseFloat(this.modalRoot.nativeElement.style.top);
    this.preMaximizePageY = parseFloat(this.modalRoot.nativeElement.style.left);
    this.preMaximizeRootWidth = this.modalRoot.nativeElement.offsetWidth;
    this.preMaximizeRootHeight = this.modalRoot.nativeElement.offsetHeight;
    this.preMaximizeBodyHeight = this.modalBody.nativeElement.offsetHeight;

    this.modalRoot.nativeElement.style.top = '0px';
    this.modalRoot.nativeElement.style.left = '0px';
    this.modalRoot.nativeElement.style.width = '100vw';
    this.modalRoot.nativeElement.style.height = '100vh';
    const diffHeight = this.modalHeader.nativeElement.offsetHeight;
    // const diffHeight = this.modalHeader.nativeElement.offsetHeight + this.modalFooter.nativeElement.offsetHeight;
    this.modalBody.nativeElement.style.height = 'calc(100vh - ' + diffHeight + 'px)';
    this.modalBody.nativeElement.style.maxHeight = 'none';

    this.maximized = true;
  }

  revertMaximize() {
    this.modalRoot.nativeElement.style.top = this.preMaximizePageX + 'px';
    this.modalRoot.nativeElement.style.left = this.preMaximizePageY + 'px';
    this.modalRoot.nativeElement.style.width = this.preMaximizeRootWidth + 'px';
    this.modalRoot.nativeElement.style.height = this.preMaximizeRootHeight + 'px';
    this.modalBody.nativeElement.style.height = this.preMaximizeBodyHeight + 'px';

    this.maximized = false;
  }

  moveOnTop() {
    if (!this.backdrop) {
      const zIndex = this.getMaxModalIndex();
      if (this.contentzIndex <= zIndex) {
        this.contentzIndex = zIndex + 1;
      }
    }
  }

  get dialogStyles() {
    return {
      display: this.visible ? 'block' : 'none',
      'z-index': this.contentzIndex,
    };
  }

  get overlayStyles() {
    return {
      display: (this.visible && this.backdrop) ? 'block' : 'none',
      'z-index': this.zIndex,
    };
  }

}
