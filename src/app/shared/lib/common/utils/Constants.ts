export const GLOBAL_CONSTANT = {
  MAX_BOOK_PER_PAGE: 20,
  ALL_SUBJECTS: "All Subjects",
  ALL_SUBJECTS_ID: null,
};

export const PAGE_NAME = {
  DASHBOARD: "DASHBOARD",
};
