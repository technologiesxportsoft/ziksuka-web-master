export class UserInfo {
  constructor(
    public id: number,
    public user_id: number,
    public role: number,
    public first_name: string,
    public last_name: string,
    public email: string,
    public country_code: string,
    public phone_number: number,
    public dob: Date,
    public gender: string,
    public photo: string,
    public user_type: string,
    public is_teacher: boolean,
    public is_birthday_today: boolean,
    public grade_section_id: number,
    public school_grade_section_id: number,
    public school_grade_id: number,
    public grade_id: number,
    public school_id: number,
    public school_name: string,
    public profile_pic: string,
    public grade_name: string,
    public school_grade_name: string,
    public section_name: string,
    public teacher: boolean
  ) {}
}
