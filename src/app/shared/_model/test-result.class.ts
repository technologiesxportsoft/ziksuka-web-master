export class TestResultListObj {
    studentId: any;
    testId: any;
    schoolGradeSectionId: any;
    schoolGradeSubjectId: any;
    topicId: any;
    subtopicId: any;
    duration: any;
    complexity: any;
    type: any;
    timeTaken: any;
    questionsCorrected: any;
    questionsAttempted: any;
    totalQuestions: any;
    marksSecured: any;
    totalMarks: any;
    percentage: any;
    createdAt: any;
    modifiedAt: any;
    schoolGradeName: any;
    sectionName: any;
    schoolSubjectName: any;
    topicName: any;
    subtopicName: any;
    submitted: any;
}
