export class PracticeTestObj {
    instructions: any;
    questions: QuestionObj[];
    testId: any;
    metaData: MetaObj;
}
export class QuestionObj {
    // class: any;
    // board: any;
    // subject: any;
    // language: any;
    // topic: any;
    // topic_name: any;
    // subtopic: any;
    // subtopic_name: any;

    // image: any;
    // video: any;
    // audio: any;
    // columns: any;

    type: any;
    score: any;
    options: any;
    questionId: any;
    questionHtml: any;
    question: any;
    optionsIndices: any;
}
export class MetaObj {
    numberOfQuestions: any;
    type: any;
    maximumMarks: any;
    waitingTime: any;
    schoolGradeSectionId: any;
    schoolGradeSubjectId: any;
    topicId: any;
    subtopicId: any;
    complexity: any;
    duration: any;
    schoolSubjectName: any;
    topicName: any;
    subtopicName: any;
}
export class AnswerObj {
    userId: any;
    testId: any;
    timeTaken: any;
    questions: QuestionAnsObj[];
    constructor() {
        this.userId = '';
        this.questions = [];
        this.testId = '';
        this.timeTaken = -1;
    }
}
export class QuestionAnsObj {
    questionId: any;
    timeTaken: any = -1;
    optionsShown: any;
    answer: any;
}
export class TimerObj {
    countDownMinutes: any;
    countDownSeconds: any;
    minutes: any;
    seconds: any;
    countDownDate: any;
    constructor() {
        this.countDownMinutes = -1;
        this.countDownSeconds = -1;
        this.minutes = -1;
        this.seconds = -1;
        this.countDownDate = -1;
    }
}
export class TestLocalStorageObj {
    practiceTestObj: PracticeTestObj;
    timerObj: TimerObj;
    answerObj: AnswerObj;
    currentQuestionNo: any;
    currentQuestionObj: QuestionObj;
    constructor() {
        this.currentQuestionNo = -1;
        this.timerObj = new TimerObj();
        this.currentQuestionObj = new QuestionObj();
        this.answerObj = new AnswerObj();
    }
}
export class TestResultObj {
    complexity: any;
    duration: any;
    isSubmitted: any;
    marksSecured: any;
    maximumMarks: any;
    numberOfQuestions: any;
    percentageObtained: any;
    questionsAttempted: any;
    questionsCorrected: any;
    schoolGradeSectionId: any;
    schoolGradeSubjectId: any;
    studentId: any;
    subtopicId: any;
    testCreatedAt: any;
    testId: any;
    testSubmittedAt: any;
    testType: any;
    timeTaken: any;
    topicId: any;
    questions: TestResultQuestionObj[];
}
export class TestResultQuestionObj {
    answer: any;
    correctAnswer: any;
    explanation: any;
    isCorrect: any;
    question: any;
    questionHtml: any;
    questionId: any;
    questionMarks: any;
    questionType: any;
    optionsIndices: any;
    optionsShown: any;
    correctAnswerString: any;
}