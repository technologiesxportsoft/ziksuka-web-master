export class User {
    aadhaarId: any;
    city: City;
    country: Country;
    countryCode: any;
    dob: any;
    emailId: any
    firstName: any;
    gender: any;
    lastName: any;
    line1: any;
    phoneNumber: any;
    photo: any;
    role: any;
    schoolGradeName: any;
    schoolGradeSectionId: any;
    schoolId: any;
    schoolName: any;
    sectionName: any;
    state: State;
    userId: any;
    userName: any;
    zipcode: any;
    line2: any;
    latitude: any;
    longitude: any;
}
export class State {
    stateId: any;
    stateName: any;
}
export class City {
    cityId: any;
    cityName: any;
}
export class Country {
    countryId: any;
    countryName: any;
}
