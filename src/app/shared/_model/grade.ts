export class GradeObj {
    gradeId: any;
    schoolGradeSectionId: any;
    schoolGradeName: any;
    sectionName: any;
    classTeacherId: any;
    isActive: any;
    imgTitle: any;
    filterString: any;
}
