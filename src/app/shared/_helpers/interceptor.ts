import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
} from '@angular/common/http';
import { Router } from "@angular/router";
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AuthenticationService } from '../_services/auth.service';
import * as shajs from 'sha.js';
import { CookieService } from 'ngx-cookie-service';
import { CommonService } from '../_services/common.service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {

  currentUserData: any;
  userId: any;
  weekDay = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'];
  excludedAPIListForLoginChk = ['https://ziksuka.com/api/login',
                                'https://ziksuka.com/api/forgotpassword',
                                'https://ziksuka.com/api/contact'];

  constructor(public authenticationService: AuthenticationService, public router: Router,
              private cookieService: CookieService, private commonService: CommonService) {
  }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //document.getElementById('loading').style.display = 'block';
    //this.Loader.ShowLoader();
    const token: string = localStorage.getItem('token');

    const hashLocalStorageValue = this.commonService.generateUserValidationToken();
    const hashCookieValue = this.cookieService.get('auth');

    if(!this.excludedAPIListForLoginChk.includes(request.url)) {
      if (hashLocalStorageValue !== hashCookieValue){
        this.authenticationService.logout();
        window.location.href = '/#/login';
      }
    } 

    if (token) {
      request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
    }
    request = request.clone({ headers: request.headers.set('Basta-Authentication-Header', this.generateToken()) });
    if (!request.headers.has('Content-Type') && !request.headers.has('Content-DontOverRide')) {
      request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
    }

    request = request.clone({ headers: request.headers.set('Accept', 'application/json') });

    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        //document.getElementById('loading').style.display = 'none';
        //this.Loader.HideLoader();
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        //document.getElementById('loading').style.display = 'none';
        //this.Loader.HideLoader();
        if (error.status == 401) {
          this.authenticationService.logout()
          this.router.navigateByUrl('/login', { replaceUrl: true });
        }
        return throwError(error);
      }));
  }

  generateToken() {
    this.currentUserData = this.authenticationService.currentUserValue;
    this.userId = this.currentUserData ? this.currentUserData.user_id : 0;
    var SEED = "bastaAuth7r3762";
    let time = String((new Date()).getTime());
    let str = time.substring(1, 10) + SEED + this.weekDay[(new Date).getDay()] + this.userId;
    var l = shajs('sha256').update(str).digest('hex')
    return 'BASTA-' + this.userId + '-' + time + '-' + l.substring(0, 20);
  }

}
