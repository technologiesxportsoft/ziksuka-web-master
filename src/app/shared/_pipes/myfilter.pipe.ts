import { Pipe, PipeTransform } from '@angular/core';
import { KeyValue } from '@angular/common';

@Pipe({
    name: 'myfilter',
    pure: false
})
export class MyFilterPipe implements PipeTransform {
    transform(items: any[], filter: any): any {
        if (!items || !filter) {
            return items;
        }
        return items.filter(item => item.filterString.toLowerCase().indexOf(filter.toLowerCase()) !== -1);
    }
}

@Pipe({
    name: 'keysort',
    pure: false
})
export class KeySortPipe implements PipeTransform {
    transform(array: Array<any>): Array<any> {
        array.sort((a: any, b: any) => {
          if (a.handRaised) {
            return -1;
          } else {
            return 1;
          }
        });
        return array;
    }
}
