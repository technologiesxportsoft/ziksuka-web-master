export enum UserRole {
    Admin = 1,
    Executive = 2,
    Principal = 3,
    Teacher = 4,
    Student = 5,
    Parent = 6,
    ContentAdmin = 7,
    ContentDeveloper = 8
  }
