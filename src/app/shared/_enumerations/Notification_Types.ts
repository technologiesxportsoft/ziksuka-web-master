export enum NotificationTypes {
    LINK = "LINK",
    RECALL_TEST = "RECALL_TEST",
    PLAIN = "PLAIN",
    CLASSROOM = "CLASSROOM",
  }
