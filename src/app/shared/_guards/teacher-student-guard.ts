import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../_services/auth.service';
import { UserRole } from '../_enumerations/UserRole';

@Injectable()
export class TeacherStudentGuard implements CanActivate {

    constructor(private _router: Router, private auth: AuthenticationService) {
    }

    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (localStorage.getItem('token') && localStorage.getItem('userInfo') && localStorage.getItem('isLogin')) {
            if ((this.auth.currentUserValue.role != UserRole.ContentAdmin && this.auth.currentUserValue.role != UserRole.ContentDeveloper)) {
                return true;
            } else {
                this._router.navigate(['/login']);
                return false;
            }
        } else {
            this._router.navigate(['/login']);
            return false;
        }
    }

}