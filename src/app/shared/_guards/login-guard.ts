import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { UserRole } from '../_enumerations/UserRole';
import { CookieService } from 'ngx-cookie-service';

@Injectable({ providedIn: 'root' })
export class LoginGuard implements CanActivate {

  constructor(private _router: Router, private cookieService: CookieService) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

    if (localStorage.getItem('token') && localStorage.getItem('userInfo') && localStorage.getItem('isLogin') && this.cookieService.get('auth')) {
      const u = JSON.parse(localStorage.getItem('userInfo'));
      console.log('Inside guard', u);
      if (![UserRole.ContentAdmin, UserRole.ContentDeveloper].includes(u.role)) {
        this._router.navigate(['/dashboard']);
      } else {
        this._router.navigate(['/admin', 'questions']);
      }
      return false;
    }
    localStorage.clear();
    // navigate to login pageuserInfo

    return true;
  }

}
