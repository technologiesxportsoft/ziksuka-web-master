import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private _router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    
    if (localStorage.getItem('token') && localStorage.getItem('userInfo') && localStorage.getItem('isLogin')) {
        return true;
    }
    // navigate to login page
    this._router.navigate(['/login']);
    return false;
  }

}