import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
@Injectable({ providedIn: 'root' })
export class Constants {
    headers: HttpHeaders = new HttpHeaders();
    constructor() {
        this.headers = this.headers.append('Accept', 'application/x-www-form-urlencoded');
        this.headers = this.headers.append('Content-Type', 'application/json');
        this.headers = this.headers.append('Access-Control-Allow-Origin', '*');
    }
    public apiUrl = 'http://18.222.131.191:8081/api';
    public apiUrlV2 = 'https://ziksuka.com/api';
    public static LOGIN = '/login';
    public static REGISTER = '/signup';
    public static STUDENT = 'api/student';
    public static ATTENDANCE = 'api/attendance';
    public static CHANGE_PASSWORD = '/changepassword';
    public static DIARY = '/diary';
    public static TIME_TABLE = '/timetable';
    public static RECOMMENDATIONS = '/recommendations';
    public static FEEDS = '/feed';
    public static BOOKS = '/books';
    public static LIBRARY = '/library';
    public static SUBJECT = '/subject';
}
