import { Component, OnInit, Input } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { RecommendationService } from "../../_services/recommendation.service";
import { UserInfo } from "../../_model/user-info";
import { AuthenticationService } from "../../_services/auth.service";
import { Router } from "@angular/router";
import { PracticeTestService } from "../../_services/practice-test.service";
import { TestLocalStorageObj } from "../../_model/practice-test";
import { UserRole } from '../../_enumerations/UserRole';
import { ToastrService } from 'ngx-toastr';
declare var $;

@Component({
  selector: "app-test-configuration",
  templateUrl: "./test-configuration.component.html",
  styleUrls: ["./test-configuration.component.css"],
})
export class TestConfigurationComponent implements OnInit {
  @Input() school_grade_section_id: any;
  @Input() showInformationForRecall: any;
  @Input() school_subject_name: any;
  @Input() school_subject_id: any;
  @Input() school_grade_subject_id: any;
  schoolGradeSectionId: any;
  user: UserInfo;
  isTeacher = false;
  subjectsList = [];
  showLoader = false;
  SubjectId: any = -1;
  TopicId: any = -1;
  SubTopicId: any = -1;
  topicsList = [];
  subTopicsList = [];
  selectedSubTopic = "All SubTopics";
  selectedTopic = "All Topics";
  selectedSubject: any = "";
  testDuration: any = 5;
  numberOfQuestions: any = 1;
  showInstructions: boolean = false;
  practiceTestData: any;
  instruction = "";
  testLocalStorageObj = new TestLocalStorageObj();
  startTestClicked = false;
  showInstructionLoader = false;
  selectedSchoolSubjectId = -1;
  selectedSchoolGradeSubjectId = -1;
  public UserRole = UserRole;

  constructor(
    private modalService: NgbModal,
    private recommendationService: RecommendationService,
    private authenticationService: AuthenticationService,
    private router: Router,
    private practiceTestService: PracticeTestService,
    private toaster: ToastrService
  ) { }

  ngOnInit() {
    $(document).ready(function () {
      $('[data-toggle="tooltip"]').tooltip();
    });
    this.user = this.authenticationService.currentUserValue;
    if (this.user.role === UserRole.Student) {
      this.isTeacher = false;
      this.schoolGradeSectionId = this.user.school_grade_section_id;
    } else if (this.user.role === UserRole.Teacher) {
      this.isTeacher = true;
      this.schoolGradeSectionId = this.school_grade_section_id;
    }
    if (!this.showInformationForRecall) {
      console.log('user', this.user);
      if (this.school_subject_id != null) {
        this.selectedSubject = this.school_subject_name;
        this.selectedSchoolSubjectId = this.school_subject_id;
        this.selectedSchoolGradeSubjectId = this.school_grade_subject_id;
        this.SubjectId = this.school_subject_id
        this.getTopic(this.SubjectId);
      }
      else {
        this.getSubjects();
      }

      setTimeout(() => {
        this.showLoader = false;
      }, 100);
    } else {
      this.testLocalStorageObj = JSON.parse(localStorage.getItem('testLocalStorageObj'));
      this.instruction = this.testLocalStorageObj.practiceTestObj.instructions;
      this.showInstructions = true;
    }
  }

  getSubjects() {
    this.showLoader = true;
    this.recommendationService
      .GetSubjects(this.schoolGradeSectionId, this.user.user_id)
      .subscribe((res) => {
        let ApiResult = res as any;
        for (let subject of ApiResult.data) {
          this.subjectsList.push({
            SubjectName: subject.schoolSubjectName,
            id: Number(subject.subjectId),
            schoolGradeSubjectId: subject.schoolGradeSubjectId,
            schoolSubjectId: subject.schoolSubjectId,
          });
        }

        this.selectedSchoolSubjectId = this.subjectsList[0].schoolSubjectId;
        this.selectedSchoolGradeSubjectId = this.subjectsList[0].schoolGradeSubjectId;
        this.selectedSubject = this.subjectsList[0].SubjectName;
        this.SubjectId = this.subjectsList[0].id;
        this.getTopic(this.subjectsList[0].schoolSubjectId);

      });
  }

  onSubjectChange(subject) {
    this.selectedSubject = subject.SubjectName;
    this.SubjectId = subject.schoolSubjectId;
    this.selectedSchoolSubjectId = subject.schoolSubjectId;
    this.selectedSchoolSubjectId = subject.schoolGradeSubjectId;
    this.getTopic(subject.schoolSubjectId);
    this.selectedSubTopic = "All SubTopics";
    this.SubTopicId = -1;
    this.selectedTopic = "All Topics";
    this.TopicId = -1;
  }

  getTopic(schoolSubjectId: number) {
    this.showLoader = true;
    this.recommendationService
      .GetTopics(this.schoolGradeSectionId, schoolSubjectId, this.user.user_id)
      .subscribe((res) => {
        let ApiResult = res as any;
        this.topicsList = [];
        for (let topic of ApiResult.data) {
          this.topicsList.push({
            TopicName: topic.topicName,
            id: Number(topic.topicId),
          });
        }
        this.showLoader = false;
      });
  }

  onTopicChange(topic) {
    this.selectedTopic = topic.TopicName;
    this.TopicId = topic.id;
    this.selectedSubTopic = "All SubTopics";
    this.SubTopicId = -1;
    this.getSubTopic(this.selectedSchoolSubjectId, topic.id);
  }
  getSubTopic(schoolSubjectId: number, topicId: number) {
    this.showLoader = true;
    this.recommendationService
      .GetSubTopics(this.schoolGradeSectionId, schoolSubjectId, topicId, this.user.user_id)
      .subscribe((res) => {
        let ApiResult = res as any;
        this.subTopicsList = [];
        for (let subTopic of ApiResult.data) {
          this.subTopicsList.push({
            SubTopicName: subTopic.subTopicName,
            id: Number(subTopic.subTopicId),
          });
        }
        this.showLoader = false;
      });
  }
  onSubTopicChange(subtopic) {
    this.selectedSubTopic = subtopic.SubTopicName;
    this.SubTopicId = subtopic.id;
  }
  onCloseModal() {
    this.modalService.dismissAll();
  }
  onStartTest() {
    this.startTestClicked = true;
    if (!this.isTeacher) {
      let body = {
        userId: this.user.user_id,
        schoolGradeSubjectId: this.SubjectId,
        topicId: this.TopicId,
        subtopicId: this.SubTopicId,
        // duration: this.testDuration,
        numberOfQuestions: this.numberOfQuestions,
        // test_type: "practice",
        // toughness: this.toughnessOfTest,
      };
      if (this.SubTopicId == -1) {
        delete body.subtopicId;
      }
      if (this.TopicId == -1) {
        delete body.topicId;
      }
      this.practiceTestService.startPracticeTest(body).subscribe((res: any) => {
        if (res.success) {
          this.startTestClicked = false;
          this.showInstructions = true;
          this.practiceTestData = res.data;
          this.instruction = res.data.instructions;
          localStorage.removeItem("testLocalStorageObj");
          localStorage.removeItem('testDetail');
          this.showInstructionLoader = true;
          setTimeout(() => {
            this.showInstructionLoader = false;
          }, 100);
        }
      });
    } else {
      let body = {
        schoolGradeSectionId: this.schoolGradeSectionId,
        teacherId: this.user.user_id,
        schoolGradeSubjectId: this.selectedSchoolGradeSubjectId,
        topicId: this.TopicId,
        subtopicId: this.SubTopicId,
        duration: this.testDuration,
        // numberOfQuestions: this.numberOfQuestions,
        // test_type: "practice",
        // toughness: this.toughnessOfTest,
      };
      if (this.SubTopicId == -1) {
        delete body.subtopicId;
      }
      if (this.TopicId == -1) {
        delete body.topicId;
      }
      this.practiceTestService.createRecallTest(body).subscribe((res: any) => {
        if (res.success) {
          this.startTestClicked = false;
          this.toaster.success(res.msg);
          this.modalService.dismissAll();
        } else {
          this.toaster.error(res.msg);
          this.modalService.dismissAll();
        }
      }, err => {
        this.toaster.error(err.msg);
      });
    }

  }
  onStartTestAfterInstruction() {
    if (!this.showInformationForRecall) {
      localStorage.setItem("practiceTestGaurd", "true");
      let obj = {
        subject: this.selectedSubject,
        topic_name: this.selectedTopic,
        subtopic_name: this.selectedSubTopic
      }
      localStorage.setItem('testDetail', JSON.stringify(obj))
      this.practiceTestService.practiceTestData.next(this.practiceTestData);
      this.testLocalStorageObj = new TestLocalStorageObj();
      this.testLocalStorageObj.practiceTestObj = this.practiceTestData;
      localStorage.setItem(
        "testLocalStorageObj",
        JSON.stringify(this.testLocalStorageObj)
      );
      this.modalService.dismissAll();
      this.router.navigate(["/practice-test"]);
    } else {
      this.modalService.dismissAll();
      this.router.navigate(["/practice-test"]);
    }
  }
}
