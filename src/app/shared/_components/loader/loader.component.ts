import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {
  showLoader = false;
  constructor() { }

  ngOnInit() {
  }

  ShowLoader() {
    this.showLoader = true;
  }

  HideLoader() {
    this.showLoader = false;
  }
}
