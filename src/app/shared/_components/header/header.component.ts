import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { UserInfo } from '../../../shared/_model/user-info';
import { AuthenticationService } from '../../../shared/_services/auth.service';
import { DashboardService } from '../../../shared/_services/dashboard.service';
import { UserRole } from '../../_enumerations/UserRole';
import { NotificationTypes } from '../../_enumerations/Notification_Types';
import { PracticeTestService } from '../../_services/practice-test.service';
import { TestLocalStorageObj } from '../../_model/practice-test';
import { TestConfigurationComponent } from '../test-configuration/test-configuration.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from '../../_services/common.service';
import { NavServiceService } from '../mat-sidenav/nav-service.service';
import { stripTags } from '@amcharts/amcharts4/.internal/core/utils/Utils';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit, OnDestroy {
	user: UserInfo;
	public UserRole = UserRole;
	public notificationType = NotificationTypes;
	public dateTimeNow: Date = new Date();
	notifications: any = [];
	practiceTestData: any;
	testLocalStorageObj = new TestLocalStorageObj();
	showLoader = false;
	initials: any;
	interval: any;



	constructor(
		private router: Router,
		private authenticationService: AuthenticationService,
		private dashboardService: DashboardService,
		private practiceTestService: PracticeTestService,
		private modalService: NgbModal,
		private toasterService: ToastrService,
		public commonService: CommonService,
		public navService: NavServiceService
	) {

		//this.user = JSON.parse(localStorage.getItem("userInfo"));
		this.user = this.authenticationService.currentUserValue;
		this.initials = this.commonService.getShortName(this.user.first_name + ' ' + this.user.last_name);
		if (
			this.user.role === this.UserRole.Principal &&
			this.user.teacher === true
		) {
			if (localStorage.getItem('principalViewAsTeacher') === null) {
				localStorage.setItem('principalViewAsTeacher', 'false');
			} else if (localStorage.getItem('principalViewAsTeacher') === 'true') {
				this.user.role = UserRole.Teacher;
			}
		}

		setInterval(() => {
			this.dateTimeNow = new Date();
		}, 1000);


		if ([UserRole.Student, UserRole.Teacher].includes(this.authenticationService.currentUserValue.role)) {
			this.getNotifications();
			this.interval = setInterval(() => {
				this.getNotifications();

			}, 15000)
		}

	}
	toggleMenu() {
		if (!document.querySelector(".main-sidebar").classList.contains("toggled")) {
			document.querySelectorAll(".sidebar").forEach(element => {
				element.classList.add("toggled");
			});
			if (!!document.querySelector('.sidebar-brand')) {
				(document.querySelector('.sidebar-brand') as HTMLElement).style.marginTop = "35px";
				(document.querySelector('.sidebar-brand') as HTMLElement).style.marginBottom = "50px";
			}
			document.querySelectorAll('.nav-sideMenu').forEach(element => {
				(element as HTMLElement).style.left = "0px";
				element.children[0].children[1].classList.add("hidden");
			});
			document.querySelector('.userInfo').classList.add("hidden");
			document.querySelector('.logo').classList.add("hidden");
			document.querySelector('.copyrightArea').classList.add("hidden");
		}
		else {
			document.querySelectorAll(".sidebar").forEach(element => {
				element.classList.remove("toggled");
			});
			(document.querySelector('.sidebar-brand') as HTMLElement).style.marginTop = "0px";
			(document.querySelector('.sidebar-brand') as HTMLElement).style.marginBottom = "0px";
			document.querySelectorAll('.nav-sideMenu').forEach(element => {
				// (element as HTMLElement).style.left = "40px";
				(element as HTMLElement).style.left = "10px";
				element.children[0].children[1].classList.remove("hidden");
			});
			document.querySelector('.userInfo').classList.remove("hidden");
			document.querySelector('.logo').classList.remove("hidden");
			document.querySelector('.copyrightArea').classList.remove("hidden");
		}
	}

	ngOnInit() {
		//	this.user = this.authenticationService.currentUserValue;
	}

	toggle(isShowing) {
		this.navService.appDrawer.open();
		if (isShowing == true) {
			this.navService.currentSubject.next(true);
			this.navService.isShowingToggleSubject.next(false);
			document.getElementById("menuData").style.width = "14rem";
			document.getElementById('addFlex').style.marginTop = '0px';
			if (!!document.querySelector('.setZIndexFlex'))
				document.getElementsByClassName('setZIndexFlex')[0]['style'].zIndex = 0
		}
		else {
			this.navService.currentSubject.next(false);
			this.navService.isShowingToggleSubject.next(true);
			if (window.screen.width < 576)
				this.navService.closeNav();
			else {
				document.getElementById("menuData").style.width = '4rem';
				document.getElementById('addFlex').style.marginTop = '150px';
				if (!!document.querySelector('.setZIndexFlex'))
					document.getElementsByClassName('setZIndexFlex')[0]['style'].zIndex = 1;
			}

		}
	}

	getNotifications() {
		if (!this.authenticationService.currentUserValue) {
			return;
		}
		this.dashboardService.GetNotifications(this.user.user_id).subscribe(
			(res) => {
				var result = res as any;
				this.notifications = result.data;
				if (this.notifications.length > 0) {
					this.notifications.forEach(element => {
						element.createdAt = new Date(element.createdAt);
					});
				}
			},
			(err) => {
				console.log(err);
			}
		);
	}

	logout() {
		this.authenticationService.logout();
		this.router.navigate(['/login']);
		this.navService.currentSubject.next(false);
		this.navService.isShowingToggleSubject.next(true);
	}

	isPrincipalTeacher() {
		if (this.user.user_type === 'PRINCIPAL' && this.user.teacher === true) {
			return true;
		}
		return false;
	}


	changePrincipalRole() {
		let viewAsTeacher = localStorage.getItem('principalViewAsTeacher');
		if (viewAsTeacher === 'false') {
			localStorage.setItem('principalViewAsTeacher', 'true');
		} else {
			localStorage.setItem('principalViewAsTeacher', 'false');
		}
		window.location.reload();
	}


	getAlternateRole() {
		let viewAsTeacher = localStorage.getItem('principalViewAsTeacher');
		if (viewAsTeacher === 'true') {
			return 'Principal';
		}
		return 'Teacher';
	}


	getUnSeenNotificationCount() {
		let newNotificationCount = 0;
		(this.notifications as []).forEach((notification: any) => {
			if (localStorage.getItem('lastReadNotification_id') < notification.notification_id) {
				newNotificationCount = (newNotificationCount + 1);
			}
		})
		return newNotificationCount;
	}


	markNotificationsAsRead() {
		if (this.notifications.length > 0) {
			let lastReadNotification_id = this.notifications[0].notification_id;
			localStorage.setItem('lastReadNotification_id', lastReadNotification_id);
		}
	}


	redirectToClassroom(classroomId: string) {
		let url = window.location.protocol + '//' + window.location.host + '/#/classroom/' + classroomId;
		window.open(url);
	}

	redirectToClassroomV2(classroomId: string) {
		this.showLoader = true;

		this.dashboardService.getClassroomLaunchUrl(classroomId, this.user.user_id).subscribe(
			(classroom) => {
				if (classroom.success) {
					let url = classroom.data.launchUrl;
					window.location.href = url;
				} else {
					this.toasterService.error(classroom.msg);
					this.showLoader = false;
				}
			}, (err) => {
				this.toasterService.error('Something went wrong. Please check your internet and try again!');
				this.showLoader = false;
			}
		);
	}

	redirectToClassroomV3(classroomUrl: string) {
		if (!!classroomUrl) {
			window.open(classroomUrl);
		} else {
			this.toasterService.error('Could not join class. Please try again!');
		}
	}


	onRecallTest(id) {
		let obj = {
			studentId: this.user.user_id,
			testId: id
		};
		this.showLoader = true;
		this.practiceTestService.fetchRecallTest(obj).subscribe((res: any) => {
			if (res.success) {
				localStorage.removeItem('testLocalStorageObj');
				localStorage.removeItem('testDetail');
				localStorage.setItem('practiceTestGaurd', 'true');
				this.practiceTestData = res.data;
				this.practiceTestService.practiceTestData.next(this.practiceTestData);
				this.testLocalStorageObj = new TestLocalStorageObj();
				this.testLocalStorageObj.practiceTestObj = this.practiceTestData;
				localStorage.setItem(
					'testLocalStorageObj',
					JSON.stringify(this.testLocalStorageObj)
				);
				let modalRef = this.modalService.open(TestConfigurationComponent, { ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true });
				modalRef.componentInstance.school_grade_section_id = 0;
				modalRef.componentInstance.showInformationForRecall = true;
				modalRef.componentInstance.school_subject_name = null;
				modalRef.componentInstance.school_subject_id = 0;
				// this.router.navigate(["/practice-test"]);
			} else {
				this.toasterService.error(res.msg);
			}
			this.showLoader = false;
		}, err => {
			this.showLoader = false;
			this.toasterService.error(err);
		});
	}


	ngOnDestroy(): void {
		if (this.interval) {
			clearInterval(this.interval);
		}
	}

}
