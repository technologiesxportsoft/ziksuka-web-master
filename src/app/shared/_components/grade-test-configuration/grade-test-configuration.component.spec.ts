import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GradeTestConfigurationComponent } from './grade-test-configuration.component';

describe('GradeTestConfigurationComponent', () => {
  let component: GradeTestConfigurationComponent;
  let fixture: ComponentFixture<GradeTestConfigurationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GradeTestConfigurationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GradeTestConfigurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
