import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DashboardService } from '../../_services/dashboard.service';
import { UserInfo } from '../../_model/user-info';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-grade-test-configuration',
  templateUrl: './grade-test-configuration.component.html',
  styleUrls: ['./grade-test-configuration.component.css']
})
export class GradeTestConfigurationComponent implements OnInit {
  user: UserInfo;
  constructor(private modalService: NgbModal,private dashboardService: DashboardService,private toaster: ToastrService) { }

  subjectList = [];
  selectedsubject = [];
  subjectDropdownSettings = {};
  topicList = [];
  selectedtopic = [];
  topicDropdownSettings = {};
  subTopicList = [];
  selectedSubTopic = [];
  subTopicDropdownSettings = {};
  testTypeList = [];
  selectedTestType = [];
  testTypeDropdownSettings = {};
  testDuration = 0 ;
  numberofQuestions = 1 ;
  toughnessOfTest = 1 ;
  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('userInfo'));
    this.getSubjects();
    this.subjectDropdownSettings = {
      singleSelection: true,
      text: "Subject",
      enableSearchFilter: false,
      enableCheckAll:false
    };
    this.topicDropdownSettings = {
      singleSelection: true,
      text: "Topic",
      enableSearchFilter: false,
      enableCheckAll:false,
      noDataLabel:"Select Subject"
    };
    this.subTopicDropdownSettings = {
      singleSelection: true,
      text: "Sub Topic",
      enableSearchFilter: false,
      enableCheckAll:false,
      noDataLabel:"Select Topic"
    };
    this.testTypeList = [
      { id: "practice", itemName: "practice" },
      { id: "recall", itemName: "recall" }
    ];
    this.testTypeDropdownSettings = {
      singleSelection: true,
      text: "Choose a Type",
      enableSearchFilter: false,
      enableCheckAll:false,
    };
  }
  onSubjectSelect(item: any) {
    this.getTopics(item.id);
console.log(this.selectedsubject);
  }
  OnSubjectDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedsubject);
  }
  onSubjectDeSelectAll(items: any) {
    console.log(items);
  }
  onTopicSelect(item: any) {
    this.getSubTopics(item.id);
    console.log(this.selectedsubject);
  }
  OnTopicDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedsubject);
  }
  onTopicDeSelectAll(items: any) {
    console.log(items);
  }
  onTestTypeSelect(item: any) {
    console.log(item);
    console.log(this.selectedsubject);
  }
  OnTestTypeDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedsubject);
  }
  onTestTypeDeSelectAll(items: any) {
    console.log(items);
  }
  onSubTopicSelect(item: any) {
    console.log(item);
    console.log(this.selectedsubject);
  }
  OnSubTopicDeSelect(item: any) {
    console.log(item);
    console.log(this.selectedsubject);
  }
  onSubTopicDeSelectAll(items: any) {
    console.log(items);
  }
  onCloseModal() {
    this.modalService.dismissAll();
  }
  getSubjects(){
this.dashboardService.GetSubjects(this.user.id).subscribe(res=>{
let subjectsObject = res["data"].subjects;
let subjectList = [];
Object.keys(subjectsObject)
  .forEach(function eachKey(key) {
    let subject = {
      id: subjectsObject[key], itemName: key
    }; 
    subjectList.push(subject);
  });
  this.subjectList = subjectList;
});
  }
  getTopics(subjectId){
    this.dashboardService.GetTopics(this.user.id,subjectId).subscribe(res=>{
      let topicsObject = res["data"].topics;
      let topicList = [];
      Object.keys(topicsObject)
        .forEach(function eachKey(key) {
          let topic = {
            id: topicsObject[key], itemName: key
          }; 
          topicList.push(topic);
        });
        this.topicList = topicList;
      });
}
getSubTopics(topicId){
  this.dashboardService.GetSubTopics(this.user.id,this.selectedsubject[0].id,topicId).subscribe(res=>{
     let subtopicsObject = res["data"].subtopics;
    let subTopicList = [];
    Object.keys(subtopicsObject)
      .forEach(function eachKey(key) {
        let subtopic = {
          id: subtopicsObject[key], itemName: key
        }; 
        subTopicList.push(subtopic);
      });
      this.subTopicList = subTopicList;
    });
}
  pushTest(){
let model = {
  subject_id: this.selectedsubject[0].id,
  topic_id: this.selectedtopic.length>0?this.selectedtopic[0].id:0,
  subtopic_id: this.selectedSubTopic.length>0?this.selectedSubTopic[0].id:0,
  duration: this.testDuration,
  num_of_questions: this.numberofQuestions,
  test_type: this.selectedTestType.length>0?this.selectedTestType[0].id:"recall",
  test_complexity:this.toughnessOfTest
}
this.dashboardService.pushTest(model).subscribe(res=>{
  this.toaster.success(res.msg);
this.onCloseModal();
});
  }
}
