import { Pipe, PipeTransform } from '@angular/core';

// tslint:disable:max-line-length
@Pipe({
  name: 'prepareQuestion'
})
export class PrepareQuestionPipe implements PipeTransform {

  transform(q: any, ...args: any[]): any {
    const qHtml = q.questionHtml;
    if (!qHtml) {
      return '';
    }
    if (qHtml.indexOf('<blank>') > -1) {
      let replaceString = qHtml;
      if (q.type === 'FILL_IN_THE_BLANK') {
        const inputString = `<span class="ptag"><input style="border: none;outline:none;border-bottom: black 1px solid;" disabled type="text"></span>`;
        replaceString = replaceString.replace('<blank>', inputString);
      } else {
        const inputString = `<p class="ptag"><input style="border: none;outline:none;border-bottom: black 1px solid;" type="text" disabled ></p>`;
        replaceString = replaceString.replace('<blank>', inputString);
      }
      return replaceString;
    } else {
      return qHtml;
    }
  }
}
