import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'prepareOptions'
})
export class PrepareOptionsPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    if (!value) {
      return [];
    }
    const rows = [];
    for (let i = 0; i < value.length; i = i + 2) {
      const arrayObj = [];
      arrayObj.push(value[i]);
      if (value[i + 1] !== undefined) {
        arrayObj.push(value[i + 1]);
      }
      rows.push(arrayObj);
    }
    return rows;
  }

}
