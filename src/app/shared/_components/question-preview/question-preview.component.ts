import { Component, OnInit, Input, Inject } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CommonService } from '../../_services/common.service';

/**
 * COMING SOON?
 * Expecting inputs
 * 1) meta: Object
 *    - schoolSubjectName
 *    - topicName
 *    - subtopicName
 *
 * 2) minutesLeft
 * 3) secondsLeft
 * 4) currentQuestionNo
 *
 * 5) question: Observable
 *     This is the only way to update the question. To change the question, emit a new value
 */

@Component({
  selector: 'app-question-preview',
  templateUrl: './question-preview.component.html',
  styleUrls: ['./question-preview.component.css']
})
export class QuestionPreviewComponent implements OnInit {

  trueFalse = [{ key: 'true', value: 'True' }, { key: 'false', value: 'False' }];
  @Input() currentQuestionNo = 1;
  @Input() questionsLength = 1;
  @Input() question: Observable<any>;
  @Input() meta;
  mathJaxObject;

  constructor(
    public dialogRef: MatDialogRef<QuestionPreviewComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: any, private commonService: CommonService
  ) {
    this.renderMath();
    if (this.dialogData) {
      if (this.dialogData.meta) {
        this.meta = this.dialogData.meta;
      }

      if (this.dialogData.question) {
        this.question = new BehaviorSubject<any>(this.dialogData.question);
      } else {
        this.question = new BehaviorSubject<any>({
          questionHtml: 'This is the default question which you have to find the answer to(?)',
          options: ['Default option 1', 'Default option 2', 'Default option 3', 'Default option 4'],
          type: 'MCO',
          score: 3,
          correct_answer: 2
        });
      }
    }
  }

  ngOnInit() {
  }

  isCorrectOption(qObservable, option) {
    if (!qObservable || !qObservable.correctAnswer) {
      return false;
    }
    return qObservable.options.filter((e, i) => qObservable.correctAnswer.includes(i + 1)).includes(option);
  }
  updateMathObt() {
    this.mathJaxObject = this.commonService.nativeGlobal()['MathJax'];
  }

  renderMath() {
    this.updateMathObt();
    let angObj = this;
    setTimeout(() => {
      angObj.mathJaxObject['Hub'].Queue(["Typeset", angObj.mathJaxObject.Hub], 'mathContent');
    }, 1000)
  }

  loadMathConfig() {
    this.updateMathObt();
    this.mathJaxObject.Hub.Config({
      showMathMenu: false,
      // tex2jax: { inlineMath: [["$", "$"]], displayMath: [["$$", "$$"]] },
      tex2jax: { inlineMath: [["$", "$"], ["\\(", "\\)"]], displayMath: [["$$", "$$"], ["$", "$"]] },
      menuSettings: { zoom: "Double-Click", zscale: "150%" },
      CommonHTML: { linebreaks: { automatic: true } },
      "HTML-CSS": { linebreaks: { automatic: true } },
      SVG: { linebreaks: { automatic: true } }
    });
  }

}
