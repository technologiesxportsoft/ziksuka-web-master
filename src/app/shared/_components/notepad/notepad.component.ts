import { Component, Input, ElementRef, AfterViewInit, ViewChild } from '@angular/core';
import { fromEvent, merge } from 'rxjs';
import { switchMap, takeUntil, pairwise, endWith } from 'rxjs/operators';
import { AbstractControl, FormControl } from '@angular/forms';

// tslint:disable:align
@Component({
	selector: 'app-notepad',
	templateUrl: './notepad.component.html',
	styleUrls: ['./notepad.component.css']
})
export class NotepadComponent implements AfterViewInit {

	@Input() public width = 1280;
	@Input() public height = 720;

	@ViewChild('notepad', null) public canvas: ElementRef;
	@ViewChild('overlay', null) public overlay: ElementRef;
	overlayVisible = false;

	private cx: CanvasRenderingContext2D;
	private ocx: CanvasRenderingContext2D;

	private mainCanvasEvents;
	private overlayCanvasEvents;
	public OVERLAY_STATUS = {
		IDLE: null,
		PENCIL: 'pencil',
		LINE: 'line',
		RECTANGLE: 'rectangle',
		ELLIPSE: 'ellipse',
		ERASE: 'erase'
	};
	public overlayStatus: string;
	private overlayData: any = {};

	public paletteDisabled = false;
	private savedBrushColor = null;



	brushColor = 'rgba(0,0,255,1)';
	brushSize = 2;

	goFullScreen = false;


	ngAfterViewInit() {
		const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;
		const overlayEl: HTMLCanvasElement = this.overlay.nativeElement;
		this.cx = canvasEl.getContext('2d');
		this.ocx = overlayEl.getContext('2d');

		canvasEl.width = this.width;
		canvasEl.height = this.height;
		overlayEl.width = this.width;
		overlayEl.height = this.height;
		// console.log(this.width);
		// console.log(this.cx.canvas.width);

		this.cx.fillStyle = 'white';
		this.cx.fillRect(0, 0, this.cx.canvas.width, this.cx.canvas.height);
		this.ocx.fillStyle = 'white';
		this.ocx.fillRect(0, 0, this.ocx.canvas.width, this.ocx.canvas.height);

		this.cx.lineWidth = this.brushSize;
		this.cx.lineCap = 'round';
		this.cx.strokeStyle = this.brushColor;
		this.ocx.lineWidth = this.brushSize;
		this.ocx.lineCap = 'round';
		this.ocx.strokeStyle = this.brushColor;

		this.captureEvents(canvasEl, overlayEl);

		// code for default pencil setting
		this.overlayStatus = this.OVERLAY_STATUS.IDLE;

		fromEvent(document, 'fullscreenchange')
			.subscribe(e => this.exitFullScreenHandler(e));
		// if (document.addEventListener) {
		//   document.addEventListener('fullscreenchange', this.exitFullScreenHandler, false);
		//   document.addEventListener('mozfullscreenchange', this.exitFullScreenHandler, false);
		//   document.addEventListener('MSFullscreenChange', this.exitFullScreenHandler, false);
		//   document.addEventListener('webkitfullscreenchange', this.exitFullScreenHandler, false);
		// }
	}

	private captureEvents(canvasEl: HTMLCanvasElement, overlayEl: HTMLCanvasElement) {

		const mainCanvasMouseDown = merge(fromEvent(canvasEl, 'mousedown'),
			fromEvent(canvasEl, 'touchstart'));

		const mainCanvasMouseUp = merge(fromEvent(canvasEl, 'mouseup'),
			fromEvent(canvasEl, 'touchend'));

		const mainCanvasMouseMove = merge(fromEvent(canvasEl, 'mousemove'),
			fromEvent(canvasEl, 'touchmove'));

		// this will capture all mousedown events from the canvas element
		this.mainCanvasEvents = mainCanvasMouseDown
			.pipe(
				switchMap((e) => {
					// after a mouse down, we'll record all mouse moves
					return mainCanvasMouseMove
						.pipe(
							// we'll stop (and unsubscribe) once the user releases the mouse
							// this will trigger a 'mouseup' event
							takeUntil(mainCanvasMouseUp),
							// we'll also stop (and unsubscribe) once the mouse leaves the canvas (mouseleave event)
							takeUntil(fromEvent(canvasEl, 'mouseleave')),
							// pairwise lets us get the previous value to draw a line from
							// the previous point to the current point
							pairwise()
						);
				})
			);

		const overlayCanvasMouseDown = merge(fromEvent(overlayEl, 'mousedown'),
			fromEvent(overlayEl, 'touchstart'));

		const overlayCanvasMouseUp = merge(fromEvent(overlayEl, 'mouseup'),
			fromEvent(overlayEl, 'touchend'));

		const overlayCanvasMouseMove = merge(fromEvent(overlayEl, 'mousemove'),
			fromEvent(overlayEl, 'touchmove'));

		this.overlayCanvasEvents = overlayCanvasMouseDown
			.pipe(
				switchMap((e) => {
					// after a mouse down, we'll record all mouse moves
					return overlayCanvasMouseMove
						// .pipe(
						//   merge(fromEvent(overlayEl, 'mouseup'), fromEvent(overlayEl, 'mouseleave'))
						// )
						.pipe(
							// we'll stop (and unsubscribe) once the user releases the mouse
							// this will trigger a 'mouseup' event
							takeUntil(overlayCanvasMouseUp),
							// we'll also stop (and unsubscribe) once the mouse leaves the overlay (mouseleave event)
							takeUntil(fromEvent(overlayEl, 'mouseleave')),
							endWith({ type: 'mouseup' }),
							// pairwise lets us get the previous value to draw a line from
							// the previous point to the current point
							pairwise(),
						);
				})
			);

		this.mainCanvasEvents.subscribe((res: [MouseEvent, MouseEvent]) => {
			const rect = canvasEl.getBoundingClientRect();
			let prevPos;
			let currentPos;
			if (res[0] instanceof TouchEvent && res[1] instanceof TouchEvent) {
				prevPos = { x: res[0].targetTouches[0].clientX - rect.left, y: res[0].targetTouches[0].clientY - rect.top };
				currentPos = { x: res[1].targetTouches[0].clientX - rect.left, y: res[1].targetTouches[0].clientY - rect.top };
			} else {
				prevPos = { x: res[0].clientX - rect.left, y: res[0].clientY - rect.top };
				currentPos = { x: res[1].clientX - rect.left, y: res[1].clientY - rect.top };
			}
			// previous and current position with the offset
			// console.log(currentPos, this.brushColor);

			// this method we'll implement soon to do the actual drawing
			this.drawOnCanvas(prevPos, currentPos, rect);
		});

		this.overlayCanvasEvents.subscribe((res: [MouseEvent, MouseEvent]) => {
			const rect = overlayEl.getBoundingClientRect();
			// previous and current position with the offset
			let prevPos;
			let currentPos;
			if (res[0] instanceof TouchEvent && res[1] instanceof TouchEvent) {
				prevPos = { x: res[0].targetTouches[0].clientX - rect.left, y: res[0].targetTouches[0].clientY - rect.top };
				currentPos = { x: res[1].targetTouches[0].clientX - rect.left, y: res[1].targetTouches[0].clientY - rect.top };
			} else {
				prevPos = { x: res[0].clientX - rect.left, y: res[0].clientY - rect.top };
				currentPos = { x: res[1].clientX - rect.left, y: res[1].clientY - rect.top };
			}

			switch (this.overlayStatus) {
				case this.OVERLAY_STATUS.LINE: {
					if (!this.overlayData.initPoint) {
						this.overlayData.initPoint = prevPos;
						// console.log('trying to set initpoint', this.overlayData);
					} else {
						if (res[1].type === 'mouseup' || res[1].type === 'mouseleave' || res[1].type === 'touchend') {
							// console.log('Finished drawing line');
							this.overlayStatus = this.OVERLAY_STATUS.PENCIL;
							this.overlayData = {};
							this.cx.drawImage(overlayEl, 0, 0);
							this.hideOverlay();
							return;
						}
						this.ocx.lineWidth = this.brushSize;
						this.ocx.lineCap = 'round';
						this.ocx.clearRect(0, 0, this.width, this.height);
						this.ocx.strokeStyle = this.brushColor;
						this.ocx.beginPath();
						const scaleX = this.width / rect.width;
						const scaleY = this.height / rect.height;
						this.ocx.moveTo(this.overlayData.initPoint.x * scaleX, this.overlayData.initPoint.y * scaleY);
						this.ocx.lineTo(currentPos.x * scaleX, currentPos.y * scaleY);
						this.ocx.stroke();
						// console.log(res[1]);
					}
				}
					break;

				case this.OVERLAY_STATUS.ELLIPSE: {
					if (!this.overlayData.initPoint) {
						this.overlayData.initPoint = prevPos;
						// console.log('trying to set initpoint', this.overlayData);
					} else {
						if (res[1].type === 'mouseup' || res[1].type === 'mouseleave' || res[1].type === 'touchend') {
							// console.log('Finished drawing ellipse');
							this.overlayStatus = this.OVERLAY_STATUS.PENCIL;
							this.overlayData = {};
							this.cx.drawImage(overlayEl, 0, 0);
							this.hideOverlay();
							return;
						}
						const scaleX = this.width / rect.width;
						const scaleY = this.height / rect.height;

						this.ocx.clearRect(0, 0, this.width, this.height);
						this.ocx.strokeStyle = this.brushColor;
						this.ocx.lineWidth = this.brushSize;
						this.ocx.beginPath();
						const center = {
							x: (currentPos.x * scaleX + this.overlayData.initPoint.x * scaleX) / 2,
							y: (currentPos.y * scaleY + this.overlayData.initPoint.y * scaleY) / 2,
						};
						const radius = {
							x: Math.abs(currentPos.x * scaleX - this.overlayData.initPoint.x * scaleX) / 2,
							y: Math.abs(currentPos.y * scaleY - this.overlayData.initPoint.y * scaleY) / 2,
						};
						this.ocx.ellipse(
							center.x, center.y,
							radius.x, radius.y,
							0, 0, 2 * Math.PI
						);
						this.ocx.stroke();
						// console.log(res[1]);
					}
				}
					break;

				case this.OVERLAY_STATUS.RECTANGLE: {
					if (!this.overlayData.initPoint) {
						this.overlayData.initPoint = prevPos;
						// console.log('trying to set initpoint', this.overlayData);
					} else {
						if (res[1].type === 'mouseup' || res[1].type === 'mouseleave' || res[1].type === 'touchend') {
							// console.log('Finished drawing rectangle');
							this.overlayStatus = this.OVERLAY_STATUS.PENCIL;
							this.overlayData = {};
							this.cx.drawImage(overlayEl, 0, 0);
							this.hideOverlay();
							return;
						}
						const scaleX = this.width / rect.width;
						const scaleY = this.height / rect.height;

						this.ocx.clearRect(0, 0, this.width, this.height);
						this.ocx.lineWidth = this.brushSize;
						this.ocx.strokeStyle = this.brushColor;
						this.ocx.beginPath();
						this.ocx.rect(
							this.overlayData.initPoint.x * scaleX, this.overlayData.initPoint.y * scaleY,
							(currentPos.x * scaleX - this.overlayData.initPoint.x * scaleX),
							(currentPos.y * scaleY - this.overlayData.initPoint.y * scaleY)
						);
						this.ocx.stroke();
						// console.log(res[1]);
					}
				}
					break;

					// Code for Default Selection -- nothing selected
					case this.OVERLAY_STATUS.IDLE: {}
						break;
			}


			
		});
	}

	private drawOnCanvas(prevPos: { x: number, y: number }, currentPos: { x: number, y: number }, rect) {
		if (this.overlayStatus == this.OVERLAY_STATUS.IDLE) {
			return;
		}
		if (!this.cx) { return; }
		this.cx.lineWidth = this.brushSize;
		this.cx.lineCap = 'round';
		this.cx.strokeStyle = this.brushColor;

		this.cx.beginPath();
		const scaleX = this.width / rect.width;
		const scaleY = this.height / rect.height;
		if (prevPos) {
			this.cx.strokeStyle = this.brushColor;
			this.cx.moveTo(prevPos.x * scaleX, prevPos.y * scaleY); // from
			this.cx.lineTo(currentPos.x * scaleX, currentPos.y * scaleY);
			this.cx.stroke();
		}
	}

	public clear() {
		const a = window.confirm('Do you want to clear the notebook screen?');
		// after clear pencil icon unselected
		this.overlayStatus = this.OVERLAY_STATUS.IDLE;
		if (!a) {
			return;
		}
		this.cx.clearRect(0, 0, this.width, this.height);
		this.cx.fillStyle = 'white';
		this.cx.fillRect(0, 0, this.cx.canvas.width, this.cx.canvas.height);
		this.restoreSavedBrushColor();
	}

	public restoreSavedBrushColor() {
		if (this.overlayStatus === this.OVERLAY_STATUS.ERASE) {
			this.brushColor = this.savedBrushColor;
			this.savedBrushColor = null;
		}
		this.paletteDisabled = false;
	}

	public startFreehand() {
		this.restoreSavedBrushColor();
		this.overlayStatus = this.OVERLAY_STATUS.PENCIL;
		this.hideOverlay();
	}

	public startLine() {
		this.restoreSavedBrushColor();
		this.overlayStatus = this.OVERLAY_STATUS.LINE;
		this.showOverlay();
	}

	public startRect() {
		this.restoreSavedBrushColor();
		this.overlayStatus = this.OVERLAY_STATUS.RECTANGLE;
		this.showOverlay();
	}

	public startEllipse() {
		this.restoreSavedBrushColor();
		this.overlayStatus = this.OVERLAY_STATUS.ELLIPSE;
		this.showOverlay();
	}

	public showOverlay() {
		this.ocx.clearRect(0, 0, this.width, this.height);
		this.overlayVisible = true;
	}

	private hideOverlay() {
		this.overlayVisible = false;
	}

	toggleFullScreen() {
		if ((document as any).webkitIsFullScreen || (document as any).mozFullScreen || (document as any).msFullscreenElement) {
			this.exitFullScreen();
		} else {
			const elem = document.documentElement as any;
			const methodToBeInvoked = elem.requestFullscreen || elem.webkitRequestFullScreen
				|| elem.mozRequestFullscreen || elem.msRequestFullscreen;
			if (methodToBeInvoked) {
				methodToBeInvoked.call(elem);
				this.goFullScreen = true;
			}
		}
	}

	exitFullScreenHandler(e) {
		// console.log('Full screen handler received', e);
		if (document.fullscreenElement === null) {
			// console.log('Exiting full screen');
			this.goFullScreen = false;
		} else {
			// console.log('Entering full screen');
			this.goFullScreen = true;
		}
	}

	private exitFullScreen() {
		if (document.exitFullscreen) {
			document.exitFullscreen();
		} else if ((document as any).mozCancelFullScreen) { /* Firefox */
			(document as any).mozCancelFullScreen();
		} else if ((document as any).webkitExitFullscreen) { /* Chrome, Safari and Opera */
			(document as any).webkitExitFullscreen();
		} else if ((document as any).msExitFullscreen) { /* IE/Edge */
			(document as any).msExitFullscreen();
		}
	}

	saveImage() {
		const link = document.createElement('a');
		link.download = 'basta-note.png';
		link.target = '_blank';
		link.href = (document.getElementById('notepad') as any).toDataURL('image/png');
		link.click();
	}

	startErase() {
		this.savedBrushColor = this.brushColor;
		this.brushColor = 'rgb(255, 255, 255)';
		this.paletteDisabled = true;
		this.overlayStatus = this.OVERLAY_STATUS.ERASE;
		this.hideOverlay();
	}
}
