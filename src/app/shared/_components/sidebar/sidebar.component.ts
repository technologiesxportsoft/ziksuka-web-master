import { Component, OnInit } from '@angular/core';
import { UserInfo } from 'src/app/shared/_model/user-info';
import { DashboardService } from 'src/app/shared/_services/dashboard.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TestConfigurationComponent } from '../test-configuration/test-configuration.component';
import { Router } from '@angular/router';
import { UserRole } from '../../_enumerations/UserRole';
import { CommonService } from '../../_services/common.service';

@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
	public UserRole = UserRole;
	user: UserInfo;
	notifications: any = [];
	aboutUser: string = "";
	initials: any;

	constructor(private modalService: NgbModal, private router: Router,
		public commonService: CommonService) { }


	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem('userInfo'));
		this.initials = this.commonService.getShortName(this.user.first_name + ' ' + this.user.last_name)
		if (this.user.role === this.UserRole.Principal && this.user.teacher === true) {
			if (localStorage.getItem("principalViewAsTeacher") === null) {
				localStorage.setItem("principalViewAsTeacher", 'false')
			} else if (localStorage.getItem("principalViewAsTeacher") === 'true') {
				this.user.role = UserRole.Teacher;
			}
		}
		
		if (this.user.role === UserRole.Student) {
			this.aboutUser = this.user.school_grade_name + " " + this.user.section_name;
		} else if (this.user.role === UserRole.Teacher) {
			this.aboutUser = "Teacher";
		} else if (this.user.role === UserRole.Principal) {
			this.aboutUser = "Principal";
		}
	
	}
	toggleMenu() {
		if (!document.querySelector(".main-sidebar").classList.contains("toggled")) {
			document.querySelectorAll(".sidebar").forEach(element => {
				element.classList.add("toggled");
				// element.classList.toggle("toggled");
			});
			(document.querySelector('.sidebar-brand') as HTMLElement).style.marginTop = "35px";
			(document.querySelector('.sidebar-brand') as HTMLElement).style.marginBottom = "50px";
			document.querySelectorAll('.nav-sideMenu').forEach(element => {
				(element as HTMLElement).style.left = "0px";
				element.children[0].children[1].classList.add("hidden");
			});
			document.querySelector('.userInfo').classList.add("hidden");
			document.querySelector('.logo').classList.add("hidden");
			document.querySelector('.copyrightArea').classList.add("hidden");
		}
		// else {
		// 	document.querySelectorAll(".sidebar").forEach(element => {
		// 		element.classList.remove("toggled");
		// 	});
		// 	(document.querySelector('.sidebar-brand') as HTMLElement).style.marginTop = "0px";
		// 	(document.querySelector('.sidebar-brand') as HTMLElement).style.marginBottom = "0px";
		// 	document.querySelectorAll('.nav-sideMenu').forEach(element => {
		// 		// (element as HTMLElement).style.left = "40px";
		// 		(element as HTMLElement).style.left = "10px";
		// 		element.children[0].children[1].classList.remove("hidden");
		// 	});
		// 	document.querySelector('.userInfo').classList.remove("hidden");
		// 	document.querySelector('.logo').classList.remove("hidden");
		// 	document.querySelector('.copyrightArea').classList.remove("hidden");
		// }
	}

	onPracticeTest() {
		let practiceTestGaurd = localStorage.getItem('practiceTestGaurd')
		let timerFlag = localStorage.getItem('timerFlag')
		if (practiceTestGaurd == 'true' && timerFlag == 'true') {
			this.router.navigate(['/practice-test'])
		} else {
			let modalRef = this.modalService.open(TestConfigurationComponent, { ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true });
			modalRef.componentInstance.school_grade_section_id = 0;
			modalRef.componentInstance.showInformationForRecall = false;
			modalRef.componentInstance.school_subject_name = null;
			modalRef.componentInstance.school_subject_id = 0;
		}
	}

}
