import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Event, NavigationEnd, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class NavServiceService {

  public appDrawer: any;
  public currentUrl = new BehaviorSubject<string>(undefined);
  isExpanded = false;
  isShowingToggle = true;

  currentSubject = new BehaviorSubject<boolean>(this.isExpanded);
  public currrentIsExpanded = this.currentSubject.asObservable();

  isShowingToggleSubject = new BehaviorSubject<boolean>(this.isShowingToggle);
  public currentShowingToggle = this.isShowingToggleSubject.asObservable();

  constructor(private router: Router) {
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.currentUrl.next(event.urlAfterRedirects);
      }
    });
  }

  public closeNav() {
    this.appDrawer.close();
  }

  public get currentUserValue() {
    return this.currentSubject.value;
  }

  public get currentShowingToggleValue() {
    return this.isShowingToggleSubject.value;
  }

  public openNav() {
    // this.appDrawer.open();
    this.appDrawer.toggle()
  }
}
