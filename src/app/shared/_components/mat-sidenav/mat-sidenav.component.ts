import { Component, OnInit, ViewChild, ElementRef, VERSION, ViewEncapsulation, Input, HostListener } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { NavServiceService } from './nav-service.service';
import { UserInfo } from '../../_model/user-info';
import { UserRole } from '../../_enumerations/UserRole';
import { CommonService } from '../../_services/common.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { TestConfigurationComponent } from '../test-configuration/test-configuration.component';
import {
  BreakpointObserver,
  Breakpoints,
  BreakpointState,
} from '@angular/cdk/layout';
@Component({
  selector: 'app-mat-sidenav',
  templateUrl: './mat-sidenav.component.html',
  styleUrls: ['./mat-sidenav.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MatSidenavComponent implements OnInit {

  @ViewChild('appDrawer', { static: true }) appDrawer: MatSidenav;
  public UserRole = UserRole;
  user: UserInfo;
  aboutUser: string = "";
  initials: any;
  navItems: any;
  isExpanded: boolean;
  version = VERSION;
  addStyle = true;
  isHandset$: Observable<boolean>;
  // public isHandset$: Observable<boolean> = this.breakpointObserver
  //   .observe(Breakpoints.Handset)
  //   .pipe(map((result: BreakpointState) => result.matches));

  constructor(public navService: NavServiceService, public commonService: CommonService,
    private modalService: NgbModal, private router: Router, private breakpointObserver: BreakpointObserver) {

  }

  ngOnInit() {
    this.user = JSON.parse(localStorage.getItem('userInfo'));
    this.initials = this.commonService.getShortName(this.user.first_name + ' ' + this.user.last_name)
    if (this.user.role === this.UserRole.Principal && this.user.teacher === true) {
      if (localStorage.getItem("principalViewAsTeacher") === null) {
        localStorage.setItem("principalViewAsTeacher", 'false')
      } else if (localStorage.getItem("principalViewAsTeacher") === 'true') {
        this.user.role = UserRole.Teacher;
      }
    }

    if (this.user.role === UserRole.Student) {
      this.aboutUser = this.user.school_grade_name + " " + this.user.section_name;
    } else if (this.user.role === UserRole.Teacher) {
      this.aboutUser = "Teacher";
    } else if (this.user.role === UserRole.Principal) {
      this.aboutUser = "Principal";
    }

    document.getElementById('menuData').style.width = '4rem';
    document.getElementById('addFlex').style.marginTop = '150px';

  }

  ngAfterViewInit() {
    this.navService.appDrawer = this.appDrawer;
    // document.getElementsByClassName('addFlex')[0]['style'].marginTop = '79px;';
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth < 576) {
      this.appDrawer.close();
    }
    else {
      this.appDrawer.open();
      this.navService.currentSubject.next(false);
      document.getElementById('menuData').style.width = '4rem';
      document.getElementById('addFlex').style.marginTop = '150px';
    }
  }
  toggleMenu() {
    if (!document.querySelector(".main-sidebar").classList.contains("toggled")) {
      document.querySelectorAll(".sidebar").forEach(element => {
        // this.navService.currentSubject.next(false);
        // element.classList.toggle("toggled");
      });
      if (!!document.querySelector('.sidebar-brand')) {
        (document.querySelector('.sidebar-brand') as HTMLElement).style.marginTop = "35px";
        (document.querySelector('.sidebar-brand') as HTMLElement).style.marginBottom = "50px";
      }
      document.querySelectorAll('.nav-sideMenu').forEach(element => {
        this.navService.currentSubject.next(false);
        this.navService.isShowingToggleSubject.next(true);
        if (window.screen.width < 576)
          this.navService.closeNav();
        else {
          document.getElementById("menuData").style.width = '4rem';
          document.getElementById('addFlex').style.marginTop = '150px';
        }
      });
      document.querySelector('.copyrightArea').classList.add("hidden");
    }
  }
  onPracticeTest() {
    let practiceTestGaurd = localStorage.getItem('practiceTestGaurd')
    let timerFlag = localStorage.getItem('timerFlag')
    if (practiceTestGaurd == 'true' && timerFlag == 'true') {
      this.router.navigate(['/practice-test'])
    } else {
      let modalRef = this.modalService.open(TestConfigurationComponent, { ariaLabelledBy: 'modal-basic-title', size: 'lg', centered: true });
      modalRef.componentInstance.school_grade_section_id = 0;
      modalRef.componentInstance.showInformationForRecall = false;
      modalRef.componentInstance.school_subject_name = null;
      modalRef.componentInstance.school_subject_id = 0;
    }
  }
}
