import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { GLOBAL_CONSTANT, PAGE_NAME } from "../lib/common/utils/Constants";

@Injectable({
  providedIn: "root",
})
export class BooksService {
  constructor(private http: HttpClient) {}

  GetSubjects(schoolGradeSectionId: number) {
    return this.http.get(
      `${environment.urlV2}/grade_subject_list?schoolGradeSectionId=` +
        schoolGradeSectionId
    );
  }
  GetBooks(obj) {
    // TODO : add loading while fetching
    if (obj.origin === PAGE_NAME.DASHBOARD) {
      return this.http.get(
        `${environment.urlV2}/books?userId=${obj.user_id}&origin=${obj.origin}`
      );
    } else if (obj.subject_id !== GLOBAL_CONSTANT.ALL_SUBJECTS_ID) {
      return this.http.get(
        `${environment.urlV2}/books?userId=${obj.user_id}&subjectId=${obj.subject_id}`
      );
    }
    return this.http.get(`${environment.urlV2}/books?userId=${obj.user_id}`);
  }
}
