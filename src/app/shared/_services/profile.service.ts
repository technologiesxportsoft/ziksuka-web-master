import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  constructor(private http: HttpClient) { }
  getProfileById(userId: any) {
    return this.http.get(`${environment.urlV2}/profile?userId=${userId}`);
  }
  getState(country) {
    return this.http.get(`${environment.urlV2}/state?countryId=${country}`);
  }
  getCity(country, state) {
    return this.http.get(`${environment.urlV2}/city?countryId=${country}&stateId=${state}`);
  }
  getCountry() {
    return this.http.get(`${environment.urlV2}/country`);
  }
  updateProfile(userObj: any) {
    return this.http.post(`${environment.urlV2}/profile`, userObj);
  }
  changePassword(obj: any) {
    return this.http.put(`${environment.urlV2}/changepassword`, obj);
  }
}
