import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
	providedIn: 'root'
})
export class HistoryService {

	constructor(private http: HttpClient) { }

	GetSubjectsListV2(sectionId: any, userId: number) {
		return this.http.get(`${environment.urlV2}/grade_subject_list?schoolGradeSectionId=` + sectionId
			+ "&userId=" + userId);
	}

	GetSubjects(userId: number) {
		return this.http.get(`${environment.url}/topics?user_id=` + userId);
	}

	GetTopics(userId: number, subjectId: number) {
		return this.http.get(`${environment.url}/topics?user_id=` + userId + "&subject_id=" + subjectId);
	}

	GetSubTopics(userId: number, subjectId: number, topicId: number) {
		return this.http.get(`${environment.url}/topics?user_id=` + userId + "&subject_id=" + subjectId + "&topic_id=" + topicId);
	}

	GetHistory(userId: number, subjectId: number, topicId: number, subtopicId: number) {
		if (subjectId != null && topicId != null && subtopicId != null) {
			return this.http.get(`${environment.url}/history?user_id=` + userId + "&subject_id=" + subjectId + '&topic_id=' + topicId + '&subtopic_id=' + subtopicId);
		} else if (subjectId != null && topicId != null) {
			return this.http.get(`${environment.url}/history?user_id=` + userId + "&subject_id=" + subjectId + '&topic_id=' + topicId);
		} else if (subjectId != null) {
			return this.http.get(`${environment.url}/history?user_id=` + userId + "&subject_id=" + subjectId);
		} else {
			return this.http.get(`${environment.url}/history?user_id=` + userId);
		}
	}
}
