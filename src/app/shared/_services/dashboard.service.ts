import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { CommonService } from "./common.service";
import { DatePipe } from '@angular/common';

@Injectable({
	providedIn: "root",
})
export class DashboardService {
	constructor(private http: HttpClient, private commonService: CommonService) { }
	
	datePipe = new DatePipe("en-IN");

	GetRecommendations( userId: number, subjectId: number, topicId: number, subtopicId: number, origin: String) {
		if (subjectId !== null && topicId !== null && subtopicId !== null) {
		  return this.http.get(
			`${environment.urlV2}/recommendations?userId=` +
			userId +
			"&subjectId=" +
			subjectId +
			"&topicId=" +
			topicId +
			"&subTopicId=" +
			subtopicId +
			"&origin=" +
			origin
		  );
		} else if (subjectId !== null && topicId !== null) {
		  return this.http.get(
			`${environment.urlV2}/recommendations?userId=` +
			userId +
			"&subjectId=" +
			subjectId +
			"&topicId=" +
			topicId +
			"&origin=" +
			origin
		  );
		} else if (subjectId !== null) {
		  return this.http.get(
			`${environment.urlV2}/recommendations?userId=` +
			userId +
			"&subjectId=" +
			subjectId +
			"&origin=" +
			origin
		  );
		} else {
		  return this.http.get(
			`${environment.urlV2}/recommendations?userId=` + userId + "&origin=" +
			origin
		  );
		}
	  }

	GetPerformance(userId: number) {
		return this.http.get(`${environment.url}/performance?user_id=` + userId);
	}
	GetNotifications(userId: number) {
		return this.http.get(`${environment.urlV2}/notification?userId=` + userId);
	}
	GetFeed(body: any) {
		return this.http.get<any>(`${environment.urlV2}/feed`, { params: body });
	}
	GetTimeTableDailySchedule(userId: number) {
		return this.http.get<any>(
			`${environment.urlV2}/timetable?userId=` + userId + `&type=DAILY`
		);
	}
	GetSubjects(userId: number) {
		return this.http.get<any>(`${environment.url}/topics?user_id=` + userId);
	}
	GetTopics(userId: number, subjectId: number) {
		return this.http.get<any>(
			`${environment.url}/topics?user_id=` + userId + `&subject_id=` + subjectId
		);
	}
	GetSubTopics(userId: number, subjectId: number, topicId: number) {
		return this.http.get<any>(
			`${environment.url}/topics?user_id=` +
			userId +
			`&subject_id=` +
			subjectId +
			`&topic_id=` +
			topicId
		);
	}
	pushTest(model) {
		return this.http.post<any>(`${environment.url}/recall_test?`, model);
	}


	startLiveClass(timetableSlot: any, userId: number) {
		let req = {
			"userId": userId,
			"schoolGradeSectionId": timetableSlot.school_grade_section_id,
			"schoolSubjectId": timetableSlot.school_subject_id,
			"schoolSubjectName": timetableSlot.school_subject_name,
			"startTime": timetableSlot.start_time,
			"endTime": timetableSlot.end_time
		}
		return this.http.post<any>(`${environment.urlV2}/classroom/create`, req);
	}

	startLiveClassV2(timetableSlot: any, userId: number) {
		let startdate = new Date();
		let enddate = new Date(startdate.getTime() + timetableSlot.duration * 60 * 1000);

		let req = {
			"userId": userId,
			"schoolGradeSectionId": timetableSlot.school_grade_section_id,
			"schoolSubjectId": timetableSlot.school_subject_id,
			"schoolSubjectName": timetableSlot.school_subject_name,
			"startTime": timetableSlot.start_time,
			"endTime": timetableSlot.end_time,
			"bcStartTime": this.datePipe.transform(startdate, "hh:mmaaa"),
			"bcEndTime": this.datePipe.transform(enddate, "hh:mmaaa"),
			"bcDate": this.datePipe.transform(startdate, "yyyy-MM-dd")
		}
		return this.http.post<any>(`${environment.urlV2}/classroom/create`, req);
	}

	notifyStudentsOfClassroom(userId: number, timetableSlot: any, classType: string, classUrl: string) {
		let req = {
			"userId": userId,
			"schoolGradeSectionId": timetableSlot.school_grade_section_id,
			"schoolSubjectId": timetableSlot.school_subject_id,
			"schoolSubjectName": timetableSlot.school_subject_name,
			[classType]: classUrl
		}
		return this.http.post<any>(`${environment.urlV2}/classroom/create/v3`, req);
	}

	// isDevMode() {
	// 	return !$(environment.production);
	// }


	getClassroomLaunchUrl(classroomId: any, userId: number) {
		return this.http.get<any>(`${environment.urlV2}/classroom/join?userId=` + userId + `&classroomId=` + classroomId);
	}

}
