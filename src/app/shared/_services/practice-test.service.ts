import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PracticeTestService {
  practiceTestData: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) { }

  startPracticeTest(obj) {
    return this.http.post(`${environment.urlV2}/practice`, obj);
  }
  endPracticeTest(obj) {
    return this.http.put(`${environment.urlV2}/practice`, obj);
  }
  createRecallTest(obj) {
    return this.http.post(`${environment.urlV2}/recall`, obj);
  }
  fetchRecallTest(obj) {
    return this.http.get(`${environment.urlV2}/recall?testId=${obj.testId}&studentId=${obj.studentId}`);
  }
  endRecallTest(obj) {
    return this.http.put(`${environment.urlV2}/recall`, obj);
  }
}
