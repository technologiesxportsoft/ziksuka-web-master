import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContactusService {

  constructor(private httpClient: HttpClient) { }

  submitContactUs(contactDetail) {
    return this.httpClient.post(`${environment.urlV2}/contact`, contactDetail);
  }
}
