import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import * as shajs from 'sha.js';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  headers: HttpHeaders = new HttpHeaders();
  URL_REGEX = "^(http[s]?:\\/\\/){0,1}(www\\.){0,1}[a-zA-Z0-9\\.\\-]+\\.[a-zA-Z]{2,5}[\\.]{0,1}";

  constructor(private http: HttpClient) { }

  getHeaders() {
    this.headers = this.headers.append('Accept', 'application/x-www-form-urlencoded');
    this.headers = this.headers.append('Content-Type', 'application/json');
    this.headers = this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers = this.headers.append('responseType', 'json');
    return this.headers;
  }

  getFormdataHeaders() {
    this.headers = this.headers.append('Accept', 'application/x-www-form-urlencoded');
    this.headers = this.headers.append('Content-DontOverRide', '');
    this.headers = this.headers.append('Access-Control-Allow-Origin', '*');
    this.headers = this.headers.append('responseType', 'json');
    return this.headers;
  }

  getShortName(fullName) {
    return fullName.split(' ').map(n => n[0]).join('');
  }

  nativeGlobal() { return window }

  generateUserValidationToken() {
    let userInfo = JSON.stringify(localStorage.userInfo);
    let loginDate = JSON.stringify(localStorage.loginDate);  
    var SEED = "bastaUserInfo12748391r8u";
    return shajs('sha256').update(userInfo + SEED + loginDate).digest('hex').substring(0,20);
  }

  isUrlValid(url): boolean {
    return new RegExp(this.URL_REGEX).test(url);
  }


}
