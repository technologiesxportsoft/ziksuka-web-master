import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Constants } from '../_components/core/constant.component';
import { DatePipe } from '@angular/common';
import { CookieService } from 'ngx-cookie-service';
import { CommonService } from './common.service';
import { environment } from '../../../environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    public currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;
    public loginTime: Date;

    constructor(private http: HttpClient, private constants: Constants, public datepipe: DatePipe,
		        public cookieService: CookieService, public commonService: CommonService) {
        this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('isLogin')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue() {
        return this.currentUserSubject.value;
    }

    login(model) {
        return this.http.post<any>(`${this.constants.apiUrlV2}/login`, model, { "headers": this.constants.headers })
            .pipe(map(data => {
                if (data.success) {
                    this.loginTime = new Date();
                    let login_date = this.datepipe.transform(this.loginTime, 'yyyy-MM-dd');
                    localStorage.setItem('loginDate', login_date);
                    localStorage.setItem('isLogin', 'true');
                    localStorage.setItem('token', data.data.token);
                    localStorage.setItem('userInfo', JSON.stringify(data.data.user_details));
                    this.cookieService.set('auth', this.commonService.generateUserValidationToken());

                    this.currentUserSubject.next(data.data.user_details);
                }
                return data;
            }));
    }

    logout() {
        localStorage.clear();
        this.cookieService.deleteAll();
        this.currentUserSubject.next(null);
    }

    forgotPassword(obj: any) {
        return this.http.put(`${environment.urlV2}/forgotpassword`, obj);
    }

}
