import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PrincipalService {

  constructor(private http :HttpClient) { }
  getDashboard(schoolId,principalId){
    return this.http.get(`${environment.url}/principal_dashboard?school_id=`+schoolId+`&principal_id=`+principalId);
  }
}
