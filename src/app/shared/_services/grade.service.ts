import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GradeService {

  constructor(private http: HttpClient) { }
  getGradeListByUserIdV2(userId: string) {
    return this.http.get(`${environment.urlV2}/grade_list?userId=` + userId);
  }
  getGradeListByUserId(userId: string) {
    return this.http.get(`${environment.url}/grade_list?user_id=` + userId);
  }
  getPerformance(gradeId: string) {
    return this.http.get(`${environment.url}/performance?grade_id=` + gradeId + `&type=overall`);
  }
  getSyllabusIndicator(gradeId: string) {
    return this.http.get(`${environment.url}/syllabus?grade_id=` + gradeId);
  }
}
