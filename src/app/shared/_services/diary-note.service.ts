import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { CommonService } from './common.service';

@Injectable({
  providedIn: 'root'
})
export class DiaryNoteService {

  constructor(private http: HttpClient, private commonService: CommonService) { }

  getStudentNotes(obj) {
    return this.http.post(`${environment.urlV2}/diary`, obj);
  }
  getTeacherNotes(obj) {
    return this.http.post(`${environment.urlV2}/diary`, obj);
  }
  addStudentNote(form) {
    return this.http.put(`${environment.urlV2}/diary`, form, { headers: this.commonService.getFormdataHeaders() });
  }
}
