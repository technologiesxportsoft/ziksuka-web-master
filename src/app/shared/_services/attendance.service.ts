import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AttendanceService {

  constructor(private http: HttpClient) { }

  getAttendance(user_id, month, year) {
    return this.http.get(`${environment.urlV2}/attendance?userId=${user_id}&month=${month}&year=${year}`);
  }
}
