import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from "@angular/common/http";

@Injectable({
    providedIn: 'root'
  })
  export class ClassroomService {
  
    constructor(private http: HttpClient) { }
  
    getWebSocketConnectionUrl() {
        return `${environment.signallingServer}`;
    }

    getIonServer() {
        return "wss://ziksuka.in:8443/ws";
    }

    getRtcConfiguration() {
        let configuration = {
            iceServers: [
                {
                    urls: `${environment.stunServer}`
                },
                {
                    urls: `${environment.turnServer}`,
                    username: `${environment.turnServerUsername}`,
                    credential: `${environment.turnServerPassword}`
                }
            ]
        }
        return configuration;
    }

    joinLiveClass(classroomId: string, userId: number) {
        return this.http.get<any>(`${environment.urlV2}/classroom/join?userId=` + userId + `&classroomId=` + classroomId);
    }


  }
  