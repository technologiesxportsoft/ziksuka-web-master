import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(private http :HttpClient) { }
  getTeacherListBySchool(schoolId){
    return this.http.get(`${environment.url}/teacher_list?school_id=`+schoolId);
  }
  getTeacherPerformance(teacherId,schoolId){
    return this.http.post(`${environment.url}/teacher_performance`,{"teacher_id": teacherId,"school_id": schoolId,})
  }
}
