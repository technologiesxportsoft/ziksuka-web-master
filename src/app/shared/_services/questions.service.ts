import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  private questions$: BehaviorSubject<any>;
  userInfo = null;

  questions = [
    { id: 1 },
    { id: 2 },
    { id: 3 },
    { id: 4 },
  ];

  constructor(public http: HttpClient, private auth: AuthenticationService) {
    this.questions$ = new BehaviorSubject<any>(this.questions);
    this.auth.currentUserSubject.subscribe(val => {
      this.userInfo = val;
    });
  }

  public getQuestionsObservable(): BehaviorSubject<any> {
    return this.questions$;
  }

  public getQuestions(subtopicId: number): Observable<any> {
    const userId = this.userInfo.user_id;
    console.log('Ids are', subtopicId, subtopicId);
    return this.http.get(`${environment.urlV2}/admin/questions?subtopicId=${subtopicId}&userId=${userId}`);
  }

  public getQuestionById(id: number): Observable<any> {
    const userId = this.userInfo.user_id;
    return this.http.get(`${environment.urlV2}/admin/question?questionId=` + id + '&userId=' + userId);
  }

  public updateQuestion(params): Observable<any> {
    params.userId = this.userInfo.user_id;
    console.log('Final request', params);
    return this.http.put(`${environment.urlV2}/admin/questions`, params);
  }

  public createNewQuestion(params): Observable<any> {
    params.userId = this.userInfo.user_id;
    return this.http.post(`${environment.urlV2}/admin/questions`, params);
  }

  public deleteQuestion(questionId: number): Observable<any> {
    const options = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
      body: { userId: this.userInfo.user_id, questionId }
    };
    return this.http.delete(`${environment.urlV2}/admin/questions`, options);
  }

  public getGrades(): Observable<any> {
    const userId = this.userInfo.user_id;
    console.log('In getGrades', userId);
    return this.http.get(`${environment.urlV2}/grade_list?userId=` + userId);
  }

  public fetchSubjects(classId): Observable<any> {
    const userId = this.userInfo.user_id;
    console.log('In fetchSubjects', userId);
    return this.http.get(`${environment.urlV2}/grade_subject_list?userId=` + userId + '&schoolGradeSectionId=' + classId);
  }

  public fetchBooks(subjectId, gradeId): Observable<any> {
    console.log('In fetchBooks');
    return this.http.get(`${environment.urlV2}/admin/book_list?subjectId=${subjectId}&gradeId=${gradeId}`);
  }

  public fetchTopics(bookId): Observable<any> {
    const userId = this.userInfo.user_id;
    console.log('In fetchTopics');
    return this.http.get(`${environment.urlV2}/topic_list?bookId=` + bookId + '&userId=' + userId);
  }

  public fetchSubtopics(bookId, topicId): Observable<any> {
    const userId = this.userInfo.user_id;
    console.log('In fetchTopics', bookId, topicId, userId);
    return this.http.get(`${environment.urlV2}/sub_topic_list?bookId=` + bookId + '&userId=' + userId + '&topicId=' + topicId);
  }
}
