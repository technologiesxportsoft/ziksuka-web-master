import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { StudentAttendence } from '../_model/StudentAttendence';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: HttpClient) { }
  GetStudentListByGrade(gradeId: number) {
    return this.http.get(`${environment.url}/student_list?grade_id=` + gradeId);
  }
  SaveStudentAttendence(studentAttendence) {
    return this.http.post(`${environment.urlV2}/attendance`, studentAttendence);
  }
  getStudentListByGradeAndSection(gradeid, sectionId) {
    return this.http.get(`${environment.url}/student_list?grade_id=` + gradeid + `&section_id=` + sectionId);
  }
  getStudentAttendence(sectionId) {
    return this.http.get(`${environment.urlV2}/grade_attendance?schoolGradeSectionId=` + sectionId);
  }
  getPerformance(studentId) {
    return this.http.get(`${environment.url}/performance?student_id=` + studentId + `&type=overall`);
  }
  getStudentListByGradeSectionV2(id) {
    return this.http.get(`${environment.urlV2}/student_list?schoolGradeSectionId=` + id);
  }
  getWeeklyTimeTable(userId){
    return this.http.get(`${environment.urlV2}/timetable?userId=`+userId);
  }
}
