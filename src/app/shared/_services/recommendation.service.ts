import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class RecommendationService {
  constructor(private http: HttpClient) { }

  GetSubjects(schoolGradeSectionId: number, userId: number) {
    return this.http.get(
      `${environment.urlV2}/grade_subject_list?schoolGradeSectionId=` +
      schoolGradeSectionId + "&userId=" + userId
    );
  }

  GetTopics(schoolGradeSectionId: number, schoolSubjectId: number, userId: number) {
    return this.http.get(
      `${environment.urlV2}/topic_list?schoolGradeSectionId=` +
      schoolGradeSectionId +
      "&schoolSubjectId=" +
      schoolSubjectId +
      "&userId=" +
      userId
    );
  }

  GetSubTopics(schoolGradeSectionId: number, schoolSubjectId: number, topicId: number, userId: number) {
    return this.http.get(
      `${environment.urlV2}/sub_topic_list?schoolGradeSectionId=` +
      schoolGradeSectionId +
      "&schoolSubjectId=" +
      schoolSubjectId +
      "&topicId=" +
      topicId +
      "&userId=" +
      userId
    );
  }

  GetRecommendation(
    userId: number,
    subjectId: number,
    topicId: number,
    subtopicId: number,
    origin: String
  ) {
    if (subjectId !== null && topicId !== null && subtopicId !== null) {
      return this.http.get(
        `${environment.urlV2}/recommendations?userId=` +
        userId +
        "&subjectId=" +
        subjectId +
        "&topicId=" +
        topicId +
        "&subtopicId=" +
        subtopicId +
        "&origin=" +
        origin
      );
    } else if (subjectId !== null && topicId !== null) {
      return this.http.get(
        `${environment.urlV2}/recommendations?userId=` +
        userId +
        "&subjectId=" +
        subjectId +
        "&topicId=" +
        topicId +
        "&origin=" +
        origin
      );
    } else if (subjectId !== null) {
      return this.http.get(
        `${environment.urlV2}/recommendations?userId=` +
        userId +
        "&subjectId=" +
        subjectId +
        "&origin=" +
        origin
      );
    } else {
      return this.http.get(
        `${environment.urlV2}/recommendations?userId=` + userId + "&origin=" + origin
      );
    }
  }
}
