import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class TestResultsService {

    constructor(private http: HttpClient) { }

    getTestResult(userId, studentId, type) {
        if (type == 'ALL') {
            return this.http.get(`${environment.urlV2}/test_list?userId=${userId}&studentId=${studentId}`)
        } else {
            return this.http.get(`${environment.urlV2}/test_list?userId=${userId}&studentId=${studentId}&type=${type}`)
        }
    }
    getTest(userId, studentId, testId) {
        return this.http.get(`${environment.urlV2}/test_result?userId=${userId}&studentId=${studentId}&testId=${testId}`)
    }

    getPerformanceData(userId: number, schoolGradeSectionId: number, studentId: number) {
        return this.http.get(`${environment.urlV2}/test/performance?userId=${userId}` +
                    `&schoolGradeSectionId=${schoolGradeSectionId}&days=7&parikshaType=RECALL&studentId=${studentId}`);
    }

}
