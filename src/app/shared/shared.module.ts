import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HeaderComponent } from "./_components/header/header.component";
import { SidebarComponent } from "./_components/sidebar/sidebar.component";
import { LoginComponent } from "../auth/login/login.component";
import { HomeLayoutComponent } from "./_components/layouts/home-layout/home-layout.component";
import { LoginLayoutComponent } from "./_components/layouts/login-layout/login-layout.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CanvasWhiteboardModule } from "ng2-canvas-whiteboard";
import { SlickCarouselModule } from "ngx-slick-carousel";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { PdfViewerModule } from "ng2-pdf-viewer";
import { AgmCoreModule } from "@agm/core";
import { ModalModule } from "ngx-bootstrap/modal";
import { RouterModule } from "@angular/router";
import { LoaderComponent } from "./_components/loader/loader.component";
import { DragModalModule } from "./lib/modal/modal-module";
import { TestConfigurationComponent } from "./_components/test-configuration/test-configuration.component";
import { NgCircleProgressModule } from "ng-circle-progress";
import { Ng5SliderModule } from 'ng5-slider';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { GradeTestConfigurationComponent } from './_components/grade-test-configuration/grade-test-configuration.component';
import { KatexModule } from 'ng-katex';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { MyFilterPipe, KeySortPipe } from './_pipes/myfilter.pipe';
import { NotebookComponent } from '../pages/notebook/notebook.component';
import { TruncatePipe } from './_pipes/truncate.pipe';
import { MatExpansionModule } from '@angular/material/expansion';
import { NotepadComponent } from './_components/notepad/notepad.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { MatFormFieldModule, MatButtonModule, MatInputModule, MatSliderModule, MatIconModule, MatTooltipModule, MatDialogModule, MatToolbarModule, MatMenuModule, MatListModule } from '@angular/material';
import { ColorPickerModule } from 'ngx-color-picker';
import { QuestionPreviewComponent } from './_components/question-preview/question-preview.component';
import { SanitizeHtmlPipe } from './_pipes/sanitize-html.pipe';
import { PrepareQuestionPipe } from './_components/question-preview/prepare-question.pipe';
import { PrepareOptionsPipe } from './_components/question-preview/prepare-options.pipe';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSidenavComponent } from './_components/mat-sidenav/mat-sidenav.component';
import { SafePipe } from './_pipes/safe.pipe';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';


@NgModule({
  declarations: [
    MyFilterPipe,
    KeySortPipe,
    HeaderComponent,
    SidebarComponent,
    LoginComponent,
    LoaderComponent,
    HomeLayoutComponent,
    LoginLayoutComponent,
    TestConfigurationComponent,
    GradeTestConfigurationComponent,
    NotebookComponent,
    NotepadComponent,
    MatSidenavComponent,
    TestConfigurationComponent, GradeTestConfigurationComponent, TruncatePipe, QuestionPreviewComponent, SanitizeHtmlPipe, PrepareQuestionPipe, PrepareOptionsPipe, SafePipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    MatExpansionModule,
    CanvasWhiteboardModule,
    SlickCarouselModule,
    NgbModule,
    DragModalModule,
    PdfViewerModule,
    KatexModule,
    MatSidenavModule,
    MatMenuModule,
    MatListModule,
    ModalModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: "AIzaSyDwyun074w-I9-ZgG-HvDAx5aNkhreeNeE"
      /* apiKey is required, unless you are a
      premium customer, in which case you can
      use clientId
      */
    }),
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 20,
      outerStrokeWidth: 2,
      innerStrokeWidth: 2,
      outerStrokeColor: "#2E1CC4",
      animationDuration: 300
    }), AngularMultiSelectModule, Ng5SliderModule,
    Ng2SearchPipeModule,
    NgxExtendedPdfViewerModule,
    FormsModule,
    Ng2SearchPipeModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatSliderModule,
    ColorPickerModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    MatDialogModule,
    MatToolbarModule,
    MatSlideToggleModule
  ],
  exports: [
    MyFilterPipe,
    MatSidenavModule,
    KeySortPipe,
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    MatExpansionModule,
    CanvasWhiteboardModule,
    SlickCarouselModule,
    NgbModule,
    KatexModule,
    PdfViewerModule,
    ModalModule,
    NgCircleProgressModule,
    AgmCoreModule,
    DragModalModule,
    Ng2SearchPipeModule,
    NgxExtendedPdfViewerModule,
    FormsModule,
    Ng2SearchPipeModule,
    MatFormFieldModule,
    MatButtonModule,
    MatInputModule,
    MatSliderModule,
    ColorPickerModule,
    MatButtonModule,
    MatIconModule,
    MatTooltipModule,
    MatSlideToggleModule,
    HeaderComponent,
    SidebarComponent,
    LoginComponent,
    LoaderComponent,
    HomeLayoutComponent,
    LoginLayoutComponent,
    GradeTestConfigurationComponent,
    NotebookComponent,
    LoginLayoutComponent,
    GradeTestConfigurationComponent,
    TruncatePipe,
    SafePipe,
    NotepadComponent
  ],
  entryComponents: [TestConfigurationComponent]
})
export class SharedModule { }
