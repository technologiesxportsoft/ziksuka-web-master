import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit {
  yt = '<iframe class="w-100 h-100" src="http://dznvyvcu8qlyz.cloudfront.net/docs/Privacy+Policy.pdf" frameborder="0"  encrypted-media" allowfullscreen></iframe>';

  editable = true;

  @ViewChild('myDiv', { static: true }) myDiv: ElementRef;
  content = ``;

  constructor(private renderer: Renderer2) { }

  ngOnInit() {
    this.renderer.setAttribute(this.myDiv.nativeElement, 'innerHTML', this.content);
  }

}




